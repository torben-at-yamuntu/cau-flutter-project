import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:contract_manager/application/service_item/create_service_item.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch_repository.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_data.dart';

void main() {
  late CreateServiceItem createServiceItem;
  late MockFirestoreInstance firestore;
  late IServiceItemRepository sRepository;
  late IBatchRepository batchRepository;

  final ServiceItemInventoryParams inventoryParams = ServiceItemInventoryParams(
    material: tServiceItemInventory1.material,
    quantity: tServiceItemInventory1.quantity,
    unit: tServiceItemInventory1.quantityUnit,
    price: tServiceItemInventory1.price,
  );

  final CreateServiceItemParams params = CreateServiceItemParams(
    projectId: tServiceItem1.projectId,
    contractId: tServiceItem1.contractId,
    specification: tServiceItem1.specification,
    startAt: tServiceItem1.startsAt,
    endPlannedAt: tServiceItem1.endPlannedAt,
    inventories: [inventoryParams],
    employeeId: tEmployeeRW1.id,
  );

  final String tPath = '''projects/${tServiceItem1.projectId}'''
      '''/contracts/${tServiceItem1.contractId}'''
      '''/serviceItems''';

  setUp(() async {
    firestore = MockFirestoreInstance();
    sRepository = ServiceItemRepository(firestore);
    batchRepository = FirestoreBatchRepository(firestore);
    createServiceItem = CreateServiceItem(sRepository, batchRepository);
  });

  test('create succesful', () async {
    final ServiceItem dbServiceItem = await createServiceItem.call(params);
    expect(firestore.hasSavedDocument('$tPath/${dbServiceItem.id}'), true);
  });
}
