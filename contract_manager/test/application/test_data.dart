import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/company/company_model.dart';
import 'package:contract_manager/infrastructure/contract/contract_model.dart';
import 'package:contract_manager/infrastructure/employee/employee_model.dart';
import 'package:contract_manager/infrastructure/project/project_model.dart';
import 'package:contract_manager/infrastructure/service_item/payment_model.dart';
import 'package:contract_manager/infrastructure/service_item/report_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_model.dart';

const Employee tEmployeeRW1 = EmployeeModel(
  id: 'tEmployeeId1',
  companyId: 'tCompanyId1',
  name: 'tName1',
  role: EmployeeRole.readWrite,
);

const Address tAddress = AddressModel(
  country: 'tCountry1',
  city: 'tCity1',
  zip: 123,
  street: 'tStreet1',
  houseNumber: 1,
);

const BankInfo tBankInfo1 = BankInfoModel(
  iban: 'tIban1',
  owner: 'tOwner1',
  bic: 'tBic1',
);

const Company tCompany1 = CompanyModel(
  id: 'tCompanyId1',
  name: 'tName1',
  bankInfo: tBankInfo1,
  address: tAddress,
);

const Company tCompany2 = CompanyModel(
  id: 'tCompanyId2',
  name: 'tName2',
  bankInfo: tBankInfo1,
  address: tAddress,
);

final Payment tPayment1 = PaymentModel(
  id: 'tPaymentId1',
  amount: 1.1,
  serviceItemIds: BuiltList(<String>[tServiceItem1.id]),
  sentPaymentAt: DateTime.fromMillisecondsSinceEpoch(2),
  receivedPaymentAt: DateTime.fromMillisecondsSinceEpoch(3),
  paymentActiveAt: DateTime.fromMillisecondsSinceEpoch(0),
);

final Contract tContract1 = ContractModel(
  id: 'tContractId1',
  projectId: 'tProject1',
  createdAt: DateTime.fromMillisecondsSinceEpoch(0),
  contractorId: 'tContractorId1',
  clientId: 'tClientId1',
  specification: 'tSpecification1',
  bankInfo: tBankInfo1,
  costs: BuiltList(<Payment>[tPayment1]),
  versionNumber: 1,
  lastSeenVersions: {tEmployeeRW1.id: 1}.build(),
  name: 'tContractName1',
);

final Project tProject1 = ProjectModel(
  id: 'tProjectId1',
  createdAt: DateTime.fromMillisecondsSinceEpoch(0),
  companyId: 'tCompanyId1',
  name: 'tProjectName1',
);

final Report tReport1 = ReportModel(
  authorId: 'tEmployeeId1',
  message: 'tMessage1',
  imagePaths: BuiltList(<String>['test']),
  isError: true,
);

//ServiceItemInventory
const ServiceItemInventory tServiceItemInventory1 = ServiceItemInventoryModel(
  id: 'tServiceItemInventoryId1',
  material: 'tMaterial1',
  quantity: 1.1,
  quantityUnit: QuantityUnit.h,
  price: 1.1,
);

final ServiceItem tServiceItem1 = ServiceItemModel(
    id: 'tServiceItemId1',
    projectId: 'tProjectId1',
    contractId: 'tContractId1',
    specification: 'tSpecification1',
    startsAt: DateTime.fromMillisecondsSinceEpoch(0),
    endPlannedAt: DateTime.fromMillisecondsSinceEpoch(1),
    inventories: {tServiceItemInventory1.id: tServiceItemInventory1}.build(),
    versionNumber: 1,
    lastSeenVersions: {tEmployeeRW1.id: 1}.build());
