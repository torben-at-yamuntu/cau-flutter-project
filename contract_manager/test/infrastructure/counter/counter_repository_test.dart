import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:contract_manager/domain/counter/counter.dart';
import 'package:contract_manager/domain/counter/counter_failure.dart';
import 'package:contract_manager/domain/counter/i_counter_repository.dart';
import 'package:contract_manager/infrastructure/counter/counter_model.dart';
import 'package:contract_manager/infrastructure/counter/counter_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late MockFirestoreInstance firestore;
  late ICounterRepository tRepository;

  late CounterModel counter1;
  late CounterModel counter2;
  late CounterModel counter3;

  setUp(() async {
    firestore = MockFirestoreInstance();
    tRepository = CounterRepository(firestore);
    final DateTime epoch = DateTime.fromMillisecondsSinceEpoch(0);

    counter1 = CounterModel(id: 'test1', value: 0, createdAt: epoch);
    await firestore.doc('counters/${counter1.id}').set(counter1.toJson());

    counter2 = CounterModel(id: 'test2', value: 0, createdAt: epoch);
    await firestore.doc('counters/${counter2.id}').set(counter2.toJson());

    counter3 = CounterModel(id: 'test3', value: 0, createdAt: epoch);
    await firestore.doc('counters/${counter3.id}').set(counter3.toJson());
  });

  group('CounterRepository', () {
    test('create successful', () async {
      final QuerySnapshot tSnapshots1 = await firestore.collection('counters').get();
      expect(tSnapshots1.docs.length, 3);

      final Either<CounterFailure, Unit> tResult = await tRepository.create();
      expect(tResult.isRight(), true);

      final QuerySnapshot tSnapshots2 = await firestore.collection('counters').get();
      expect(tSnapshots2.docs.length, 4);
    });

    test('increment successful', () async {
      final DocumentSnapshot tSnapshots1 = await firestore.doc('counters/test1').get();
      final Counter tCounterModel1 = CounterModel.fromJson(tSnapshots1.data()!);
      expect(tCounterModel1.value, 0);

      final Either<CounterFailure, Unit> tResult = await tRepository.increment('test1');
      expect(tResult.isRight(), isTrue);

      final DocumentSnapshot tSnapshots2 = await firestore.doc('counters/test1').get();
      final Counter tCounterModel2 = CounterModel.fromJson(tSnapshots2.data()!);
      expect(tCounterModel2.value, 1);
    });

    test('watchCounters successful', () async {
      final Stream<BuiltList<Counter>> tResult = tRepository.watchCounters();

      final BuiltList<Counter> tFirst = await tResult.first;
      final BuiltList<Counter> tFirstExpectation = BuiltList(<Counter>[counter1, counter2, counter3]);
      expect(tFirst, tFirstExpectation);
    });

    test('increment successful', () async {
      await tRepository.increment('test1');
      expect(firestore.hasSavedDocument('counters/test1'), isTrue);

      final DocumentSnapshot snapshot = await firestore.doc('counters/test1').get();

      final CounterModel tResultCounter = counter1.copyWith(value: counter1.value + 1);
      expect(snapshot.data(), tResultCounter.toJson());
    });
  });
}
