import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/counter/counter.dart';
import 'package:contract_manager/infrastructure/counter/counter_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  final DateTime tTime = DateTime.fromMillisecondsSinceEpoch(0);
  final Counter tCounter = CounterModel(
    createdAt: tTime,
    id: 'testId',
    value: 0,
  );

  group('CounterModel.fromJson', () {
    test('CounterModel.fromJson successful', () {
      final Map<String, dynamic> tJson = <String, dynamic>{
        'id': 'testId',
        'value': 0,
        'createdAt': Timestamp.fromMillisecondsSinceEpoch(0),
      };

      final Counter tCounterModel = CounterModel.fromJson(tJson);
      expect(tCounter, tCounterModel);
    });

    test('CounterModel.fromJson failing', () {
      try {
        final Map<String, dynamic> tJson = <String, dynamic>{};

        CounterModel.fromJson(tJson);
        expect(false, isTrue);
      } on MissingRequiredKeysException catch (e) {
        final List<String> tMissingKeys = <String>['id', 'value', 'createdAt'];

        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue);
      }
    });
  });
}
