import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/service_item/report_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_model.dart';

final DateTime tStartDate = DateTime.fromMillisecondsSinceEpoch(0);
final DateTime tEndDate = DateTime.fromMillisecondsSinceEpoch(1000);
final DateTime tWorkDoneAt = DateTime.fromMillisecondsSinceEpoch(2000);
final DateTime tApprovedDate = DateTime.fromMillisecondsSinceEpoch(3000);

final Timestamp tStartDateJ = Timestamp.fromMillisecondsSinceEpoch(0);
final Timestamp tEndDateJ = Timestamp.fromMillisecondsSinceEpoch(1000);
final Timestamp tWorkDoneAtJ = Timestamp.fromMillisecondsSinceEpoch(2000);
final Timestamp tApprovedAtJ = Timestamp.fromMillisecondsSinceEpoch(3000);

final BuiltList<String> imagePaths = BuiltList<String>(<String>['0', '1']);

const ServiceItemInventoryModel tServiceItemInventory1 = ServiceItemInventoryModel(
  id: 'id',
  material: 'material',
  quantity: 3.3,
  quantityUnit: QuantityUnit.kg,
  price: 1.1,
);
const ServiceItemInventoryModel tServiceItemInventory2 = ServiceItemInventoryModel(
  id: 'test',
  material: 'test',
  quantity: 5.5,
  quantityUnit: QuantityUnit.h,
  price: 1.1,
);
final ReportModel tReport = ReportModel(
  authorId: 'authorId',
  message: 'message',
  imagePaths: BuiltList<String>(imagePaths),
  isError: false,
);

final BuiltMap<String, int> tLastSeenVersions = BuiltMap(<String, int>{
  'Item': 1,
});
final Map<String, int> tLastSeenVersionsJ = <String, int>{
  'Item': 1,
};

const String tId1 = 'id1';
const String tId2 = 'id2';

const String tProjectId1 = 'projectId1';
const String tProjectId2 = 'projectId2';

const String tContractId1 = 'contractId1';
const String tContractId2 = 'contractId2';

const String tSpecification1 = 'specification1';
const String tSpecification2 = 'specification2';

final BuiltMap<String, ServiceItemInventory> tList = {tServiceItemInventory1.id: tServiceItemInventory1}.build();
final BuiltMap<String, ServiceItemInventory> tList2 = {tServiceItemInventory2.id: tServiceItemInventory2}.build();

final ServiceItemModel tServiceItem = ServiceItemModel(
  id: tId1,
  projectId: tProjectId1,
  contractId: tContractId1,
  specification: tSpecification1,
  startsAt: tStartDate,
  endPlannedAt: tEndDate,
  inventories: tList,
  versionNumber: 1,
  lastSeenVersions: tLastSeenVersions,
  report: tReport,
  approvedAt: tApprovedDate,
  workDoneAt: tWorkDoneAt,
);

final ServiceItem tServiceItemChanged = tServiceItem.copyWith(
  approvedAt: tApprovedDate,
  workDoneAt: tWorkDoneAt,
  report: tReport,
  inventories: tList2,
);

final ServiceItemModel tCompareCopyWith = ServiceItemModel(
  id: tId1,
  projectId: tProjectId1,
  contractId: tContractId1,
  specification: tSpecification1,
  startsAt: tStartDate,
  endPlannedAt: tEndDate,
  approvedAt: tApprovedDate,
  workDoneAt: tWorkDoneAt,
  inventories: tList2,
  versionNumber: 1,
  lastSeenVersions: tLastSeenVersions,
  report: tReport,
);

final Map<String, dynamic> tServiceItemJ = <String, dynamic>{
  'id': tId1,
  'projectId': tProjectId1,
  'contractId': tContractId1,
  'specification': tSpecification1,
  'startsAt': tStartDateJ,
  'endPlannedAt': tEndDateJ,
  'inventories': tList.values.map((serviceItem) => serviceItem.toJson()),
  'report': tReport.toJson(),
  'versionNumber': 1,
  'lastSeenVersions': tLastSeenVersionsJ,
  'workDoneAt': tWorkDoneAtJ,
  'approvedAt': tApprovedAtJ,
};
