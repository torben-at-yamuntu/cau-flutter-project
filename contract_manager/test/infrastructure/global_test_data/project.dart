import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/infrastructure/project/project_model.dart';

const int tTime1 = 0;
const int tTime2 = 1;

const String idKey = 'id';
const String createdAtKey = 'createdAt';
const String companyIDKey = 'companyId';
const String nameKey = 'name';

const String tId1 = '1';

final DateTime tDateTime1 = DateTime.fromMillisecondsSinceEpoch(tTime1);

final Timestamp tTimeStamp = Timestamp.fromMillisecondsSinceEpoch(tTime1);

const String tCompanyID1 = 'id1';
const String tCompanyID2 = 'id2';

const String tName1 = 'tName1';
const String tName2 = 'tName2';

final ProjectModel tProject1 = ProjectModel(
  id: tId1,
  createdAt: tDateTime1,
  companyId: tCompanyID1,
  name: tName1,
);

final ProjectModel tProject2 = ProjectModel(
  id: tId1,
  createdAt: tDateTime1,
  companyId: tCompanyID1,
  name: tName2,
);

final Map<String, dynamic> tJson = <String, dynamic>{
  idKey: tId1,
  createdAtKey: tTimeStamp,
  companyIDKey: tCompanyID1,
  nameKey: tName1,
};
