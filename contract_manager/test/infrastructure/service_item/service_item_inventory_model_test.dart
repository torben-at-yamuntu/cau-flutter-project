import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const String idKey = 'id';
  const String materialKey = 'material';
  const String quantityKey = 'quantity';
  const String quantityUnitKey = 'quantityUnit';

  const String tId1 = '1';

  const String tMaterial1 = 'a';
  const String tMaterial2 = 'b';

  const double tQuantity1 = 1;
  const double tQuantity2 = 2;

  const QuantityUnit tQuantityUnit1 = QuantityUnit.h;
  const String tQuantityUnitS1 = 'h';
  const QuantityUnit tQuantityUnit2 = QuantityUnit.kg;

  const ServiceItemInventory tServiceItemInventory1 = ServiceItemInventoryModel(
    id: tId1,
    material: tMaterial1,
    quantity: tQuantity1,
    quantityUnit: tQuantityUnit1,
    price: 1.1,
  );

  const ServiceItemInventory tServiceItemInventory2 = ServiceItemInventoryModel(
    id: tId1,
    material: tMaterial2,
    quantity: tQuantity2,
    quantityUnit: tQuantityUnit2,
    price: 1.1,
  );

  const Map<String, dynamic> tJson = <String, dynamic>{
    idKey: tId1,
    materialKey: tMaterial1,
    quantityKey: tQuantity1,
    quantityUnitKey: tQuantityUnitS1,
    'price': 1.1,
  };

  group('ServiceItemInventory', () {
    group('fromJson', () {
      test('succesful', () {
        final ServiceItemInventory tServiceItemInventoryModel = ServiceItemInventoryModel.fromJson(tJson);
        expect(tServiceItemInventoryModel, tServiceItemInventory1);
      });

      test('failing, because missing keys', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{};

          ServiceItemInventoryModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on MissingRequiredKeysException catch (e) {
          final List<String> tMissingKeys = <String>[idKey, materialKey, quantityKey, quantityUnitKey, 'price'];

          for (final String key in e.missingKeys) {
            expect(tMissingKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });

      test('failing, because dissallowed null', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{
            idKey: null,
            materialKey: null,
            quantityKey: null,
            quantityUnitKey: null,
            'price': null
          };

          ServiceItemInventoryModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on DisallowedNullValueException catch (e) {
          final List<String> dissallowedNullValueKeys = <String>[
            idKey,
            materialKey,
            quantityKey,
            quantityUnitKey,
            'price'
          ];

          for (final String key in e.keysWithNullValues) {
            expect(dissallowedNullValueKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });
    });

    group('toJson', () {
      test('successful', () {
        final Map<String, dynamic> tJsonFromModel = tServiceItemInventory1.toJson();
        expect(tJsonFromModel, tJson);
      });

      test('fromJson -> toJson == tJson', () {
        final ServiceItemInventory tServiceItemInventoryFromJson = ServiceItemInventoryModel.fromJson(tJson);
        final Map<String, dynamic> tJsonFromModel = tServiceItemInventoryFromJson.toJson();
        expect(tJsonFromModel, tJson);
      });
    });

    group('copyWith', () {
      test('Empty', () {
        final ServiceItemInventory tServiceItemInventoryResult = tServiceItemInventory1.copyWith();
        expect(tServiceItemInventoryResult, tServiceItemInventory1);
      });

      test('All', () {
        final ServiceItemInventory tServiceItemInventoryResult = tServiceItemInventory1.copyWith(
          material: tMaterial2,
          quantity: tQuantity2,
          quantityUnit: tQuantityUnit2,
        );
        expect(tServiceItemInventoryResult, tServiceItemInventory2);
      });
    });
  });
}
