import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/service_item/payment_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  final DateTime tSPTime = DateTime.fromMillisecondsSinceEpoch(0);
  final DateTime tRPTime = DateTime.fromMillisecondsSinceEpoch(1000);
  final DateTime tPATime = DateTime.fromMillisecondsSinceEpoch(2000);
  final List<String> serviceItemIds = ['0', '1'];
  final Payment tPayment = PaymentModel(
    id: 'testId',
    amount: 10,
    serviceItemIds: BuiltList<String>(serviceItemIds),
    sentPaymentAt: tSPTime,
    receivedPaymentAt: tRPTime,
    paymentActiveAt: tPATime,
  );

  final Timestamp tSPTimeJ = Timestamp.fromMillisecondsSinceEpoch(0);
  final Timestamp tRPTimeJ = Timestamp.fromMillisecondsSinceEpoch(1000);
  final Timestamp tPATimeJ = Timestamp.fromMillisecondsSinceEpoch(2000);
  final Map<String, dynamic> tJson = <String, dynamic>{
    'id': 'testId',
    'amount': 10.0,
    'serviceItemIds': serviceItemIds,
    'sentPaymentAt': tSPTimeJ,
    'receivedPaymentAt': tRPTimeJ,
    'paymentActiveAt': tPATimeJ,
  };

  group('PaymentModel.fromJson', () {
    test('successful', () {
      final Payment tPaymentModel = PaymentModel.fromJson(tJson);
      expect(tPayment, tPaymentModel);
    });
    test('failing, because missing keys', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{};

        PaymentModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        final List<String> tMissingKeys = <String>[
          'id',
          'amount',
          'serviceItemIds',
          'sentPaymentAt',
          'receivedPaymentAt',
          'paymentActiveAt',
        ];

        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });

    test('failing, because dissallowed null', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{
          'id': null,
          'amount': null,
          'serviceItemIds': null,
          'sentPaymentAt': null,
          'receivedPaymentAt': null,
          'paymentActiveAt': null,
        };

        PaymentModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on DisallowedNullValueException catch (e) {
        final List<String> dissallowedNullValueKeys = <String>[
          'id',
          'amount',
          'serviceItemIds',
        ];

        for (final String key in e.keysWithNullValues) {
          expect(dissallowedNullValueKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });

  group('PaymentModel.toJson', () {
    test('PaymentModel.toJson successful', () {
      final Map<String, dynamic> tJsonFromModel = tPayment.toJson();
      expect(tJson, tJsonFromModel);
    });

    test('PaymentModel.fromJson -> PaymentModel.toJson == tJson', () {
      final Payment tPaymentFromJson = PaymentModel.fromJson(tJson);
      final Map<String, dynamic> tJsonFromModel = tPaymentFromJson.toJson();

      expect(tJsonFromModel, tJson);
    });
  });

  group('PaymentModel.copyWith', () {
    test('Empty', () {
      final Payment tPaymentResult = tPayment.copyWith();
      expect(tPaymentResult, tPayment);
    });
    test('All', () {
      final Payment tPaymentResult = tPayment.copyWith(
        id: 'testId2',
        amount: 11,
        serviceItemIds: BuiltList<String>(const <String>['1', '2', '3']),
        sentPaymentAt: tPATime,
        receivedPaymentAt: tSPTime,
        paymentActiveAt: tRPTime,
      );
      final Payment tPaymentExpected = PaymentModel(
        id: 'testId2',
        amount: 11,
        serviceItemIds: BuiltList<String>(const <String>['1', '2', '3']),
        sentPaymentAt: tPATime,
        receivedPaymentAt: tSPTime,
        paymentActiveAt: tRPTime,
      );
      expect(tPaymentResult, tPaymentExpected);
    });
  });
}
