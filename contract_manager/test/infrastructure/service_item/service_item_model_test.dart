import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/service_item/report_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  final DateTime tStartDate = DateTime.fromMillisecondsSinceEpoch(0);
  final DateTime tEndDate = DateTime.fromMillisecondsSinceEpoch(1000);
  final DateTime tWorkDoneAt = DateTime.fromMillisecondsSinceEpoch(2000);
  final DateTime tApprovedDate = DateTime.fromMillisecondsSinceEpoch(3000);

  final Timestamp tStartDateJ = Timestamp.fromMillisecondsSinceEpoch(0);
  final Timestamp tEndDateJ = Timestamp.fromMillisecondsSinceEpoch(1000);
  final Timestamp tWorkDoneAtJ = Timestamp.fromMillisecondsSinceEpoch(2000);
  final Timestamp tApprovedAtJ = Timestamp.fromMillisecondsSinceEpoch(3000);

  final BuiltList<String> imagePaths = BuiltList<String>(<String>['0', '1']);

  const ServiceItemInventory tServiceItemInventory1 = ServiceItemInventoryModel(
    id: 'id',
    material: 'material',
    quantity: 3.3,
    quantityUnit: QuantityUnit.kg,
    price: 1.1,
  );
  const ServiceItemInventory tServiceItemInventory2 = ServiceItemInventoryModel(
    id: 'test',
    material: 'test',
    quantity: 5.5,
    quantityUnit: QuantityUnit.h,
    price: 1.1,
  );
  final Report tReport = ReportModel(
    authorId: 'authorId',
    message: 'message',
    imagePaths: BuiltList<String>(imagePaths),
    isError: false,
  );

  final BuiltMap<String, int> tLastSeenVersions = BuiltMap(<String, int>{
    'Item': 1,
  });

  final BuiltMap<String, ServiceItemInventory> tList = {tServiceItemInventory1.id: tServiceItemInventory1}.build();
  final BuiltMap<String, ServiceItemInventory> tList2 = {tServiceItemInventory2.id: tServiceItemInventory2}.build();

  final ServiceItem tServiceItem = ServiceItemModel(
    id: 'id',
    projectId: 'projectId',
    contractId: 'contractId',
    specification: 'specification',
    startsAt: tStartDate,
    endPlannedAt: tEndDate,
    inventories: tList,
    versionNumber: 1,
    lastSeenVersions: tLastSeenVersions,
    report: tReport,
    approvedAt: tApprovedDate,
    workDoneAt: tWorkDoneAt,
  );

  final ServiceItem tServiceItemChanged = tServiceItem.copyWith(
    approvedAt: tApprovedDate,
    workDoneAt: tWorkDoneAt,
    report: tReport,
    inventories: tList2,
  );

  final ServiceItem tCompareCopyWith = ServiceItemModel(
    id: 'id',
    projectId: 'projectId',
    contractId: 'contractId',
    specification: 'specification',
    startsAt: tStartDate,
    endPlannedAt: tEndDate,
    approvedAt: tApprovedDate,
    workDoneAt: tWorkDoneAt,
    inventories: tList2,
    versionNumber: 1,
    lastSeenVersions: tLastSeenVersions,
    report: tReport,
  );

  group('ServiceItemModel.fromJson', () {
    test('Failing: Missing keys case', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{};

        ServiceItemModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        final Set<String> tMissingKeys = <String>{
          'id',
          'projectId',
          'contractId',
          'specification',
          'startsAt',
          'endPlannedAt',
          'inventories',
          'versionNumber',
          'lastSeenVersions',
          'workDoneAt',
          'approvedAt',
          'report',
        };

        expect(e.missingKeys.length, equals(tMissingKeys.length));

        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
    test('Failing: Nullable Case', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{
          'id': null,
          'projectId': null,
          'contractId': null,
          'specification': null,
          'startsAt': null,
          'endPlannedAt': null,
          'inventories': null,
          'versionNumber': null,
          'lastSeenVersions': null,
          'workDoneAt': null,
          'approvedAt': null,
          'report': null,
        };
        ServiceItemModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on DisallowedNullValueException catch (e) {
        final Set<String> disallowedNullKeys = <String>{
          'id',
          'projectId',
          'contractId',
          'specification',
          'startsAt',
          'endPlannedAt',
          'inventories',
          'versionNumber',
          'lastSeenVersions',
        };

        expect(e.keysWithNullValues.length, equals(disallowedNullKeys.length));

        for (final String key in e.keysWithNullValues) {
          expect(disallowedNullKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });

  group('ServiceItemModel.toJson', () {
    // test('Success', () {
    //   expect(tServiceItem.toJson(), tServiceItemJ);
    // });

    test('tojson fromJson= ServiceItemModel', () {
      expect(ServiceItemModel.fromJson(tServiceItem.toJson()), tServiceItem);
    });
  });

  group('ProjectModel.copyWith', () {
    test('Empty', () {
      expect(tServiceItem.copyWith(), tServiceItem);
    });

    test('All', () {
      expect(tServiceItemChanged, tCompareCopyWith);
    });
  });
}
