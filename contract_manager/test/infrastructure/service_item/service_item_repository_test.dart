import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import '../global_test_data/service_item.dart';

void main() {
  //Mock environment
  late MockFirestoreInstance firestore;
  late IServiceItemRepository tRepository;

  const String _serviceItemCollectionName = 'serviceItems';

  // Put a single project in mocked database
  setUp(() async {
    firestore = MockFirestoreInstance();
    tRepository = ServiceItemRepository(firestore);

    await firestore
        .doc('projects/$tProjectId1/contracts/$tContractId1/$_serviceItemCollectionName/${tServiceItem.id}')
        .set(tServiceItem.toJson());
  });

  group('ServiceItemRepository', () {
    {
      // test('addServiceItemInventory succesful', () async {
      //   final DocumentSnapshot tSnapshot1 = await firestore.doc('$_serviceItemCollectionName/${tServiceItem.id}').get();
      //   expect((tSnapshot1.get('inventories') as Map<String, dynamic>).length, 1, reason: 'initial DB empty');

      //   await tRepository.addServiceItemInventory(
      //       serviceItem: tServiceItem, serviceItemInventory: tServiceItemInventory2);

      //   final DocumentSnapshot tSnapshot2 = await firestore.doc('$_serviceItemCollectionName/${tServiceItem.id}').get();
      //   expect((tSnapshot2.get('inventories') as Map<String, dynamic>).length, 2, reason: 'adding new inventory fails');

      //   await tRepository.addServiceItemInventory(
      //       serviceItem: tServiceItem, serviceItemInventory: tServiceItemInventory2);

      //   final DocumentSnapshot tSnapshot3 = await firestore.doc('$_serviceItemCollectionName/${tServiceItem.id}').get();
      //   expect((tSnapshot3.get('inventories') as Map<String, dynamic>).length, 2, reason: 'added duplicate inventory ');
      // });

      // test('createServiceItem successful', () async {
      //   final QuerySnapshot tSnapshots1 = await firestore
      //       .collection('projects/$tProjectId1/contracts/$tContractId1/$_serviceItemCollectionName')
      //       .get();
      //   expect(tSnapshots1.docs.length, 1);
      //
      //   await tRepository.create(
      //     projectId: tProjectId1,
      //     contractId: tContractId1,
      //     specification: tSpecification1,
      //     startAt: tStartDate,
      //     endPlannedAt: tEndDate,
      //     inventories: tList,
      //   );
      //
      //   final QuerySnapshot tSnapshots2 = await firestore
      //       .collection('projects/$tProjectId1/contracts/$tContractId1/$_serviceItemCollectionName')
      //       .get();
      //   expect(tSnapshots2.docs.length, 2);
      // });
    }
  });
}
