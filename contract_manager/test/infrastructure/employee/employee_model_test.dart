import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/infrastructure/employee/employee_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const String idKey = 'id';
  const String nameKey = 'name';
  const String roleKey = 'role';
  const String companyIdKey = 'companyId';

  const String tId1 = '1';

  const String tName1 = 'a';
  const String tName2 = 'b';

  const String tCompanyId1 = 'tCompanyId1';

  const EmployeeRole tRole1 = EmployeeRole.admin;
  const EmployeeRole tRole2 = EmployeeRole.read;

  const String tRoleS1 = 'admin';

  const Employee tEmployee1 = EmployeeModel(
    id: tId1,
    name: tName1,
    role: tRole1,
    companyId: tCompanyId1,
  );

  const Employee tEmployee2 = EmployeeModel(
    id: tId1,
    name: tName2,
    role: tRole2,
    companyId: tCompanyId1,
  );

  final Map<String, dynamic> tJson = <String, dynamic>{
    idKey: tId1,
    nameKey: tName1,
    roleKey: tRoleS1,
    companyIdKey: tCompanyId1,
  };

  group('EmployeeModel.fromJson', () {
    test('successful', () {
      final Employee tEmployeeModel = EmployeeModel.fromJson(tJson);
      expect(tEmployeeModel, tEmployee1);
    });

    test('failing, because of missing key', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{};

        EmployeeModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        final List<String> tMissingKeys = <String>[
          idKey,
          nameKey,
          roleKey,
          companyIdKey,
        ];

        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });

    test('failing, because of null', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{
          idKey: null,
          nameKey: null,
          roleKey: null,
          companyIdKey: null
        };

        EmployeeModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on DisallowedNullValueException catch (e) {
        final List<String> tNotNullKeys = <String>[
          idKey,
          nameKey,
          roleKey,
          companyIdKey,
        ];

        for (final String key in e.keysWithNullValues) {
          expect(tNotNullKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });

  group('Employee.toJson', () {
    test('successful', () {
      final Map<String, dynamic> tJsonFromModel = tEmployee1.toJson();
      expect(tJsonFromModel, tJson);
    });

    test('fromJson -> toJson == Json', () {
      final Employee tEmployeeFromJson = EmployeeModel.fromJson(tJson);
      final Map<String, dynamic> tJsonFromModel = tEmployeeFromJson.toJson();
      expect(tJsonFromModel, tJson);
    });
  });

  group('EmployeeModel.copyWith', () {
    test('Empty', () {
      final Employee tEmployeeResult = tEmployee1.copyWith();
      expect(tEmployeeResult, tEmployee1);
    });

    test('Aöö', () {
      final Employee tEmployeeResult = tEmployee1.copyWith(
        name: tName2,
        role: tRole2,
      );
      expect(tEmployeeResult, tEmployee2);
    });
  });
}
