import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/infrastructure/service_item/report_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  final List<String> imagePaths = ['0', '1'];
  final List<String> imagePathsChanged = ['c', 'h', 'a', 'n', 'g', 'e', 'd'];

  final Map<String, dynamic> tReportJ = <String, dynamic>{
    'authorId': 'authorId',
    'message': 'message',
    'imagePaths': ['0', '1'],
    'isError': false,
  };

  final Report tReport = ReportModel(
    authorId: 'authorId',
    message: 'message',
    imagePaths: BuiltList<String>(imagePaths),
    isError: false,
  );
  final Report tReportChanged = tReport.copyWith(
    authorId: 'changed',
    message: 'changed',
    imagePaths: BuiltList<String>(imagePathsChanged),
    isError: true,
  );
  final Report tReportDifferent = ReportModel(
    authorId: 'changed',
    message: 'changed',
    imagePaths: BuiltList<String>(imagePathsChanged),
    isError: true,
  );

  group('ReporttModel.fromJson', () {
    test('Successfull', () {
      expect(ReportModel.fromJson(tReportJ), tReport);
    });

    test('Failing, because missing keys', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{};
        ReportModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        for (final String key in e.missingKeys) {
          final List<String> tMissingKeys = <String>[
            'authorId',
            'message',
            'imagePaths',
            'isError',
          ];
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
    test('Failing, because nullable', () {
      try {
        final Map<String, dynamic> errorJson2 = <String, dynamic>{
          'authorId': null,
          'message': null,
          'imagePaths': null,
          'isError': null,
        };
        ReportModel.fromJson(errorJson2);
        expect(false, isTrue, reason: 'No exception.');
      } on DisallowedNullValueException catch (e) {
        final List<String> dissallowedNullValueKeys = <String>[
          'authorId',
          'message',
          'imagePaths',
          'isError',
        ];
        for (final String key in e.keysWithNullValues) {
          expect(dissallowedNullValueKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });

  group('ReporttModel.toJson', () {
    test('Successful', () {
      expect(tReport.toJson(), tReportJ);
    });

    test('ReporttModel.fromJson -> ReporttModel.toJson == tJson', () {
      expect(ReportModel.fromJson(tReportJ).toJson(), tReportJ);
    });
  });

  group('ReportModel.copyWith', () {
    test('Empty', () {
      expect(tReport.copyWith(), tReport);
    });

    test('All', () {
      expect(tReportChanged, tReportDifferent);
    });
  });
}
