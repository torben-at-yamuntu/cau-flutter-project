import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/company/company_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const String idKey = 'id';
  const String nameKey = 'name';
  const String bankInfoKey = 'bankInfo';
  const String addressKey = 'address';

  const String tId1 = 'tId1';
  const String tId2 = 'tId2';

  const String tName1 = 'tName1';
  const String tName2 = 'tName2';

  const BankInfo tbankInfo1 = BankInfoModel(
    iban: 'testIban',
    owner: 'testOwner',
    bic: 'testBic',
  );

  const BankInfo tbankInfo2 = BankInfoModel(
    iban: 'testIban1',
    owner: 'testOwner1',
    bic: 'testBic1',
  );

  const Address tAddress1 = AddressModel(
    country: 'tCountry1',
    city: 'tCity1',
    zip: 1,
    street: 'tStreet1',
    houseNumber: 10,
  );

  const Address tAddress2 = AddressModel(
    country: 'tCountry2',
    city: 'tCity2',
    zip: 2,
    street: 'tStreet2',
    houseNumber: 20,
  );

  const Company tCompany1 = CompanyModel(
    id: tId1,
    name: tName1,
    bankInfo: tbankInfo1,
    address: tAddress1,
  );

  const Company tCompany2 = CompanyModel(
    id: tId2,
    name: tName2,
    bankInfo: tbankInfo2,
    address: tAddress2,
  );

  final Map<String, dynamic> tJson = <String, dynamic>{
    idKey: tId1,
    nameKey: tName1,
    bankInfoKey: tbankInfo1.toJson(),
    addressKey: tAddress1.toJson(),
  };

  group('Company', () {
    group('fromJson', () {
      test('succesful', () {
        final Company tCompanyModel = CompanyModel.fromJson(tJson);
        expect(tCompanyModel, tCompany1);
      });

      test('failing, because missing keys', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{};

          CompanyModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on MissingRequiredKeysException catch (e) {
          final List<String> tMissingKeys = <String>[
            idKey,
            nameKey,
            bankInfoKey,
            addressKey,
          ];

          for (final String key in e.missingKeys) {
            expect(tMissingKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });

      test('failing, because dissallowed null', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{
            idKey: null,
            nameKey: null,
            bankInfoKey: null,
            addressKey: null,
          };

          CompanyModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on DisallowedNullValueException catch (e) {
          final List<String> dissallowedNullValueKeys = <String>[
            idKey,
            nameKey,
            bankInfoKey,
            addressKey,
          ];

          for (final String key in e.keysWithNullValues) {
            expect(dissallowedNullValueKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });
    });

    group('toJson', () {
      test('successful', () {
        final Map<String, dynamic> tJsonFromModel = tCompany1.toJson();
        expect(tJsonFromModel, tJson);
      });

      test('fromJson -> toJson == tJson', () {
        final Company tCompanyFromJson = CompanyModel.fromJson(tJson);
        final Map<String, dynamic> tJsonFromModel = tCompanyFromJson.toJson();
        expect(tJsonFromModel, tJson);
      });
    });

    group('copyWith', () {
      test('Empty', () {
        final Company tCompanyResult = tCompany1.copyWith();
        expect(tCompanyResult, tCompany1);
      });

      test('All', () {
        final Company tCompanyResult = tCompany1.copyWith(
          id: tId2,
          name: tName2,
          bankInfo: tbankInfo2,
          address: tAddress2,
        );
        expect(tCompanyResult, tCompany2);
      });
    });
  });
}
