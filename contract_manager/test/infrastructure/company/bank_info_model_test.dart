import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const BankInfo tbankInfo = BankInfoModel(
    iban: 'testIban',
    owner: 'testOwner',
    bic: 'testBic',
  );

  const Map<String, dynamic> tbankInfoJ = <String, dynamic>{
    'iban': 'testIban',
    'owner': 'testOwner',
    'bic': 'testBic',
  };

  final Map<String, dynamic> tEmpty = <String, dynamic>{};

  final List<String> tMissingKeys = <String>[
    'iban',
    'owner',
    'bic',
  ];

  final BankInfo tResult = tbankInfo.copyWith(
    iban: 'test',
    owner: 'test',
    bic: 'test',
  );

  const BankInfo tExpected = BankInfoModel(
    iban: 'test',
    owner: 'test',
    bic: 'test',
  );
  group('BankInfoModel.fromJson', () {
    test('Successful', () {
      expect(tbankInfo, BankInfoModel.fromJson(tbankInfoJ));
    });
    test('Failing', () {
      try {
        BankInfoModel.fromJson(tEmpty);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });
  group('BankInfoModel.toJson', () {
    test('Success', () {
      expect(tbankInfo.toJson(), tbankInfoJ);
    });
    test('Success: Circle translation', () {
      expect(BankInfoModel.fromJson(tbankInfoJ).toJson(), tbankInfoJ);
    });
  });
  group('BankInfoModel.copyWith', () {
    test('Empty copy', () {
      expect(tbankInfo.copyWith(), tbankInfo);
    });

    test('New input copy', () {
      expect(tResult, tExpected);
    });
  });
}
