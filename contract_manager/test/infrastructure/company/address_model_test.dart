import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const String countryKey = 'country';
  const String cityKey = 'city';
  const String zipKey = 'zip';
  const String streetKey = 'street';
  const String houseNumberKey = 'houseNumber';

  const String tCountry1 = 'tCountry1';
  const String tCountry2 = 'tCountry2';

  const String tCity1 = 'tCity1';
  const String tCity2 = 'tCity2';

  const int tZip1 = 1;
  const int tZip2 = 2;

  const String tStreet1 = 'tStreet1';
  const String tStreet2 = 'tStreet2';

  const int tHouseNumber1 = 10;
  const int tHouseNumber2 = 20;

  const Address tAddress1 = AddressModel(
    country: tCountry1,
    city: tCity1,
    zip: tZip1,
    street: tStreet1,
    houseNumber: tHouseNumber1,
  );

  const Address tAddress2 = AddressModel(
    country: tCountry2,
    city: tCity2,
    zip: tZip2,
    street: tStreet2,
    houseNumber: tHouseNumber2,
  );

  const Map<String, dynamic> tJson = <String, dynamic>{
    countryKey: tCountry1,
    cityKey: tCity1,
    zipKey: tZip1,
    streetKey: tStreet1,
    houseNumberKey: tHouseNumber1,
  };

  group('Address', () {
    group('fromJson', () {
      test('succesful', () {
        final Address tAddressModel = AddressModel.fromJson(tJson);
        expect(tAddressModel, tAddress1);
      });

      test('failing, because missing keys', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{};

          AddressModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on MissingRequiredKeysException catch (e) {
          final List<String> tMissingKeys = <String>[
            countryKey,
            cityKey,
            zipKey,
            streetKey,
            houseNumberKey,
          ];

          for (final String key in e.missingKeys) {
            expect(tMissingKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });

      test('failing, because dissallowed null', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{
            countryKey: null,
            cityKey: null,
            zipKey: null,
            streetKey: null,
            houseNumberKey: null,
          };

          AddressModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on DisallowedNullValueException catch (e) {
          final List<String> dissallowedNullValueKeys = <String>[
            countryKey,
            cityKey,
            zipKey,
            streetKey,
            houseNumberKey,
          ];

          for (final String key in e.keysWithNullValues) {
            expect(dissallowedNullValueKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });
    });

    group('toJson', () {
      test('successful', () {
        final Map<String, dynamic> tJsonFromModel = tAddress1.toJson();
        expect(tJsonFromModel, tJson);
      });

      test('fromJson -> toJson == tJson', () {
        final Address tAddressFromJson = AddressModel.fromJson(tJson);
        final Map<String, dynamic> tJsonFromModel = tAddressFromJson.toJson();
        expect(tJsonFromModel, tJson);
      });
    });

    group('copyWith', () {
      test('Empty', () {
        final Address tAddressResult = tAddress1.copyWith();
        expect(tAddressResult, tAddress1);
      });

      test('All', () {
        final Address tAddressResult = tAddress1.copyWith(
          country: tCountry2,
          city: tCity2,
          zip: tZip2,
          street: tStreet2,
          houseNumber: tHouseNumber2,
        );
        expect(tAddressResult, tAddress2);
      });
    });
  });
}
