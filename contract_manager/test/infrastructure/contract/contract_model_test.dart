import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/contract/contract_model.dart';
import 'package:contract_manager/infrastructure/service_item/payment_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const String idKey = 'id';
  const String projectIdKey = 'projectId';
  const String createdAtKey = 'createdAt';
  const String contractorIdKey = 'contractorId';
  const String clientIdKey = 'clientId';
  const String acceptedByKey = 'acceptedBy';
  const String specificationKey = 'specification';
  const String bankInfoKey = 'bankInfo';
  const String costsKey = 'costs';
  const String versionNumberKey = 'versionNumber';
  const String lastSeenVersionKey = 'lastSeenVersions';

  const String tId1 = 'tId1';

  const String tProjectId = 'projectId';

  const int tTime1 = 1;

  final DateTime tDateTime1 = DateTime.fromMillisecondsSinceEpoch(tTime1);

  final Timestamp tTimestamp = Timestamp.fromMillisecondsSinceEpoch(tTime1);

  const String tContractorId1 = 'tContractorId1';

  const String tClientId1 = 'tClient1';

  const String? tAcceptedBy1 = 'user1';
  const String? tAcceptedBy2 = 'user2';

  const String tSpecification1 = 'tSpecification1';

  const String tIban1 = 'tIban1';
  const String tIban2 = 'tIban2';

  const String tOwner1 = 'tOwner1';
  const String tOwner2 = 'tOwner2';

  const String tBic1 = 'tBic1';
  const String tBic2 = 'tBic2';

  const BankInfo tBankInfo1 = BankInfoModel(iban: tIban1, owner: tOwner1, bic: tBic1);
  const BankInfo tBankInfo2 = BankInfoModel(iban: tIban2, owner: tOwner2, bic: tBic2);

  const double tAmount1 = 1;
  const double tAmount2 = 2;

  final Payment tPayment1 = PaymentModel(
    id: tId1,
    amount: tAmount1,
    serviceItemIds: BuiltList<String>(<String>[tId1]),
    sentPaymentAt: tDateTime1,
    paymentActiveAt: tDateTime1,
    receivedPaymentAt: tDateTime1,
  );
  final Payment tPayment2 = PaymentModel(
    id: tId1,
    amount: tAmount2,
    serviceItemIds: BuiltList<String>(<String>[tId1]),
    sentPaymentAt: tDateTime1,
    paymentActiveAt: tDateTime1,
    receivedPaymentAt: tDateTime1,
  );

  final BuiltList<Payment> tCosts1 = BuiltList<Payment>(<Payment>[tPayment1]);
  final BuiltList<Payment> tCosts2 = BuiltList<Payment>(<Payment>[tPayment2]);

  const int tVersionNumber1 = 1;
  const int tVersionNumber2 = 2;

  final BuiltMap<String, int> tLastSeenVersions1 = BuiltMap(<String, int>{
    tOwner1: tVersionNumber1,
  });

  final BuiltMap<String, int> tLastSeenVersions2 = BuiltMap(<String, int>{
    tOwner1: tVersionNumber2,
  });

  final Contract tContract1 = ContractModel(
    id: tId1,
    projectId: tProjectId,
    createdAt: tDateTime1,
    contractorId: tContractorId1,
    clientId: tClientId1,
    specification: tSpecification1,
    bankInfo: tBankInfo1,
    costs: tCosts1,
    versionNumber: tVersionNumber1,
    lastSeenVersions: tLastSeenVersions1,
    acceptedBy: tAcceptedBy1,
    name: 'tContractName1',
  );

  final Contract tContract2 = ContractModel(
    id: tId1,
    projectId: tProjectId,
    createdAt: tDateTime1,
    contractorId: tContractorId1,
    clientId: tClientId1,
    specification: tSpecification1,
    bankInfo: tBankInfo2,
    costs: tCosts2,
    versionNumber: tVersionNumber1,
    lastSeenVersions: tLastSeenVersions2,
    acceptedBy: tAcceptedBy2,
    name: 'tContractName1',
  );

  final Map<String, int> tLastSeenVersions1Json = <String, int>{
    tOwner1: tVersionNumber1,
  };

  final Map<String, dynamic> tJson = <String, dynamic>{
    idKey: tId1,
    projectIdKey: tProjectId,
    createdAtKey: tTimestamp,
    contractorIdKey: tContractorId1,
    clientIdKey: tClientId1,
    acceptedByKey: tAcceptedBy1,
    specificationKey: tSpecification1,
    bankInfoKey: tBankInfo1.toJson(),
    costsKey: tCosts1.map((payment) => payment.toJson()).toList(),
    versionNumberKey: tVersionNumber1,
    lastSeenVersionKey: tLastSeenVersions1Json,
    'name': 'tContractName1',
  };

  group('ContractModel', () {
    group('fromJson', () {
      test('succesful', () {
        final Contract tContractModel = ContractModel.fromJson(tJson);
        expect(tContractModel, tContract1);
      });

      test('failing, because missing keys', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{};

          ContractModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on MissingRequiredKeysException catch (e) {
          final Set<String> tMissingKeys = <String>{
            idKey,
            projectIdKey,
            createdAtKey,
            contractorIdKey,
            clientIdKey,
            specificationKey,
            bankInfoKey,
            costsKey,
            lastSeenVersionKey,
            versionNumberKey,
            acceptedByKey,
            'name',
          };

          expect(e.missingKeys.length, equals(tMissingKeys.length));

          for (final String key in e.missingKeys) {
            expect(tMissingKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });

      test('failing, because dissallowed null', () {
        try {
          final Map<String, dynamic> errorJson = <String, dynamic>{
            idKey: null,
            projectIdKey: null,
            createdAtKey: null,
            contractorIdKey: null,
            clientIdKey: null,
            specificationKey: null,
            bankInfoKey: null,
            costsKey: null,
            versionNumberKey: null,
            lastSeenVersionKey: null,
            acceptedByKey: null,
            'name': null
          };

          ContractModel.fromJson(errorJson);
          expect(false, isTrue, reason: 'No exception.');
        } on DisallowedNullValueException catch (e) {
          final Set<String> disallowedNullValueKeys = <String>{
            idKey,
            projectIdKey,
            createdAtKey,
            contractorIdKey,
            clientIdKey,
            bankInfoKey,
            costsKey,
            versionNumberKey,
            lastSeenVersionKey,
            'name',
          };

          expect(e.keysWithNullValues.length, equals(disallowedNullValueKeys.length));

          for (final String key in e.keysWithNullValues) {
            expect(disallowedNullValueKeys.contains(key), isTrue);
          }
        } catch (e) {
          expect(false, isTrue, reason: 'Different exception: $e');
        }
      });
    });

    group('toJson', () {
      test('toJson fromJson == tContract', () {
        final Map<String, dynamic> tJsonFromModel = tContract1.toJson();
        final Contract tContractFromJson = ContractModel.fromJson(tJsonFromModel);
        expect(tContractFromJson, tContract1);
      });
    });

    group('copyWith', () {
      test('Empty', () {
        final Contract tContractResult = tContract1.copyWith();
        expect(tContractResult, tContract1);
      });

      test('All', () {
        final Contract tContractResult = tContract1.copyWith(
          bankInfo: tBankInfo2,
          costs: tCosts2,
          lastSeenVersions: tLastSeenVersions2,
          acceptedBy: tAcceptedBy2,
        );
        expect(tContractResult, equals(tContract2));
      });
    });
  });
}
