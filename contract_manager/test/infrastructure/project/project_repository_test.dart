import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:contract_manager/domain/project/i_project_repository.dart';
import 'package:contract_manager/infrastructure/project/project_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import '../global_test_data/project.dart';

void main() {
  //Mock environment
  late MockFirestoreInstance firestore;
  late IProjectRepository tRepository;

  const String _projectCollectionName = 'projects';

  // Put a single project in mocked database
  setUp(() async {
    firestore = MockFirestoreInstance();
    tRepository = ProjectRepository(firestore);

    await firestore.doc('$_projectCollectionName/${tProject1.id}').set(tProject1.toJson());
  });

  group('ProjectRepository', () {
    test('create successful', () async {
      // Should only be 1 project in database
      final QuerySnapshot tSnapshot1 = await firestore.collection(_projectCollectionName).get();
      expect(tSnapshot1.docs.length, 1);

      await tRepository.create(companyId: tCompanyID2, name: tName2);
      // After adding, should be 2 projects in database
      final QuerySnapshot tSnapshot2 = await firestore.collection(_projectCollectionName).get();
      expect(tSnapshot2.docs.length, 2);
    });
  });
}
