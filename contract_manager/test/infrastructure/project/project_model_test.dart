import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/infrastructure/project/project_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  const int tTime1 = 0;

  const String idKey = 'id';
  const String createdAtKey = 'createdAt';
  const String companyIDKey = 'companyId';
  const String nameKey = 'name';

  const String tId1 = '1';

  final DateTime tDateTime1 = DateTime.fromMillisecondsSinceEpoch(tTime1);

  final Timestamp tTimeStamp = Timestamp.fromMillisecondsSinceEpoch(tTime1);

  const String tCompanyID1 = 'id1';

  const String tName1 = 'tName1';
  const String tName2 = 'tName2';

  final Project tProject1 = ProjectModel(
    id: tId1,
    createdAt: tDateTime1,
    companyId: tCompanyID1,
    name: tName1,
  );

  final Project tProject2 = ProjectModel(
    id: tId1,
    createdAt: tDateTime1,
    companyId: tCompanyID1,
    name: tName2,
  );

  final Map<String, dynamic> tJson = <String, dynamic>{
    idKey: tId1,
    createdAtKey: tTimeStamp,
    companyIDKey: tCompanyID1,
    nameKey: tName1,
  };

  group('ProjectModel.fromJson', () {
    test('successfull', () {
      final Project tProjectModel = ProjectModel.fromJson(tJson);
      expect(tProjectModel, tProject1);
    });

    test('failing, because missing keys', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{};

        ProjectModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on MissingRequiredKeysException catch (e) {
        final List<String> tMissingKeys = <String>[
          idKey,
          createdAtKey,
          companyIDKey,
          nameKey,
        ];

        for (final String key in e.missingKeys) {
          expect(tMissingKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
    test('failing, because nullable', () {
      try {
        final Map<String, dynamic> errorJson = <String, dynamic>{
          idKey: null,
          createdAtKey: null,
          companyIDKey: null,
          nameKey: null,
        };

        ProjectModel.fromJson(errorJson);
        expect(false, isTrue, reason: 'No exception.');
      } on DisallowedNullValueException catch (e) {
        final List<String> disallowedNullKeys = <String>[
          idKey,
          createdAtKey,
          companyIDKey,
          nameKey
        ];

        for (final String key in e.keysWithNullValues) {
          expect(disallowedNullKeys.contains(key), isTrue);
        }
      } catch (e) {
        expect(false, isTrue, reason: 'Different exception: $e');
      }
    });
  });

  group('ProjectModel.toJson', () {
    test('successful', () {
      final Map<String, dynamic> tJsonFromModel = tProject1.toJson();
      expect(tJsonFromModel, tJson);
    });

    test('ProjectModel.fromJson -> ProjectModel.toJson == tJson', () {
      final Project tProjectFromJson = ProjectModel.fromJson(tJson);
      final Map<String, dynamic> tJsonFromModel = tProjectFromJson.toJson();
      expect(tJsonFromModel, tJson);
    });
  });

  group('ProjectModel.copyWith', () {
    test('Empty', () {
      final Project tProjectResult = tProject1.copyWith();
      expect(tProjectResult, tProject1);
    });

    test('All', () {
      final Project tProjectResult = tProject1.copyWith(name: tName2);

      expect(tProjectResult, tProject2);
    });
  });
}
