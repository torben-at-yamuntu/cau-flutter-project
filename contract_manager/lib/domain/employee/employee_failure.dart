import 'package:freezed_annotation/freezed_annotation.dart';

part 'employee_failure.freezed.dart';

@freezed
class EmployeeFailure with _$EmployeeFailure {
  const factory EmployeeFailure.unexpected() = UnexpectedEmployeeFailure;
}
