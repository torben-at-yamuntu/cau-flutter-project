import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';

/// The repository to interact with [Employee].
abstract class IEmployeeRepository {
  /// Returns a realtime data stream of all [Employee]s with [Company.id] equals [companyId].
  Stream<BuiltList<Employee>> watchEmployeesOfCompany({
    required String companyId,
  });

  /// Returns a realtime data stream of the [Employee] with [Employee.id] equals [employeeId].
  Stream<Employee> watchEmployee({
    required String employeeId,
  });

  /// Creates a new [Employee] with parameters [employeeId], [name] and [role]
  /// for the [Company] with [Company.id] equals [companyId].
  /// The [employeeId] must equal the id from the authentication service of the
  /// account the employee should use.
  Future<Employee> create({
    required String employeeId,
    required String name,
    required String companyId,
    required EmployeeRole role,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Deletes the [Employee] where [Employee.id] equals [employee.id].
  Future<void> deleteEmployee({
    required Employee employee,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Changes [employee.role] to [role].
  Future<void> changeRole({
    required EmployeeRole role,
    required Employee employee,
    IBatch? batch,
    ITransaction? transaction,
  });
}
