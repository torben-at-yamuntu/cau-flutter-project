// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'employee_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$EmployeeFailureTearOff {
  const _$EmployeeFailureTearOff();

  UnexpectedEmployeeFailure unexpected() {
    return const UnexpectedEmployeeFailure();
  }
}

/// @nodoc
const $EmployeeFailure = _$EmployeeFailureTearOff();

/// @nodoc
mixin _$EmployeeFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedEmployeeFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedEmployeeFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EmployeeFailureCopyWith<$Res> {
  factory $EmployeeFailureCopyWith(
          EmployeeFailure value, $Res Function(EmployeeFailure) then) =
      _$EmployeeFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$EmployeeFailureCopyWithImpl<$Res>
    implements $EmployeeFailureCopyWith<$Res> {
  _$EmployeeFailureCopyWithImpl(this._value, this._then);

  final EmployeeFailure _value;
  // ignore: unused_field
  final $Res Function(EmployeeFailure) _then;
}

/// @nodoc
abstract class $UnexpectedEmployeeFailureCopyWith<$Res> {
  factory $UnexpectedEmployeeFailureCopyWith(UnexpectedEmployeeFailure value,
          $Res Function(UnexpectedEmployeeFailure) then) =
      _$UnexpectedEmployeeFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedEmployeeFailureCopyWithImpl<$Res>
    extends _$EmployeeFailureCopyWithImpl<$Res>
    implements $UnexpectedEmployeeFailureCopyWith<$Res> {
  _$UnexpectedEmployeeFailureCopyWithImpl(UnexpectedEmployeeFailure _value,
      $Res Function(UnexpectedEmployeeFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedEmployeeFailure));

  @override
  UnexpectedEmployeeFailure get _value =>
      super._value as UnexpectedEmployeeFailure;
}

/// @nodoc
class _$UnexpectedEmployeeFailure implements UnexpectedEmployeeFailure {
  const _$UnexpectedEmployeeFailure();

  @override
  String toString() {
    return 'EmployeeFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedEmployeeFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedEmployeeFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedEmployeeFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedEmployeeFailure implements EmployeeFailure {
  const factory UnexpectedEmployeeFailure() = _$UnexpectedEmployeeFailure;
}
