import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Class for Employees of Companies.
abstract class Employee extends Entity {
  const Employee(
    this.id,
    this.companyId,
    this.name,
    this.role,
  );

  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final String companyId;

  @JsonKey(required: true, disallowNullValue: true)
  final String name;

  /// Rights of employee to read or change data in their company
  @JsonKey(required: true, disallowNullValue: true)
  final EmployeeRole role;

  @override
  List<Object?> get props => [id, name, role];

  Employee copyWith({String? name, EmployeeRole? role});
}

/// Different roles inside the [Company].
enum EmployeeRole { read, readWrite, admin }

extension EmployeeRoleX on EmployeeRole {
  String toPrettyString() {
    switch (this) {
      case EmployeeRole.read:
        return 'Lesen';
      case EmployeeRole.readWrite:
        return 'Schreiben';
      case EmployeeRole.admin:
        return 'Admin';
    }
  }
}
