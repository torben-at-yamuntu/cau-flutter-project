/// Interface for the AuthRepository
abstract class IAuthRepository {
  /// Returns the userId when logged in or `null` if not.
  String? getLoggedInUserId();

  /// Logs a user in with email and password
  Future<void> logIn(String email, String password);

  /// Logs a user out
  Future<void> logout();

  /// Register a new user with email and password
  Future<String> register(String email, String password);
}
