/// The domain layer is the heart of our application.
///
/// It defines the visibility of the presentation, application and
/// infrastructure layer to each other. That means, each layer just knows
/// what is defined in the domain layer but not how the other layers work
/// internally. This gives us the opportunity to replace our complete backend
/// for example without any changes in the domain, presentation and application
/// layer. This general concept is also known as
/// ["domain driven design"](https://de.wikipedia.org/wiki/Domain-driven_Design).
library domain_layer;

export 'auth/i_auth_repository.dart';
export 'batch/batch_failure.dart';
export 'batch/i_batch.dart';
export 'batch/i_batch_repository.dart';
export 'company/address.dart';
export 'company/bank_info.dart';
export 'company/company.dart';
export 'company/i_company_repository.dart';
export 'contract/contract.dart';
export 'contract/contract_failure.dart';
export 'contract/i_contract_repository.dart';
export 'core/entity.dart';
export 'core/use_case.dart';
export 'core/versionable_document.dart';
export 'employee/employee.dart';
export 'employee/i_employee_repository.dart';
export 'project/i_project_repository.dart';
export 'project/project.dart';
export 'project/project_failure.dart';
export 'service_item/i_service_item_repository.dart';
export 'service_item/payment.dart';
export 'service_item/report.dart';
export 'service_item/service_item.dart';
export 'service_item/service_item_inventory.dart';
export 'storage/i_storage_repository.dart';
export 'transaction/i_transaction.dart';
export 'transaction/i_transaction_repository.dart';
export 'transaction/transaction_failure.dart';
