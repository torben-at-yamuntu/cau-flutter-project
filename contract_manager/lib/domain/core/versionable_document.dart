import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Represents version of documentents
abstract class VersionableDocument extends Entity {
  ///Constructs a VersionableDocument.
  ///@param versionnumber Current versionnumber
  ///@param lastSeenVersions Map of Versions with UserID, and his last seen versionnumber
  const VersionableDocument(this.versionNumber, this.lastSeenVersions);

  @JsonKey(required: true, disallowNullValue: true)
  final int versionNumber;

  @JsonKey(required: true, disallowNullValue: true)
  final BuiltMap<String, int> lastSeenVersions;

  /// Returns if the user has seen the newest Version.
  /// @param User to look up.
  bool hasSeenLatestVersion(String userID) => lastSeenVersions[userID] == versionNumber;

  @override
  List<Object?> get props => [versionNumber, lastSeenVersions];
}
