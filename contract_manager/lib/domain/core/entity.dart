import 'package:equatable/equatable.dart';

/// Interface for JSON representation.
abstract class Entity extends Equatable {
  const Entity();

  Map<String, dynamic> toJson();

  Map<String, dynamic> jsonFlatDiff(Entity change) {
    assert(runtimeType == change.runtimeType, 'Types of Entities must be the same!');

    final Map<String, dynamic> currentJson = toJson();
    final Map<String, dynamic> changedJson = change.toJson();

    final Map<String, dynamic> currentFlatJson = _flatten(currentJson);
    return _flatten(changedJson)
      ..removeWhere((key, dynamic value) => currentFlatJson[key] == value);
  }

  Map<String, dynamic> _flatten(
    Map<String, dynamic> target, {
    String delimiter = '.',
    bool safe = false,
    int? maxDepth,
  }) {
    final result = <String, dynamic>{};

    void step(
      Map<String, dynamic> obj, [
      String? previousKey,
      int currentDepth = 1,
    ]) {
      obj.forEach((key, dynamic value) {
        final newKey = previousKey != null ? '$previousKey$delimiter$key' : key;

        if (maxDepth != null && currentDepth >= maxDepth) {
          result[newKey] = value;
          return;
        }
        if (value is Map<String, Object>) {
          return step(value, newKey, currentDepth + 1);
        }
        if (value is List && !safe) {
          return step(
            _listToMap<dynamic>(value as List<Object>),
            newKey,
            currentDepth + 1,
          );
        }
        result[newKey] = value;
      });
    }

    step(target);

    return result;
  }

  Map<String, T> _listToMap<T>(List<T> list) =>
      list.asMap().map((key, value) => MapEntry(key.toString(), value));
}
