import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/counter/counter.dart';
import 'package:contract_manager/domain/counter/counter_failure.dart';
import 'package:dartz/dartz.dart';

abstract class ICounterRepository {
  Stream<BuiltList<Counter>> watchCounters();

  Future<Either<CounterFailure, Unit>> create();

  Future<Either<CounterFailure, Unit>> increment(String counterId);
}
