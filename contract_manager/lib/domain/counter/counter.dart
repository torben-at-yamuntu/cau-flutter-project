import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

abstract class Counter extends Equatable {
  const Counter(this.createdAt, this.id, this.value);

  @JsonKey(required: true, disallowNullValue: true)
  final DateTime createdAt;

  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final int value;

  @override
  List<Object> get props => [createdAt, id, value];

  Counter copyWith({DateTime? createdAt, String? id, int? value});
}
