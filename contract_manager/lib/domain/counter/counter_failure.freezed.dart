// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'counter_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CounterFailureTearOff {
  const _$CounterFailureTearOff();

  UnexpectedCounterFailure unexpected() {
    return const UnexpectedCounterFailure();
  }
}

/// @nodoc
const $CounterFailure = _$CounterFailureTearOff();

/// @nodoc
mixin _$CounterFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedCounterFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedCounterFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CounterFailureCopyWith<$Res> {
  factory $CounterFailureCopyWith(
          CounterFailure value, $Res Function(CounterFailure) then) =
      _$CounterFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$CounterFailureCopyWithImpl<$Res>
    implements $CounterFailureCopyWith<$Res> {
  _$CounterFailureCopyWithImpl(this._value, this._then);

  final CounterFailure _value;
  // ignore: unused_field
  final $Res Function(CounterFailure) _then;
}

/// @nodoc
abstract class $UnexpectedCounterFailureCopyWith<$Res> {
  factory $UnexpectedCounterFailureCopyWith(UnexpectedCounterFailure value,
          $Res Function(UnexpectedCounterFailure) then) =
      _$UnexpectedCounterFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedCounterFailureCopyWithImpl<$Res>
    extends _$CounterFailureCopyWithImpl<$Res>
    implements $UnexpectedCounterFailureCopyWith<$Res> {
  _$UnexpectedCounterFailureCopyWithImpl(UnexpectedCounterFailure _value,
      $Res Function(UnexpectedCounterFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedCounterFailure));

  @override
  UnexpectedCounterFailure get _value =>
      super._value as UnexpectedCounterFailure;
}

/// @nodoc
class _$UnexpectedCounterFailure implements UnexpectedCounterFailure {
  const _$UnexpectedCounterFailure();

  @override
  String toString() {
    return 'CounterFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedCounterFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedCounterFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedCounterFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedCounterFailure implements CounterFailure {
  const factory UnexpectedCounterFailure() = _$UnexpectedCounterFailure;
}
