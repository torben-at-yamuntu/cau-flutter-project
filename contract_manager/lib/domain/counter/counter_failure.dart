import 'package:freezed_annotation/freezed_annotation.dart';

part 'counter_failure.freezed.dart';

@freezed
abstract class CounterFailure with _$CounterFailure {
  const factory CounterFailure.unexpected() = UnexpectedCounterFailure;
}
