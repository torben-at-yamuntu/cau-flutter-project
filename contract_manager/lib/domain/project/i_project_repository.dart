import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/domain/project/project_failure.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';

/// The repository to interact with [Project].
abstract class IProjectRepository {
  /// Creates a new [Project] with parameters:
  /// [Project.companyId] equals [companyId]
  /// [Project.name] equals [name]
  /// Throws [ProjectFailure] when something went wrong.
  Future<Project> create({
    required String companyId,
    required String name,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Returns the [Project] where [Project.id] equals [projectId].
  /// Throws [ProjectFailure] when something went wrong.
  Future<Project> getProject({
    required String projectId,
    ITransaction? transaction,
  });

  /// Updates [project.name] to [name].
  /// Throws [ProjectFailure] when something went wrong.
  Future<void> updateName({
    required Project project,
    required String name,
    IBatch? batch,
    ITransaction? transaction,
  });
}
