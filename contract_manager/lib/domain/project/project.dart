import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Project for Contracts.
abstract class Project extends Entity {
  const Project(
    this.id,
    this.createdAt,
    this.companyId,
    this.name,
  );

  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final DateTime createdAt;

  /// The Company which is leading the [Project]
  @JsonKey(required: true, disallowNullValue: true)
  final String companyId;

  /// The name of the [Project]
  @JsonKey(required: true, disallowNullValue: true)
  final String name;

  @override
  List<Object?> get props => [
        id,
        createdAt,
        companyId,
        name,
      ];

  Project copyWith({
    String? name,
  });
}
