import 'package:contract_manager/domain/core/entity.dart';
import 'package:json_annotation/json_annotation.dart';

/// [BankInfo] which [Project] contains.
abstract class BankInfo extends Entity {
  const BankInfo(this.iban, this.owner, this.bic);

  @JsonKey(required: true, disallowNullValue: true)
  final String iban;

  @JsonKey(required: true, disallowNullValue: true)
  final String bic;

  /// The name of the owner of the bank account
  @JsonKey(required: true, disallowNullValue: true)
  final String owner;

  @override
  List<Object> get props => [iban, owner, bic];

  BankInfo copyWith({
    String? iban,
    String? owner,
    String? bic,
  });
}
