import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Class for Companies.
abstract class Company extends Entity {
  const Company(
    this.id,
    this.name,
    this.bankInfo,
    this.address,
  );
  @JsonKey(required: true, disallowNullValue: true)
  final String id;
  @JsonKey(required: true, disallowNullValue: true)
  final String name;
  @JsonKey(required: true, disallowNullValue: true)
  final Address address;

  /// Information about the connected bank account
  @JsonKey(required: true, disallowNullValue: true)
  final BankInfo bankInfo;

  @override
  List<Object?> get props => [id, name, bankInfo];

  Company copyWith({
    String? id,
    String? name,
    BankInfo? bankInfo,
    Address? address,
  });
}
