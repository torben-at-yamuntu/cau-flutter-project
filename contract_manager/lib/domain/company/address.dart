import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

abstract class Address extends Entity {
  const Address(
    this.country,
    this.city,
    this.zip,
    this.street,
    this.houseNumber,
  );
  @JsonKey(required: true, disallowNullValue: true)
  final String country;

  @JsonKey(required: true, disallowNullValue: true)
  final String city;

  @JsonKey(required: true, disallowNullValue: true)
  final int zip;

  @JsonKey(required: true, disallowNullValue: true)
  final String street;

  @JsonKey(required: true, disallowNullValue: true)
  final int houseNumber;

  @override
  List<Object?> get props => [street, houseNumber, zip, city, country];

  Address copyWith({
    String? country,
    String? city,
    int? zip,
    String? street,
    int? houseNumber,
  });
}
