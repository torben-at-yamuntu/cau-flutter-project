import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/company/company_failure.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';

/// The repository to interact with [Company].
abstract class ICompanyRepository {
  /// Returns a realtime data stream of the [Company] with [Company.id] equals [companyId].
  /// Throws [CompanyFailure] when something went wrong.
  Stream<Company> watchCompany({
    required String companyId,
  });

  /// Returns the [Company] with [Company.id] equals [companyId].
  /// Throws [CompanyFailure] when something went wrong.
  Future<Company> getCompany({
    required String companyId,
    ITransaction? transaction,
  });

  /// Creates a [Company] with parameters:
  /// [Company.name] equals [name]
  /// [Company.address] equals [address]
  /// [Company.bankInfo] equals [bankInfo].
  /// Throws [CompanyFailure] when something went wrong.
  Future<Company> create({
    required String name,
    required String country,
    required String city,
    required int zip,
    required String street,
    required int houseNumber,
    required String iban,
    required String bic,
    required String accountOwner,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Updates [company.bankInfo] to [bankInfo].
  /// Throws [CompanyFailure] when something went wrong.
  Future<void> updateBankInfo({
    required Company company,
    required BankInfo bankInfo,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Updates [company.address] to [address].
  /// Throws [CompanyFailure] when something went wrong.
  Future<void> updateAddress({
    required Company company,
    required Address address,
    IBatch? batch,
    ITransaction? transaction,
  });
}
