// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'company_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CompanyFailureTearOff {
  const _$CompanyFailureTearOff();

  UnexpectedCompanyFailure unexpected() {
    return const UnexpectedCompanyFailure();
  }
}

/// @nodoc
const $CompanyFailure = _$CompanyFailureTearOff();

/// @nodoc
mixin _$CompanyFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedCompanyFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedCompanyFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CompanyFailureCopyWith<$Res> {
  factory $CompanyFailureCopyWith(
          CompanyFailure value, $Res Function(CompanyFailure) then) =
      _$CompanyFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$CompanyFailureCopyWithImpl<$Res>
    implements $CompanyFailureCopyWith<$Res> {
  _$CompanyFailureCopyWithImpl(this._value, this._then);

  final CompanyFailure _value;
  // ignore: unused_field
  final $Res Function(CompanyFailure) _then;
}

/// @nodoc
abstract class $UnexpectedCompanyFailureCopyWith<$Res> {
  factory $UnexpectedCompanyFailureCopyWith(UnexpectedCompanyFailure value,
          $Res Function(UnexpectedCompanyFailure) then) =
      _$UnexpectedCompanyFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedCompanyFailureCopyWithImpl<$Res>
    extends _$CompanyFailureCopyWithImpl<$Res>
    implements $UnexpectedCompanyFailureCopyWith<$Res> {
  _$UnexpectedCompanyFailureCopyWithImpl(UnexpectedCompanyFailure _value,
      $Res Function(UnexpectedCompanyFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedCompanyFailure));

  @override
  UnexpectedCompanyFailure get _value =>
      super._value as UnexpectedCompanyFailure;
}

/// @nodoc
class _$UnexpectedCompanyFailure implements UnexpectedCompanyFailure {
  const _$UnexpectedCompanyFailure();

  @override
  String toString() {
    return 'CompanyFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedCompanyFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedCompanyFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedCompanyFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedCompanyFailure implements CompanyFailure {
  const factory UnexpectedCompanyFailure() = _$UnexpectedCompanyFailure;
}
