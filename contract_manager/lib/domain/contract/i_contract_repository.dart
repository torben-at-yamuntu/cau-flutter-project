import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';

/// The repository to interact with [Contract].
abstract class IContractRepository {
  /// Returns a realtime data stream of all [Contract]s with [Contract.clientId] or
  /// [Contract.contractorId] equals [companyId].
  Stream<BuiltList<Contract>> watchAllContractsWithCompanyParticipation({
    required String companyId,
  });

  /// Returns a realtime data stream of all [Contract]s with [Contract.id] equals [contractId] and
  /// [Contract.projectId] equals [projectId].
  Stream<Contract> watchContract({
    required String contractId,
    required String projectId,
  });

  /// Create a [Contract] with the parameters:
  /// [Contract.projectId] equals [projectId].
  /// [Contract.contractorId] equals [contractorId].
  /// [Contract.clientId] equals [clientId].
  /// [Contract.specification] equals [specification].
  /// [Contract.bankInfo] equals [bankInfo].
  /// Throws [ContractFailure] when something went wrong.
  Future<Contract> create({
    required String projectId,
    required String contractorId,
    required String clientId,
    required String specification,
    required BankInfo bankInfo,
    required String name,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Adds [payment] to [contract.costs].
  /// Throws [ContractFailure] when something went wrong.
  Future<void> addPayment({
    required Contract contract,
    required double amount,
    required List<String> serviceItemIds,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Sets [contract.acceptedBy] to [employeeId].
  /// There must be an [Employee] with [Employee.id] equals [employeeId] and is member of
  /// the [Company] where [Company.id] equals [contract.clientId].
  /// Throws [ContractFailure] when something went wrong.
  Future<void> acceptContract({
    required Contract contract,
    required String employeeId,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Sets [contract.lastSeenVersions[employeeId]] to [contract.versionNumber].
  /// Throws [ContractFailure] when something went wrong.
  Future<void> updateLastSeenVersion({
    required Contract contract,
    required String employeeId,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Increments [contract.versionNumber] by 1.
  /// Throws [ContractFailure] when something went wrong.
  Future<void> incrementVersion({
    required Contract contract,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<void> deleteContract({
    required Contract contract,
    IBatch? batch,
    ITransaction? transaction,
  });
  Future<void> setParentServiceItemId({
    required Contract contract,
    required String parentServiceItemId,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<Contract> getContractById({
    required String projectId,
    required String contractId,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<void> updatePaymentSentPaymentAt({
    required Contract contract,
    required String paymentId,
    required DateTime sentPaymentAt,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<void> updatePaymentReceivedPaymentAt({
    required Contract contract,
    required String paymentId,
    required DateTime receivedPaymentAt,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<void> updatePaymentPaymentActiveAt({
    required Contract contract,
    required String paymentId,
    required DateTime paymentActiveAt,
    IBatch? batch,
    ITransaction? transaction,
  });
}
