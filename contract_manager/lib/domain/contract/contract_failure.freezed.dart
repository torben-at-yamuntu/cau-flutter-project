// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'contract_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ContractFailureTearOff {
  const _$ContractFailureTearOff();

  UnexpectedContractFailure unexpected() {
    return const UnexpectedContractFailure();
  }
}

/// @nodoc
const $ContractFailure = _$ContractFailureTearOff();

/// @nodoc
mixin _$ContractFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedContractFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedContractFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContractFailureCopyWith<$Res> {
  factory $ContractFailureCopyWith(
          ContractFailure value, $Res Function(ContractFailure) then) =
      _$ContractFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContractFailureCopyWithImpl<$Res>
    implements $ContractFailureCopyWith<$Res> {
  _$ContractFailureCopyWithImpl(this._value, this._then);

  final ContractFailure _value;
  // ignore: unused_field
  final $Res Function(ContractFailure) _then;
}

/// @nodoc
abstract class $UnexpectedContractFailureCopyWith<$Res> {
  factory $UnexpectedContractFailureCopyWith(UnexpectedContractFailure value,
          $Res Function(UnexpectedContractFailure) then) =
      _$UnexpectedContractFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedContractFailureCopyWithImpl<$Res>
    extends _$ContractFailureCopyWithImpl<$Res>
    implements $UnexpectedContractFailureCopyWith<$Res> {
  _$UnexpectedContractFailureCopyWithImpl(UnexpectedContractFailure _value,
      $Res Function(UnexpectedContractFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedContractFailure));

  @override
  UnexpectedContractFailure get _value =>
      super._value as UnexpectedContractFailure;
}

/// @nodoc
class _$UnexpectedContractFailure implements UnexpectedContractFailure {
  const _$UnexpectedContractFailure();

  @override
  String toString() {
    return 'ContractFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedContractFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedContractFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedContractFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedContractFailure implements ContractFailure {
  const factory UnexpectedContractFailure() = _$UnexpectedContractFailure;
}
