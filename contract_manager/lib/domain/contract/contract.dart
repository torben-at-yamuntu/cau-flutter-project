import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/core/versionable_document.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Contracts to manage in a project.
abstract class Contract extends VersionableDocument {
  const Contract(
    this.id,
    this.projectId,
    this.createdAt,
    this.contractorId,
    this.clientId,
    this.acceptedBy,
    this.specification,
    this.bankInfo,
    this.costs,
    this.parentServiceItemId,
    this.name,
    int versionNumber,
    BuiltMap<String, int> lastSeenVersions,
  ) : super(versionNumber, lastSeenVersions);

  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final String projectId;

  @JsonKey(required: true, disallowNullValue: true)
  final DateTime createdAt;

  /// The [Company.id] of the contractor
  @JsonKey(required: true, disallowNullValue: true)
  final String contractorId;

  /// The [Company.id] of the client
  @JsonKey(required: true, disallowNullValue: true)
  final String clientId;

  @JsonKey(required: true)

  /// The [Employee.id] of the [Employee] who accepted the [Contract]
  final String? acceptedBy;
  @JsonKey(required: true)

  /// The content of the contract
  final String specification;
  @JsonKey(required: true, disallowNullValue: true)
  final BankInfo bankInfo;
  @JsonKey(required: true, disallowNullValue: true)
  final BuiltList<Payment> costs;

  bool get isAccepted => acceptedBy != null;

  final String? parentServiceItemId;

  @JsonKey(required: true, disallowNullValue: true)
  final String name;

  @override
  List<Object?> get props => [
        ...super.props,
        id,
        projectId,
        createdAt,
        contractorId,
        clientId,
        acceptedBy,
        specification,
        bankInfo,
        costs,
        parentServiceItemId,
        name,
      ];
  Contract copyWith({
    String? acceptedBy,
    BankInfo? bankInfo,
    BuiltList<Payment>? costs,
    BuiltMap<String, int>? lastSeenVersions,
    String? parentServiceItemId,
  });
}
