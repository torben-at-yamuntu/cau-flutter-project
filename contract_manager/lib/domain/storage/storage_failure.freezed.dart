// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'storage_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$StorageFailureTearOff {
  const _$StorageFailureTearOff();

  UnexpectedStorageFailure unexpected() {
    return const UnexpectedStorageFailure();
  }
}

/// @nodoc
const $StorageFailure = _$StorageFailureTearOff();

/// @nodoc
mixin _$StorageFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedStorageFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedStorageFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StorageFailureCopyWith<$Res> {
  factory $StorageFailureCopyWith(
          StorageFailure value, $Res Function(StorageFailure) then) =
      _$StorageFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$StorageFailureCopyWithImpl<$Res>
    implements $StorageFailureCopyWith<$Res> {
  _$StorageFailureCopyWithImpl(this._value, this._then);

  final StorageFailure _value;
  // ignore: unused_field
  final $Res Function(StorageFailure) _then;
}

/// @nodoc
abstract class $UnexpectedStorageFailureCopyWith<$Res> {
  factory $UnexpectedStorageFailureCopyWith(UnexpectedStorageFailure value,
          $Res Function(UnexpectedStorageFailure) then) =
      _$UnexpectedStorageFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedStorageFailureCopyWithImpl<$Res>
    extends _$StorageFailureCopyWithImpl<$Res>
    implements $UnexpectedStorageFailureCopyWith<$Res> {
  _$UnexpectedStorageFailureCopyWithImpl(UnexpectedStorageFailure _value,
      $Res Function(UnexpectedStorageFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedStorageFailure));

  @override
  UnexpectedStorageFailure get _value =>
      super._value as UnexpectedStorageFailure;
}

/// @nodoc
class _$UnexpectedStorageFailure implements UnexpectedStorageFailure {
  const _$UnexpectedStorageFailure();

  @override
  String toString() {
    return 'StorageFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedStorageFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedStorageFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedStorageFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedStorageFailure implements StorageFailure {
  const factory UnexpectedStorageFailure() = _$UnexpectedStorageFailure;
}
