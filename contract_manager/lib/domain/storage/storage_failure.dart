import 'package:freezed_annotation/freezed_annotation.dart';

part 'storage_failure.freezed.dart';

@freezed
abstract class StorageFailure with _$StorageFailure {
  const factory StorageFailure.unexpected() = UnexpectedStorageFailure;
}
