// Interface for IStorageRepository.
import 'dart:typed_data';

import 'package:image/image.dart';

abstract class IStorageRepository {
  ///Stores an image to the given [path].
  Future<void> storeImage(Uint8List bytes, String path);

  ///Retrieves an image to the given [path].
  Future<Image> getImage(String path);
}
