import 'package:freezed_annotation/freezed_annotation.dart';

part 'batch_failure.freezed.dart';

@freezed
class BatchFailure with _$BatchFailure {
  const factory BatchFailure.unexpected() = UnexpectedBatchFaliure;
}
