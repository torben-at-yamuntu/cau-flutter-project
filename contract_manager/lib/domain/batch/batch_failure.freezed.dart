// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'batch_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$BatchFailureTearOff {
  const _$BatchFailureTearOff();

  UnexpectedBatchFaliure unexpected() {
    return const UnexpectedBatchFaliure();
  }
}

/// @nodoc
const $BatchFailure = _$BatchFailureTearOff();

/// @nodoc
mixin _$BatchFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedBatchFaliure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedBatchFaliure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BatchFailureCopyWith<$Res> {
  factory $BatchFailureCopyWith(
          BatchFailure value, $Res Function(BatchFailure) then) =
      _$BatchFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$BatchFailureCopyWithImpl<$Res> implements $BatchFailureCopyWith<$Res> {
  _$BatchFailureCopyWithImpl(this._value, this._then);

  final BatchFailure _value;
  // ignore: unused_field
  final $Res Function(BatchFailure) _then;
}

/// @nodoc
abstract class $UnexpectedBatchFaliureCopyWith<$Res> {
  factory $UnexpectedBatchFaliureCopyWith(UnexpectedBatchFaliure value,
          $Res Function(UnexpectedBatchFaliure) then) =
      _$UnexpectedBatchFaliureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedBatchFaliureCopyWithImpl<$Res>
    extends _$BatchFailureCopyWithImpl<$Res>
    implements $UnexpectedBatchFaliureCopyWith<$Res> {
  _$UnexpectedBatchFaliureCopyWithImpl(UnexpectedBatchFaliure _value,
      $Res Function(UnexpectedBatchFaliure) _then)
      : super(_value, (v) => _then(v as UnexpectedBatchFaliure));

  @override
  UnexpectedBatchFaliure get _value => super._value as UnexpectedBatchFaliure;
}

/// @nodoc
class _$UnexpectedBatchFaliure implements UnexpectedBatchFaliure {
  const _$UnexpectedBatchFaliure();

  @override
  String toString() {
    return 'BatchFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedBatchFaliure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedBatchFaliure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedBatchFaliure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedBatchFaliure implements BatchFailure {
  const factory UnexpectedBatchFaliure() = _$UnexpectedBatchFaliure;
}
