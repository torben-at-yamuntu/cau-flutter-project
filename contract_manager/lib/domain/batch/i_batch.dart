abstract class IBatch {
  Future<void> commit();
}
