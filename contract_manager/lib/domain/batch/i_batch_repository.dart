import 'package:contract_manager/domain/batch/i_batch.dart';

/// A repository to get batches
abstract class IBatchRepository {
  IBatch getBatch();
}
