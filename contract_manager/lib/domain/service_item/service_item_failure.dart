import 'package:freezed_annotation/freezed_annotation.dart';

part 'service_item_failure.freezed.dart';

@freezed
abstract class ServiceItemFailure with _$ServiceItemFailure {
  const factory ServiceItemFailure.unexpected() = UnexpectedServiceItemFailure;
}
