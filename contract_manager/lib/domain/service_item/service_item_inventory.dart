import 'package:contract_manager/domain/core/entity.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// Material needed for a ServiceItem
abstract class ServiceItemInventory extends Entity {
  const ServiceItemInventory(
    this.id,
    this.material,
    this.quantity,
    this.quantityUnit,
    this.price,
  );
  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final String material;

  @JsonKey(required: true, disallowNullValue: true)
  final double quantity;

  @JsonKey(required: true, disallowNullValue: true)
  final QuantityUnit quantityUnit;

  @JsonKey(required: true, disallowNullValue: true)
  final double price;

  @override
  List<Object> get props => [
        id,
        material,
        quantity,
        quantityUnit,
        price,
      ];
  ServiceItemInventory copyWith({
    String? material,
    double? quantity,
    QuantityUnit? quantityUnit,
  });

  ServiceItemInventoryParams toParams() =>
      ServiceItemInventoryParams(material: material, quantity: quantity, unit: quantityUnit, price: price);
}

enum QuantityUnit { h, m2, kg, t, m, m3, stk }

extension QuantityUnitX on QuantityUnit {
  String toPrettyString() {
    switch (this) {
      case QuantityUnit.h:
        return 'std';
      case QuantityUnit.m2:
        return 'm^2';
      case QuantityUnit.kg:
        return 'kg';
      case QuantityUnit.t:
        return 't';
      case QuantityUnit.m:
        return 'm';
      case QuantityUnit.m3:
        return 'm^3';
      case QuantityUnit.stk:
        return 'm^3';
    }
  }
}

class ServiceItemInventoryParams extends Equatable {
  const ServiceItemInventoryParams({
    required this.material,
    required this.quantity,
    required this.unit,
    required this.price,
  });

  final String material;
  final double quantity;
  final QuantityUnit unit;
  final double price;

  @override
  List<Object?> get props => [
        material,
        quantity,
        unit,
        price,
      ];
}
