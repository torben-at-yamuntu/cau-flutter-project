import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

abstract class Report extends Entity {
  const Report(
    this.authorId,
    this.message,
    this.imagePaths,
    this.isError,
  );

  @JsonKey(required: true, disallowNullValue: true)
  final String authorId;

  @JsonKey(required: true, disallowNullValue: true)
  final String message;

  @JsonKey(required: true, disallowNullValue: true)
  final BuiltList<String> imagePaths;

  @JsonKey(required: true, disallowNullValue: true)
  final bool isError;

  @override
  List<Object?> get props => [authorId, message, imagePaths, isError];

  Report copyWith({
    String? authorId,
    String? message,
    BuiltList<String>? imagePaths,
    bool? isError,
  });
}
