import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/core/entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

abstract class Payment extends Entity {
  const Payment(
    this.id,
    this.amount,
    this.serviceItemIds,
    this.sentPaymentAt,
    this.receivedPaymentAt,
    this.paymentActiveAt,
  );

  @JsonKey(required: true, disallowNullValue: true)
  final String id;
  @JsonKey(required: true, disallowNullValue: true)
  final double amount;

  /// When these service items have finished the payment is required
  @JsonKey(required: true, disallowNullValue: true)
  final BuiltList<String> serviceItemIds;
  @JsonKey(required: true)
  final DateTime? sentPaymentAt;
  @JsonKey(required: true)
  final DateTime? receivedPaymentAt;
  @JsonKey(required: true)
  final DateTime? paymentActiveAt;

  PaymentStatus get status {
    if (paymentActiveAt == null) return PaymentStatus.deactivated;
    if (sentPaymentAt == null) return PaymentStatus.required;
    if (receivedPaymentAt == null) return PaymentStatus.sent;
    return PaymentStatus.received;
  }

  @override
  List<Object?> get props => [
        id,
        amount,
        serviceItemIds,
        sentPaymentAt,
        receivedPaymentAt,
        paymentActiveAt,
      ];

  Payment copyWith({
    String? id,
    double? amount,
    BuiltList<String>? serviceItemIds,
    DateTime? sentPaymentAt,
    DateTime? receivedPaymentAt,
    DateTime? paymentActiveAt,
  });
}

enum PaymentStatus { deactivated, required, sent, received }
