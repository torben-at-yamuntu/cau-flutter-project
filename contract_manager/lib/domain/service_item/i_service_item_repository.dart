import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_failure.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';
import 'package:dartz/dartz.dart';

/// The repository to interact with [Employee].
abstract class IServiceItemRepository {
  /// Returns a realtime data stream of all [ServiceItem]s with [ServiceItem.contractId] equals [contract.clientId]
  /// and all [Project]s where [Contract.clientId] equals [companyId] of a related [Contract].
  Stream<BuiltList<ServiceItem>> watchServiceItems({required Contract contract});

  /// Creates a ServiceItem with parameters:
  /// [ServiceItem.projectId] equals [projectId],
  /// [ServiceItem.contractId] equals [contractId],
  /// [ServiceItem.specification] equals [specification],
  /// [ServiceItem.startsAt] equals [startAt],
  /// [ServiceItem.endPlannedAt] equals [endPlannedAt],
  /// [ServiceItem.inventories] equals [inventories],
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<ServiceItem> create({
    required String projectId,
    required String contractId,
    required String specification,
    required DateTime startAt,
    required DateTime endPlannedAt,
    required List<ServiceItemInventoryParams> inventories,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Updates the quantity of a supply in the ServiceItemInventory with
  /// [ServiceItemInventory.quantity] equals [quantity]
  /// [ServiceItemInventory.quantityUnit] equals [unit]
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> updateServiceItemQuantity({
    required ServiceItem serviceItem,
    required String serviceItemInventoryId,
    required double quantity,
    QuantityUnit? unit,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Adds a new [ServiceItemInventory] to a [ServiceItem].
  /// [serviceItemInventory] will be added to [serviceItem.inventories].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> addServiceItemInventory({
    required ServiceItem serviceItem,
    required ServiceItemInventory serviceItemInventory,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Delete [ServiceItemInventory] to a [ServiceItem].
  /// [serviceItemInventory] will be deleted from [serviceItem.inventories].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> deleteServiceItemInventory({
    required ServiceItem serviceItem,
    required ServiceItemInventory serviceItemInventory,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Sets [serviceItem.report] equal to [report].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> setReport({
    required ServiceItem serviceItem,
    required String employeeId,
    required String message,
    required bool isError,
    required BuiltList<String> imagePaths,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Updates [serviceItem.workDoneAt] to [DateTime.now()].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> updateWorkDoneAtToNow({
    required ServiceItem serviceItem,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Updates [serviceItem.approvedAt] to [DateTime.now()].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> updateApprovedAtToNow({
    required ServiceItem serviceItem,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Sets [contract.lastSeenVersions[employeeId]] to [serviceItem.versionNumber].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> updateLastSeenVersion({
    required ServiceItem serviceItem,
    required String employeeId,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Increments [serviceItem.versionNumber] by 1.
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> incrementVersion({
    required ServiceItem serviceItem,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Changes [serviceItem.contractId] to  [newContractId].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> setSubContractId({
    required ServiceItem serviceItem,
    required Option<String> subContractIdOption,
    IBatch? batch,
    ITransaction? transaction,
  });

  /// Changes [serviceItem.contractId] to  [newContractId].
  /// Throws [ServiceItemFailure] when something went wrong.
  Future<void> setOldContractId({
    required ServiceItem serviceItem,
    required String outsourcedContractId,
    IBatch? batch,
    ITransaction? transaction,
  });

  Future<ServiceItem> getServiceItemById({
    required String projectId,
    required String contractId,
    required String serviceItemId,
    ITransaction? transaction,
  });
}
