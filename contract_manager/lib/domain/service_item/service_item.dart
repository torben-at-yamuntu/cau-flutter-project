import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/core/versionable_document.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// ServiceItem represents "Leistungsposition".
abstract class ServiceItem extends VersionableDocument {
  const ServiceItem(
    this.id,
    this.projectId,
    this.contractId,
    this.specification,
    this.startsAt,
    this.endPlannedAt,
    this.workDoneAt,
    this.approvedAt,
    this.inventories,
    this.report,
    this.outsourcedContractId,
    this.subContractId,
    int versionNumber,
    BuiltMap<String, int> lastSeenVersions,
  ) : super(versionNumber, lastSeenVersions);

  @JsonKey(required: true, disallowNullValue: true)
  final String id;

  @JsonKey(required: true, disallowNullValue: true)
  final String projectId;

  @JsonKey(required: true, disallowNullValue: true)
  final String contractId;

  @JsonKey(required: true, disallowNullValue: true)
  final String specification;

  @JsonKey(required: true, disallowNullValue: true)
  final DateTime startsAt;

  @JsonKey(required: true, disallowNullValue: true)
  final DateTime endPlannedAt;

  @JsonKey(required: true)
  final DateTime? workDoneAt;

  @JsonKey(required: true)
  final DateTime? approvedAt;

  final String? outsourcedContractId;

  final String? subContractId;

  @JsonKey(required: true, disallowNullValue: true)
  final BuiltMap<String, ServiceItemInventory> inventories;

  @JsonKey(required: true)
  final Report? report;

  ServiceItemStatus get status {
    if (DateTime.now().isBefore(startsAt)) return ServiceItemStatus.open;
    if (workDoneAt == null) return ServiceItemStatus.inProgress;
    if (approvedAt == null) return ServiceItemStatus.finished;
    return ServiceItemStatus.approved;
  }

  @override
  List<Object?> get props => [
        ...super.props,
        id,
        projectId,
        contractId,
        specification,
        startsAt,
        endPlannedAt,
        workDoneAt,
        approvedAt,
        inventories,
        report,
        outsourcedContractId,
        subContractId,
      ];

  ServiceItem copyWith({
    DateTime? workDoneAt,
    DateTime? approvedAt,
    BuiltMap<String, ServiceItemInventory>? inventories,
    BuiltMap<String, int>? lastSeenVersions,
    Report? report,
    String? contractId,
    String? outsourcedContractId,
    Option<String>? subContractId,
  });
}

enum ServiceItemStatus { open, inProgress, finished, approved }
