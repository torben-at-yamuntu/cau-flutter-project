// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'transaction_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$TransactionFailureTearOff {
  const _$TransactionFailureTearOff();

  UnexpectedTransactionFailure unexpected() {
    return const UnexpectedTransactionFailure();
  }
}

/// @nodoc
const $TransactionFailure = _$TransactionFailureTearOff();

/// @nodoc
mixin _$TransactionFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedTransactionFailure value) unexpected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedTransactionFailure value)? unexpected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionFailureCopyWith<$Res> {
  factory $TransactionFailureCopyWith(
          TransactionFailure value, $Res Function(TransactionFailure) then) =
      _$TransactionFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$TransactionFailureCopyWithImpl<$Res>
    implements $TransactionFailureCopyWith<$Res> {
  _$TransactionFailureCopyWithImpl(this._value, this._then);

  final TransactionFailure _value;
  // ignore: unused_field
  final $Res Function(TransactionFailure) _then;
}

/// @nodoc
abstract class $UnexpectedTransactionFailureCopyWith<$Res> {
  factory $UnexpectedTransactionFailureCopyWith(
          UnexpectedTransactionFailure value,
          $Res Function(UnexpectedTransactionFailure) then) =
      _$UnexpectedTransactionFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnexpectedTransactionFailureCopyWithImpl<$Res>
    extends _$TransactionFailureCopyWithImpl<$Res>
    implements $UnexpectedTransactionFailureCopyWith<$Res> {
  _$UnexpectedTransactionFailureCopyWithImpl(
      UnexpectedTransactionFailure _value,
      $Res Function(UnexpectedTransactionFailure) _then)
      : super(_value, (v) => _then(v as UnexpectedTransactionFailure));

  @override
  UnexpectedTransactionFailure get _value =>
      super._value as UnexpectedTransactionFailure;
}

/// @nodoc
class _$UnexpectedTransactionFailure implements UnexpectedTransactionFailure {
  const _$UnexpectedTransactionFailure();

  @override
  String toString() {
    return 'TransactionFailure.unexpected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnexpectedTransactionFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unexpected,
  }) {
    return unexpected();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnexpectedTransactionFailure value) unexpected,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnexpectedTransactionFailure value)? unexpected,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class UnexpectedTransactionFailure implements TransactionFailure {
  const factory UnexpectedTransactionFailure() = _$UnexpectedTransactionFailure;
}
