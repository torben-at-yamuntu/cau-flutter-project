import 'package:contract_manager/domain/transaction/i_transaction.dart';

typedef TransactionCallback<T> = Future<T> Function(ITransaction transaction);

/// A repository to execute transactions
abstract class ITransactionRepository<T> {
  Future<T> runTransaction(TransactionCallback<T> callback);
}
