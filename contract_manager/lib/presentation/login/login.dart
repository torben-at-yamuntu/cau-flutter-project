import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/domain/auth/auth_failure.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/presentation/routing/app_router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

typedef OnLoginSuccess = Future<bool> Function();

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    this.onLoginSuccess,
    Key? key,
  }) : super(key: key);

  final OnLoginSuccess? onLoginSuccess;
  final String title = 'Login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 400),
            child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(36),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      controller: emailController,
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                        border:
                            OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(32))),
                        labelText: 'Email',
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Bitte geben Sie ihre Email an';
                        }
                        final bool emailValid = RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(value);
                        if (!emailValid) {
                          return 'Bitte geben Sie eine gültige Email an';
                        }
                        return null;
                      },
                      onFieldSubmitted: (_) => onSubmit(),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                        border:
                            OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(32))),
                        labelText: 'Passwort',
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Bitte geben Sie ihr Passwort an';
                        }
                        return null;
                      },
                      onFieldSubmitted: (_) => onSubmit(),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    LoginButton(onPressed: _isLoading ? null : onSubmit),
                  ],
                ),
              ),
            ),
          ),
        ),
      );

  Future<void> onSubmit() async {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_formKey.currentState!.validate()) {
      print(emailController.text);
      setState(() {
        _isLoading = true;
      });
      try {
        await context
            .read(authRepositoryProvider)
            .logIn(emailController.text, passwordController.text);

        final onLoginSuccess = widget.onLoginSuccess;

        if (onLoginSuccess != null) {
          await onLoginSuccess.call();
        } else {
          await context.router.replaceAll(const [ContractsScreenRoute()]);
        }
      } on AuthFailure catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('This didnt work: $e')));
        setState(() {
          _isLoading = false;
        });
      } catch (e) {
        print(e.runtimeType);
      }
    }
  }
}

class LoginButton extends StatelessWidget {
  const LoginButton({
    Key? key,
    this.onPressed,
  }) : super(key: key);

  final FutureOr<void> Function()? onPressed;

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: Container()),
            const Text('Login'),
            Expanded(child: Container()),
            if (onPressed == null)
              const SizedBox(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(
                  strokeWidth: 3,
                ),
              )
            else
              const SizedBox(
                height: 20,
                width: 20,
              )
          ],
        ),
      );
}
