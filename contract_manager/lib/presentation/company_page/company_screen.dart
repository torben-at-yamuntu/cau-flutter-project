import 'package:contract_manager/application/contract/create_company_provider.dart';
import 'package:contract_manager/application/register/register_company_uc.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CreateCompanyScreen extends StatefulWidget {
  const CreateCompanyScreen({Key? key}) : super(key: key);

  @override
  _CreateCompanyScreenState createState() => _CreateCompanyScreenState();
}

class _CreateCompanyScreenState extends State<CreateCompanyScreen> {
  late final TextEditingController _nameController;
  late final TextEditingController _countryController;
  late final TextEditingController _cityController;
  late final TextEditingController _zipController;
  late final TextEditingController _streetController;
  late final TextEditingController _houseNumberController;
  late final TextEditingController _ibanController;
  late final TextEditingController _bicController;
  late final TextEditingController _accountOwnerController;
  late final TextEditingController _adminEmailController;
  late final TextEditingController _adminPasswordController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _countryController = TextEditingController();
    _cityController = TextEditingController();
    _zipController = TextEditingController();
    _streetController = TextEditingController();
    _houseNumberController = TextEditingController();
    _ibanController = TextEditingController();
    _bicController = TextEditingController();
    _accountOwnerController = TextEditingController();
    _adminEmailController = TextEditingController();
    _adminPasswordController = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _countryController.dispose();
    _cityController.dispose();
    _zipController.dispose();
    _streetController.dispose();
    _houseNumberController.dispose();
    _ibanController.dispose();
    _bicController.dispose();
    _accountOwnerController.dispose();
    _adminEmailController.dispose();
    _adminPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 480),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Center(
                        child: Text(
                          'Firma erstellen',
                          style: Theme.of(context).textTheme.headline3,
                        ),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _nameController,
                        decoration: const InputDecoration(
                          labelText: 'Firmenname',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _countryController,
                        decoration: const InputDecoration(
                          labelText: 'Land',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _cityController,
                        decoration: const InputDecoration(
                          labelText: 'Stadt',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _zipController,
                        decoration: const InputDecoration(
                          labelText: 'Postleitzahl',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _streetController,
                        decoration: const InputDecoration(
                          labelText: 'Straße',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _houseNumberController,
                        decoration: const InputDecoration(
                          labelText: 'Hausnummer',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                          try {
                            int.parse(value);
                          } catch (e) {
                            return 'Bitte gib eine valide Zahl an!';
                          }
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _ibanController,
                        decoration: const InputDecoration(
                          labelText: 'IBAN',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _bicController,
                        decoration: const InputDecoration(
                          labelText: 'BIC',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _accountOwnerController,
                        decoration: const InputDecoration(
                          labelText: 'Kontoinhaber',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _adminEmailController,
                        decoration: const InputDecoration(
                          labelText: 'Admin Email Adresse',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _adminPasswordController,
                        decoration: const InputDecoration(
                          labelText: 'Admin Passwort',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) return 'Feld benötigt!';
                        },
                      ),
                      const SizedBox(height: 20),
                      Center(
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(content: Text('Processing Data')));
                              final UseCase<void, RegisterCompanyParams> createCompany =
                                  context.read(createCompanyProvider);

                              final RegisterCompanyParams registerCompanyParams = RegisterCompanyParams(
                                name: _nameController.text,
                                country: _countryController.text,
                                city: _cityController.text,
                                zip: int.parse(_zipController.text),
                                street: _streetController.text,
                                houseNumber: int.parse(_houseNumberController.text),
                                iban: _ibanController.text,
                                bic: _bicController.text,
                                accountOwner: _accountOwnerController.text,
                                adminEmail: _adminEmailController.text,
                                adminPassword: _adminPasswordController.text,
                              );

                              createCompany(registerCompanyParams);
                            }
                          },
                          child: const Text('Erstellen'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
