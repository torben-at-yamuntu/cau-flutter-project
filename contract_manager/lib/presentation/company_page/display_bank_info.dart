import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DisplayBankInfo extends StatefulWidget {
  const DisplayBankInfo({
    Key? key,
    required this.bankInfo,
  }) : super(key: key);

  final BankInfo bankInfo;

  @override
  _DisplayBankInfoState createState() => _DisplayBankInfoState();
}

class _DisplayBankInfoState extends State<DisplayBankInfo> {
  final TextEditingController _bicControler = TextEditingController();
  final TextEditingController _ibanControler = TextEditingController();
  final TextEditingController _ownerControler = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bicControler.text = widget.bankInfo.bic;
    _ibanControler.text = widget.bankInfo.iban;
    _ownerControler.text = widget.bankInfo.owner;
  }

  @override
  void dispose() {
    _bicControler.dispose();
    _ibanControler.dispose();
    _ownerControler.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Bankinfo:'),
            ),
            TextFormField(
              controller: _bicControler,
              decoration: const InputDecoration(
                labelText: 'BIC',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie ihren BIC an';
                }
                final bool bicValid = RegExp(r'[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}').hasMatch(value);
                if (!bicValid) {
                  return 'Bitte geben sie einen korrekten BIC an';
                }
              },
            ),
            TextFormField(
              controller: _ibanControler,
              decoration: const InputDecoration(
                labelText: 'IBAN',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie ihre IBAN an';
                }
                final bool ibanValid = RegExp(r'[A-Z]{2}\d{2} ?\d{4} ?\d{4} ?\d{4} ?\d{4} ?[\d]{0,2}').hasMatch(value);
                if (!ibanValid) {
                  return 'Bitte geben sie eine korrekte IBAN an';
                }
              },
            ),
            TextFormField(
              controller: _ownerControler,
              decoration: const InputDecoration(
                labelText: 'Inhaber',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie den Inhaber des Kontos an';
                }
              },
            ),
          ],
        ),
      );
}
