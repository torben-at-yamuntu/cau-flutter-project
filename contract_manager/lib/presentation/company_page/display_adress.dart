import 'package:contract_manager/domain/company/address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DisplayAddress extends StatefulWidget {
  const DisplayAddress({
    Key? key,
    required this.address,
  }) : super(key: key);
  final Address address;
  @override
  _DisplayAddressState createState() => _DisplayAddressState();
}

class _DisplayAddressState extends State<DisplayAddress> {
  final TextEditingController _numberControler = TextEditingController();
  final TextEditingController _streetControler = TextEditingController();
  final TextEditingController _countryControler = TextEditingController();
  final TextEditingController _cityControler = TextEditingController();

  @override
  void initState() {
    super.initState();
    _numberControler.text = widget.address.houseNumber.toString();
    _streetControler.text = widget.address.street;
    _cityControler.text = widget.address.city;
    _countryControler.text = widget.address.country;
  }

  @override
  void dispose() {
    _numberControler.dispose();
    _streetControler.dispose();
    _cityControler.dispose();
    _countryControler.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Addresse:'),
            ),
            TextFormField(
              controller: _countryControler,
              decoration: const InputDecoration(
                labelText: 'Land',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie ihr Land an';
                }
              },
            ),
            TextFormField(
              controller: _cityControler,
              decoration: const InputDecoration(
                labelText: 'Stadt',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie ihre Stadt an';
                }
              },
            ),
            TextFormField(
                controller: _streetControler,
                decoration: const InputDecoration(
                  labelText: 'Straße',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Bitte geben Sie ihre Straße an';
                  }
                }),
            TextFormField(
              controller: _numberControler,
              decoration: const InputDecoration(
                labelText: 'Hausnummer',
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Bitte geben Sie ihre Hausnummer an';
                }
                final bool numberValid = RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$').hasMatch(value);
                if (!numberValid) {
                  return 'Bitte geben Sie nur Zahlen an';
                }
              },
            ),
          ],
        ),
      );
}
