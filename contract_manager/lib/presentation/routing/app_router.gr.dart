// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'dart:async' as _i13;

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i11;

import '../../domain/domain.dart' as _i12;
import '../company_page/company_screen.dart' as _i4;
import '../company_screen/company_management_screen.dart' as _i6;
import '../contracts_screen/create_contract_screen.dart' as _i7;
import '../not_found_screen/not_found_screen.dart' as _i5;
import '../presentation.dart' as _i3;
import '../projects_screen/create_project_screen.dart' as _i8;
import '../report/create_report.dart' as _i10;
import '../serviceitem/service_item_screen.dart' as _i9;
import 'auth_guard.dart' as _i2;

class AppRouter extends _i1.RootStackRouter {
  AppRouter({required this.authGuard});

  final _i2.AuthGuard authGuard;

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    EmptyRouterPageRoute.name: (entry) {
      return _i1.MaterialPageX(
          entry: entry, child: const _i1.EmptyRouterPage());
    },
    ReportScreenRoute.name: (entry) {
      var args = entry.routeData.argsAs<ReportScreenRouteArgs>();
      return _i1.MaterialPageX(
          entry: entry,
          child: _i3.ReportScreen(key: args.key, report: args.report));
    },
    CreateCompanyScreenRoute.name: (entry) {
      return _i1.MaterialPageX(
          entry: entry, child: const _i4.CreateCompanyScreen());
    },
    LoginScreenRoute.name: (entry) {
      var args = entry.routeData
          .argsAs<LoginScreenRouteArgs>(orElse: () => LoginScreenRouteArgs());
      return _i1.MaterialPageX(
          entry: entry,
          child: _i3.LoginScreen(
              onLoginSuccess: args.onLoginSuccess, key: args.key));
    },
    NotFoundScreenRoute.name: (entry) {
      return _i1.MaterialPageX(entry: entry, child: const _i5.NotFoundScreen());
    },
    ContractsScreenRoute.name: (entry) {
      return _i1.MaterialPageX(
          entry: entry, child: const _i3.ContractsScreen());
    },
    ContractDetailScreenRoute.name: (entry) {
      var pathParams = entry.routeData.pathParams;
      var args = entry.routeData.argsAs<ContractDetailScreenRouteArgs>(
          orElse: () => ContractDetailScreenRouteArgs(
              contractId: pathParams.getString('contractId'),
              projectId: pathParams.getString('projectId')));
      return _i1.MaterialPageX(
          entry: entry,
          child: _i3.ContractDetailScreen(
              contractId: args.contractId,
              projectId: args.projectId,
              key: args.key));
    },
    CompanyManagementScreenRoute.name: (entry) {
      var pathParams = entry.routeData.pathParams;
      var args = entry.routeData.argsAs<CompanyManagementScreenRouteArgs>(
          orElse: () => CompanyManagementScreenRouteArgs(
              companyId: pathParams.getString('companyId')));
      return _i1.MaterialPageX(
          entry: entry,
          child: _i6.CompanyManagementScreen(
              companyId: args.companyId, key: args.key));
    },
    CreateContractScreenRoute.name: (entry) {
      var pathParams = entry.routeData.pathParams;
      var args = entry.routeData.argsAs<CreateContractScreenRouteArgs>(
          orElse: () => CreateContractScreenRouteArgs(
              projectId: pathParams.getString('projectId')));
      return _i1.MaterialPageX(
          entry: entry,
          child: _i7.CreateContractScreen(
              projectId: args.projectId, key: args.key));
    },
    CreateProjectScreenRoute.name: (entry) {
      var pathParams = entry.routeData.pathParams;
      var args = entry.routeData.argsAs<CreateProjectScreenRouteArgs>(
          orElse: () => CreateProjectScreenRouteArgs(
              companyId: pathParams.getString('companyId')));
      return _i1.MaterialPageX(
          entry: entry,
          child: _i8.CreateProjectScreen(
              companyId: args.companyId, key: args.key));
    },
    ServiceItemScreenRoute.name: (entry) {
      var pathParams = entry.routeData.pathParams;
      var args = entry.routeData.argsAs<ServiceItemScreenRouteArgs>(
          orElse: () => ServiceItemScreenRouteArgs(
              serviceItemId: pathParams.getString('serviceItemId'),
              contractId: pathParams.getString('contractId'),
              projectId: pathParams.getString('projectId')));
      return _i1.MaterialPageX(
          entry: entry,
          child: _i9.ServiceItemScreen(
              serviceItemId: args.serviceItemId,
              contractId: args.contractId,
              projectId: args.projectId,
              key: args.key));
    },
    CreateReportScreenRoute.name: (entry) {
      var args = entry.routeData.argsAs<CreateReportScreenRouteArgs>();
      return _i1.MaterialPageX(
          entry: entry,
          child: _i10.CreateReportScreen(
              serviceItem: args.serviceItem, key: args.key));
    }
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(EmptyRouterPageRoute.name, path: '/', guards: [
          authGuard
        ], children: [
          _i1.RouteConfig(ContractsScreenRoute.name, path: ''),
          _i1.RouteConfig(ContractDetailScreenRoute.name,
              path: 'project/:projectId/contract/:contractId'),
          _i1.RouteConfig(CompanyManagementScreenRoute.name,
              path: 'company/:companyId'),
          _i1.RouteConfig(CreateContractScreenRoute.name,
              path: 'project/:projectId/createContract'),
          _i1.RouteConfig(CreateProjectScreenRoute.name,
              path: 'company/:companyId/createProject'),
          _i1.RouteConfig(ServiceItemScreenRoute.name,
              path:
                  'project/:projectId/contract/:contractId/serviceitem/:serviceItemId'),
          _i1.RouteConfig(CreateReportScreenRoute.name, path: 'createreport')
        ]),
        _i1.RouteConfig(ReportScreenRoute.name, path: '/report'),
        _i1.RouteConfig(CreateCompanyScreenRoute.name, path: '/createCompany'),
        _i1.RouteConfig(LoginScreenRoute.name, path: '/login'),
        _i1.RouteConfig(NotFoundScreenRoute.name, path: '/404'),
        _i1.RouteConfig('*#redirect',
            path: '*', redirectTo: '/404', fullMatch: true)
      ];
}

class EmptyRouterPageRoute extends _i1.PageRouteInfo {
  const EmptyRouterPageRoute({List<_i1.PageRouteInfo>? children})
      : super(name, path: '/', initialChildren: children);

  static const String name = 'EmptyRouterPageRoute';
}

class ReportScreenRoute extends _i1.PageRouteInfo<ReportScreenRouteArgs> {
  ReportScreenRoute({_i11.Key? key, required _i12.Report report})
      : super(name,
            path: '/report',
            args: ReportScreenRouteArgs(key: key, report: report));

  static const String name = 'ReportScreenRoute';
}

class ReportScreenRouteArgs {
  const ReportScreenRouteArgs({this.key, required this.report});

  final _i11.Key? key;

  final _i12.Report report;
}

class CreateCompanyScreenRoute extends _i1.PageRouteInfo {
  const CreateCompanyScreenRoute() : super(name, path: '/createCompany');

  static const String name = 'CreateCompanyScreenRoute';
}

class LoginScreenRoute extends _i1.PageRouteInfo<LoginScreenRouteArgs> {
  LoginScreenRoute(
      {_i13.Future<bool> Function()? onLoginSuccess, _i11.Key? key})
      : super(name,
            path: '/login',
            args:
                LoginScreenRouteArgs(onLoginSuccess: onLoginSuccess, key: key));

  static const String name = 'LoginScreenRoute';
}

class LoginScreenRouteArgs {
  const LoginScreenRouteArgs({this.onLoginSuccess, this.key});

  final _i13.Future<bool> Function()? onLoginSuccess;

  final _i11.Key? key;
}

class NotFoundScreenRoute extends _i1.PageRouteInfo {
  const NotFoundScreenRoute() : super(name, path: '/404');

  static const String name = 'NotFoundScreenRoute';
}

class ContractsScreenRoute extends _i1.PageRouteInfo {
  const ContractsScreenRoute() : super(name, path: '');

  static const String name = 'ContractsScreenRoute';
}

class ContractDetailScreenRoute
    extends _i1.PageRouteInfo<ContractDetailScreenRouteArgs> {
  ContractDetailScreenRoute(
      {required String contractId, required String projectId, _i11.Key? key})
      : super(name,
            path: 'project/:projectId/contract/:contractId',
            args: ContractDetailScreenRouteArgs(
                contractId: contractId, projectId: projectId, key: key),
            params: {'contractId': contractId, 'projectId': projectId});

  static const String name = 'ContractDetailScreenRoute';
}

class ContractDetailScreenRouteArgs {
  const ContractDetailScreenRouteArgs(
      {required this.contractId, required this.projectId, this.key});

  final String contractId;

  final String projectId;

  final _i11.Key? key;
}

class CompanyManagementScreenRoute
    extends _i1.PageRouteInfo<CompanyManagementScreenRouteArgs> {
  CompanyManagementScreenRoute({required String companyId, _i11.Key? key})
      : super(name,
            path: 'company/:companyId',
            args: CompanyManagementScreenRouteArgs(
                companyId: companyId, key: key),
            params: {'companyId': companyId});

  static const String name = 'CompanyManagementScreenRoute';
}

class CompanyManagementScreenRouteArgs {
  const CompanyManagementScreenRouteArgs({required this.companyId, this.key});

  final String companyId;

  final _i11.Key? key;
}

class CreateContractScreenRoute
    extends _i1.PageRouteInfo<CreateContractScreenRouteArgs> {
  CreateContractScreenRoute({required String projectId, _i11.Key? key})
      : super(name,
            path: 'project/:projectId/createContract',
            args: CreateContractScreenRouteArgs(projectId: projectId, key: key),
            params: {'projectId': projectId});

  static const String name = 'CreateContractScreenRoute';
}

class CreateContractScreenRouteArgs {
  const CreateContractScreenRouteArgs({required this.projectId, this.key});

  final String projectId;

  final _i11.Key? key;
}

class CreateProjectScreenRoute
    extends _i1.PageRouteInfo<CreateProjectScreenRouteArgs> {
  CreateProjectScreenRoute({required String companyId, _i11.Key? key})
      : super(name,
            path: 'company/:companyId/createProject',
            args: CreateProjectScreenRouteArgs(companyId: companyId, key: key),
            params: {'companyId': companyId});

  static const String name = 'CreateProjectScreenRoute';
}

class CreateProjectScreenRouteArgs {
  const CreateProjectScreenRouteArgs({required this.companyId, this.key});

  final String companyId;

  final _i11.Key? key;
}

class ServiceItemScreenRoute
    extends _i1.PageRouteInfo<ServiceItemScreenRouteArgs> {
  ServiceItemScreenRoute(
      {required String serviceItemId,
      required String contractId,
      required String projectId,
      _i11.Key? key})
      : super(name,
            path:
                'project/:projectId/contract/:contractId/serviceitem/:serviceItemId',
            args: ServiceItemScreenRouteArgs(serviceItemId: serviceItemId, contractId: contractId, projectId: projectId, key: key),
            params: {
              'serviceItemId': serviceItemId,
              'contractId': contractId,
              'projectId': projectId
            });

  static const String name = 'ServiceItemScreenRoute';
}

class ServiceItemScreenRouteArgs {
  const ServiceItemScreenRouteArgs(
      {required this.serviceItemId,
      required this.contractId,
      required this.projectId,
      this.key});

  final String serviceItemId;

  final String contractId;

  final String projectId;

  final _i11.Key? key;
}

class CreateReportScreenRoute
    extends _i1.PageRouteInfo<CreateReportScreenRouteArgs> {
  CreateReportScreenRoute(
      {required _i12.ServiceItem serviceItem, _i11.Key? key})
      : super(name,
            path: 'createreport',
            args: CreateReportScreenRouteArgs(
                serviceItem: serviceItem, key: key));

  static const String name = 'CreateReportScreenRoute';
}

class CreateReportScreenRouteArgs {
  const CreateReportScreenRouteArgs({required this.serviceItem, this.key});

  final _i12.ServiceItem serviceItem;

  final _i11.Key? key;
}
