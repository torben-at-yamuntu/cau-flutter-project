import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'app_router.gr.dart';

class AuthGuard extends AutoRouteGuard {
  AuthGuard();

  @override
  Future<bool> canNavigate(
    List<PageRouteInfo<dynamic>> pendingRoutes,
    StackRouter router,
  ) async {
    // waits 300ms to let get authenticated before showing the login page if a auth cookie is already set
    await Future<dynamic>.delayed(const Duration(milliseconds: 400));
    final String? userId =
        router.navigatorKey.currentContext?.read(authRepositoryProvider).getLoggedInUserId();
    if (userId == null) {
      await router.replaceAll([
        LoginScreenRoute(onLoginSuccess: () async {
          await router.replaceAll(pendingRoutes);
          return true;
        })
      ]);
      return false;
    }

    return true;
  }
}
