import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/presentation/company_page/company_screen.dart';
import 'package:contract_manager/presentation/company_screen/company_management_screen.dart';
import 'package:contract_manager/presentation/contracts_screen/contract_detail_screen.dart';
import 'package:contract_manager/presentation/contracts_screen/contracts_screen.dart';
import 'package:contract_manager/presentation/contracts_screen/create_contract_screen.dart';
import 'package:contract_manager/presentation/login/login.dart';
import 'package:contract_manager/presentation/not_found_screen/not_found_screen.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:contract_manager/presentation/projects_screen/create_project_screen.dart';
import 'package:contract_manager/presentation/report/create_report.dart';
import 'package:contract_manager/presentation/serviceitem/service_item_screen.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute<void>(
      path: '/',
      page: EmptyRouterPage,
      guards: [/*AuthGuard*/],
      children: [
        AutoRoute<void>(
          path: '',
          initial: true,
          page: ContractsScreen,
        ),
        AutoRoute<void>(
          page: ContractDetailScreen,
          path: 'project/:projectId/contract/:contractId',
        ),
        AutoRoute<void>(
          path: 'company/:companyId',
          page: CompanyManagementScreen,
        ),
        AutoRoute<void>(
          path: 'project/:projectId/createContract',
          page: CreateContractScreen,
        ),
        AutoRoute<void>(
          path: 'company/:companyId/createProject',
          page: CreateProjectScreen,
        ),
        AutoRoute<void>(
          path: 'project/:projectId/contract/:contractId/serviceitem/:serviceItemId',
          page: ServiceItemScreen,
        ),
        AutoRoute<void>(
          path: 'createreport',
          page: CreateReportScreen,
        ),
      ],
    ),
    AutoRoute<void>(
      path: '/report',
      page: ReportScreen,
    ),
    AutoRoute<void>(
      path: '/createCompany',
      page: CreateCompanyScreen,
    ),
    AutoRoute<void>(
      path: '/login',
      page: LoginScreen,
    ),
    AutoRoute<void>(
      path: '/404',
      page: NotFoundScreen,
    ),
    RedirectRoute(
      path: '*',
      redirectTo: '/404',
    ),
  ],
)
class $AppRouter {}
