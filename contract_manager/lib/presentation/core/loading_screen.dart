import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: const Center(
          child: CircularProgressIndicator(),
        ),
      );
}
