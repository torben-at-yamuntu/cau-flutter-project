import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CMAppBar extends AppBar {
  CMAppBar({
    Key? key,
    required BuildContext context,
  }) : super(key: key, title: const Text('Contract Manager'), automaticallyImplyLeading: !kIsWeb, actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (value) => handleClick(value, context),
            itemBuilder: (BuildContext context) => {
              'Logout',
              'Create Project',
              'Unternehmen',
              'Create Company',
            }
                .map(
                  (String choice) => PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  ),
                )
                .toList(),
          ),
        ]);
}

void handleClick(
  String value,
  BuildContext context,
) {
  switch (value) {
    case 'Logout':
      context.read(authRepositoryProvider).logout();
      context.router.pushPath('/');
      //logout();
      break;
    case 'Neues Projekt':
      context.read(authRepositoryProvider).getLoggedInUserId();
      context.router.pushPath('/');
      break;

    case 'Unternehmen':
      context.read(contractRepositoryProvider).create(
          projectId: '81JUgLHC4sW66ZPAaajR',
          contractorId: 'UPwX9tdOVjWWzFoGPfE4',
          clientId: 'FAYomiB6rMq1IRQKQA74',
          specification: 'A cool contract',
          bankInfo: const BankInfoModel(iban: 'DE 01231231123', bic: '123123123', owner: 'Mustermann'),
          name: 'Erster Vertrag');
      // final String? userId = context.read(authRepositoryProvider).getLoggedInUserId();
      // final Employee employee =
      //await context.read(employeeRepositoryProvider).watchEmployee(employeeId: userId!, companyId: userId).last;
      // await context.router.push(CompanyScreenRoute(companyId: employee.companyId));
      break;

    case 'Create Company':
    //  final String? userId = context.read(authRepositoryProvider).getLoggedInUserId();
    //   final Employee employee = await context.read(employeeRepositoryProvider).watchEmployee(employeeId: userId!).last;
    //await context.router.push(CompanyManagementScreenRoute(companyId: 'UPwX9tdOVjWWzFoGPfE4'));
  }
}
