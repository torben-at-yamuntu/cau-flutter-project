import 'package:contract_manager/application/register/register_employee_usecase.dart';
import 'package:contract_manager/application/register/register_usecase_provider.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  final String title = 'Register';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  String safe = '';
  EmployeeRole _selectedLocation = EmployeeRole.read;
  bool _isCompany = false;
  final List<EmployeeRole> _locations = [EmployeeRole.admin, EmployeeRole.read, EmployeeRole.readWrite];
  final TextEditingController _name = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400),
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(36),
                  child: Column(
                    children: <Widget>[
                      const Text(
                        'Einen Nutzer erstellen',
                        textAlign: TextAlign.center,
                      ),
                      TextFormField(
                          controller: _email,
                          decoration: const InputDecoration(
                            labelText: 'Email',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bitte geben Sie eine Email an';
                            }
                            final bool emailValid =
                                RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value);
                            if (!emailValid) {
                              return 'Bitte geben Sie eine gültige Email an';
                            }
                            safe = value;
                            return null;
                          }),
                      const SizedBox(height: 15),
                      TextFormField(
                          decoration: const InputDecoration(
                            labelText: 'Email bestätigen',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bitte bestätigen Sie die Email';
                            }
                            final bool emailValid =
                                RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value);
                            if (!emailValid) {
                              return 'Bitte geben Sie eine gültige Email an';
                            } else if (value != safe) {
                              return 'Sie haben nicht die gleiche Email angeben.';
                            }
                            safe = '';
                            return null;
                          }),
                      const SizedBox(height: 15),
                      TextFormField(
                          controller: _password,
                          obscureText: true,
                          decoration: const InputDecoration(
                            labelText: 'Passwort',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bitte geben sie ein Passwort an.';
                            }
                            safe = value;
                            return null;
                          }),
                      const SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                          obscureText: true,
                          decoration: const InputDecoration(
                            labelText: 'Passwort bestätigen',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bestätigen Sie ihr Passwort';
                            } else if (value != safe) {
                              return 'Passwörter sind nicht identisch.';
                            }
                            safe = '';
                            return null;
                          }),
                      const SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                          controller: _name,
                          decoration: const InputDecoration(
                            labelText: 'Nutzer-/Unternehmensname',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bitte geben Sie den Namen des Nutzers an.';
                            }
                            return null;
                          }),
                      const SizedBox(
                        height: 15,
                      ),
                      CheckboxListTile(
                          title: const Text('Benutzer ist Unternehmen'),
                          value: _isCompany,
                          onChanged: (newValue) {
                            setState(() {
                              _isCompany = newValue!;
                            });
                          }),
                      Row(
                        children: [
                          const Text(
                            'Der Nutzer kann',
                            style: TextStyle(fontSize: 17),
                          ),
                          const SizedBox(width: 5),
                          DropdownButton(
                            hint: const Text('Nutzerrechte wählen'),
                            value: _selectedLocation,
                            items: _locations
                                .map((location) => DropdownMenuItem(
                                      value: location,
                                      child: Text(location.toPrettyString()),
                                    ))
                                .toList(),
                            onChanged: (EmployeeRole? newValue) {
                              setState(() {
                                _selectedLocation = newValue!;
                              });
                            },
                          ),
                        ],
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          // Validate returns true if the form is valid, or false
                          // otherwise.

                          if (_formKey.currentState!.validate()) {
                            if (_isCompany) {
                              //CreateCompany
                            } else {
                              await context.read(registerEmployeeUseCaseProvider).call(RegisterParams(
                                    email: _email.text,
                                    name: _name.text,
                                    password: _password.text,
                                    role: _selectedLocation,
                                    companyID: '',
                                  ));
                            }

                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(content: Text('Processing Data')));
                          }
                        },
                        child: const Text('Nutzer erstellen'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
