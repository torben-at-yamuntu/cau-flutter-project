import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/application/project/create_project.dart';
import 'package:contract_manager/application/project/create_project_provider.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CreateProjectScreen extends StatefulWidget {
  const CreateProjectScreen({
    @PathParam('companyId') required this.companyId,
    Key? key,
  }) : super(key: key);

  final String companyId;

  @override
  CreateProjectScreenState createState() => CreateProjectScreenState();
}

class CreateProjectScreenState extends State<CreateProjectScreen> {
  TextEditingController projectName = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400),
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(36),
                  child: Column(
                    children: <Widget>[
                      const Text(
                        'Ein Projekt erstellen',
                        textAlign: TextAlign.center,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Projektname',
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Bitte geben Sie einen Projektnamen an';
                          }
                          return null;
                        },
                        controller: projectName,
                      ),
                      const SizedBox(height: 15),
                      ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            try {
                              context.read(createProjectProvider).call(
                                    CreateProjectParams(
                                      companyId: widget.companyId,
                                      name: projectName.text,
                                    ),
                                  );
                            } catch (e) {
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Fehler!')));
                            }
                            context.router.pop();
                          }
                        },
                        child: const Text('Projekt erstellen'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
