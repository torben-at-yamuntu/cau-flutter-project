import 'package:auto_route/annotations.dart';
import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/application/employee_stream.dart';
import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/presentation/company_screen/register_employee.dart';
import 'package:contract_manager/presentation/core/error_screen.dart';
import 'package:contract_manager/presentation/core/loading_screen.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CompanyManagementScreen extends ConsumerWidget {
  const CompanyManagementScreen({
    @PathParam('companyId') required this.companyId,
    Key? key,
  }) : super(key: key);

  final String companyId;

  @override
  Widget build(BuildContext context, Reader watch) {
    final AsyncValue<CompanyViewModel> employeeState = watch(companyViewModelStreamProvider(companyId));
    return employeeState.when<Widget>(
      loading: () => const LoadingScreen(),
      error: (_, __) => const ErrorScreen(),
      data: (viewModel) => CompanyLoadedScreen(viewModel: viewModel),
    );
  }
}

/// main Widget organising the company informaion , employees information and the register information
class CompanyLoadedScreen extends StatelessWidget {
  const CompanyLoadedScreen({Key? key, required this.viewModel}) : super(key: key);

  final CompanyViewModel viewModel;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: CustomScrollView(
            slivers: [
              CompanyDisplay(company: viewModel.company),
              Employeedisplay(
                employees: viewModel.employees,
              ),
              const RegisterEmployeeScreen(),
            ],
          ),
        ),
      );
}

/// displays list of employees and their role
class Employeedisplay extends StatefulWidget {
  const Employeedisplay({Key? key, required this.employees}) : super(key: key);

  final BuiltList<Employee> employees;
  @override
  _EmployeedisplayState createState() => _EmployeedisplayState();
}

class _EmployeedisplayState extends State<Employeedisplay> {
  @override
  Widget build(BuildContext context) => SliverToBoxAdapter(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(2),
              child: SizedBox(
                height: (widget.employees.length) * 76,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: widget.employees.length,
                  itemBuilder: (_, index) => ListTile(
                    title: Text(widget.employees[index].name),
                    subtitle: Text('ID: ${widget.employees[index].id}'),
                    trailing: PopupMenuButton<EmployeeRole>(
                      onSelected: (EmployeeRole result) {
                        setState(() {
                          widget.employees[index].copyWith(role: result);
                          // get input to backend
                        });
                      },
                      itemBuilder: (BuildContext context) => <PopupMenuEntry<EmployeeRole>>[
                        const PopupMenuItem<EmployeeRole>(
                          value: EmployeeRole.admin,
                          child: Text('admin'),
                        ),
                        const PopupMenuItem<EmployeeRole>(
                          value: EmployeeRole.readWrite,
                          child: Text('read Write'),
                        ),
                        const PopupMenuItem<EmployeeRole>(
                          value: EmployeeRole.read,
                          child: Text('read'),
                        ),
                      ],
                      child: Text(widget.employees[index].role.toPrettyString()),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
}

///displays the Company information
class CompanyDisplay extends StatelessWidget {
  const CompanyDisplay({Key? key, required this.company}) : super(key: key);

  final Company company;

  @override
  Widget build(BuildContext context) {
    final expansionTile = ExpansionTile(
      title: const SelectableText('Firma: company.name'),
      children: [
        Text('FirmenName: ${company.name}'),
        Text('Firmen ID: ${company.id}'),
        BankInfoScreen(
          bankinfo: company.bankInfo,
        ),
        AdressScreen(
          address: company.address,
        ),
      ],
    );
    return SliverToBoxAdapter(
      child: expansionTile,
    );
  }
}

//displays the Bankinfo
class BankInfoScreen extends StatelessWidget {
  const BankInfoScreen({Key? key, required this.bankinfo}) : super(key: key);

  final BankInfo bankinfo;
  @override
  Widget build(BuildContext context) => Column(
        children: [
          const Text('Bank-Info'),
          Text('IBAN: ${bankinfo.iban}'),
          Text('BIC: ${bankinfo.bic}'),
          Text('Besitzer: ${bankinfo.owner}'),
        ],
      );
}

class AdressScreen extends StatelessWidget {
  const AdressScreen({Key? key, required this.address}) : super(key: key);

  final Address address;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          const Text('Addresse'),
          Text('Land: ${address.country}'),
          Text('Stadt: ${address.city}'),
          Text('Straße: ${address.street}'),
          Text('Nummer: ${address.houseNumber.toString()}'),
        ],
      );
}
