import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegisterEmployeeScreen extends StatefulWidget {
  const RegisterEmployeeScreen({Key? key}) : super(key: key);

  final String title = 'Register';

  @override
  _RegisterEmployeeScreenState createState() => _RegisterEmployeeScreenState();
}

class _RegisterEmployeeScreenState extends State<RegisterEmployeeScreen> {
  final _formKey = GlobalKey<FormState>();
  String safe = '';
  String? _selectedLocation;
  final List<String> _locations = ['Leserecht', 'Lese- und Schreibrecht', 'Administratorrechte'];
  @override
  Widget build(BuildContext context) => SliverToBoxAdapter(
        child: ExpansionTile(
          title: const SelectableText('Angestellten hinzufügen'),
          collapsedBackgroundColor: const Color(0x0F282828),
          backgroundColor: const Color(0x00282828),
          children: [
            SingleChildScrollView(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 400),
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(36),
                    child: Column(
                      children: <Widget>[
                        const Text(
                          'Einen Nutzer erstellen',
                          textAlign: TextAlign.center,
                        ),
                        TextFormField(
                            decoration: const InputDecoration(
                              hintText: 'Email',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte geben Sie eine Email an';
                              }
                              final bool emailValid =
                                  RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value);
                              if (!emailValid) {
                                return 'Bitte geben Sie eine gültige Email an';
                              }
                              safe = value;
                              return null;
                            }),
                        const SizedBox(height: 15),
                        TextFormField(
                            decoration: const InputDecoration(
                              hintText: 'Email bestätigen',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte bestätigen Sie die Email';
                              }
                              final bool emailValid =
                                  RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value);
                              if (!emailValid) {
                                return 'Bitte geben Sie eine gültige Email an';
                              } else if (value != safe) {
                                return 'Sie haben nicht die gleiche Email angeben.';
                              }
                              safe = '';
                              return null;
                            }),
                        const SizedBox(height: 15),
                        TextFormField(
                            obscureText: true,
                            decoration: const InputDecoration(
                              hintText: 'Passwort',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte geben sie ein Passwort an.';
                              }
                              safe = value;
                              return null;
                            }),
                        const SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                            obscureText: true,
                            decoration: const InputDecoration(
                              hintText: 'Passwort bestätigen',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bestätigen Sie ihr Passwort';
                              } else if (value != safe) {
                                return 'Passwörter sind nicht identisch.';
                              }
                              safe = '';
                              return null;
                            }),
                        const SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                            decoration: const InputDecoration(
                              hintText: 'Nutzername',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte geben Sie den Namen des Nutzers an.';
                              }
                              return null;
                            }),
                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            const Text(
                              'Nutzer hat',
                              style: TextStyle(fontSize: 17),
                            ),
                            const SizedBox(width: 5),
                            DropdownButton(
                              hint: const Text('Nutzerrechte wählen'),
                              value: _selectedLocation,
                              items: _locations
                                  .map((location) => DropdownMenuItem(
                                        value: location,
                                        child: Text(location),
                                      ))
                                  .toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedLocation = newValue.toString();
                                });
                              },
                            ),
                          ],
                        ),
                        ElevatedButton(
                          onPressed: () {
                            // Validate returns true if the form is valid, or false
                            // otherwise.
                            if (_formKey.currentState!.validate()) {
                              // If the form is valid, display a Snackbar.
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(content: Text('Processing Data')));
                            }
                          },
                          child: const Text('Nutzer erstellen'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
