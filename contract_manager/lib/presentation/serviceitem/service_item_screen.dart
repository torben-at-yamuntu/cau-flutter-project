import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/application/service_Item/complete_service_item.dart';
import 'package:contract_manager/application/service_Item/complete_service_item_provider.dart';
import 'package:contract_manager/application/service_item_stream.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/presentation/core/date_time_extension.dart';
import 'package:contract_manager/presentation/core/loading_screen.dart';
import 'package:contract_manager/presentation/login/login.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:contract_manager/presentation/routing/app_router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ServiceItemScreen extends ConsumerWidget {
  const ServiceItemScreen({
    @PathParam('serviceItemId') required this.serviceItemId,
    @PathParam('contractId') required this.contractId,
    @PathParam('projectId') required this.projectId,
    Key? key,
  }) : super(key: key);

  final String serviceItemId;
  final String contractId;
  final String projectId;
  @override
  Widget build(BuildContext context, Reader watch) {
    final AsyncValue<ServiceItem> serviceItemState = watch(
      serviceItemStreamProvider(
        ServiceItemParams(
          contractId: contractId,
          projectId: projectId,
          serviceItemId: serviceItemId,
        ),
      ),
    );
    return serviceItemState.when<Widget>(
      loading: () => const LoadingScreen(),
      error: (error, stackTrace) {
        print(error);
        print(stackTrace);
        return const LoginScreen();
      },
      data: (serviceItem) => ServiceItemLoadedScreen(serviceItem: serviceItem),
    );
  }
}

class ServiceItemLoadedScreen extends StatelessWidget {
  const ServiceItemLoadedScreen({
    required this.serviceItem,
    Key? key,
  }) : super(key: key);
  final ServiceItem serviceItem;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 480),
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      const SizedBox(height: 20),
                      Text(
                        serviceItem.specification,
                        textScaleFactor: 3,
                      ),
                      const SizedBox(height: 20),
                      Container(
                        color: const Color(0x7724629c),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Anfang am : ${serviceItem.startsAt.formatDate}'),
                            Text('Ende am : ${serviceItem.endPlannedAt.formatDate}')
                          ],
                        ),
                      ),
                      InventorySection(serviceItem: serviceItem),
                      const SizedBox(height: 20),
                      Container(
                        color: Colors.red,
                        child: (serviceItem.report != null)
                            ? Text('Report Message by ${serviceItem.report!.authorId}: ${serviceItem.report!.message}')
                            : null,
                      ),
                      const SizedBox(height: 10),
                      if (serviceItem.report != null)
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Colors.red),
                          onPressed: () {
                            context.router.push(
                              ReportScreenRoute(
                                report: serviceItem.report!,
                              ),
                            );
                          },
                          child: const Text('View Pictures'),
                        ),
                      const SizedBox(height: 20),
                      Wrap(
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              try {
                                final UseCase<void, CompleteServiceItemParams> completeServiceItem =
                                    context.read(completeServiceItemProvider);

                                completeServiceItem(CompleteServiceItemParams(
                                  serviceItem: serviceItem,
                                ));
                              } catch (e) {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Fehler!')));
                              }
                              /*navigate somewhere*/
                            },
                            child: Text('Mark ${serviceItem.id} as ready'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              /*use case something something*/
                            },
                            child: const Text('Leistungposition auslagern'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              context.router.push(CreateReportScreenRoute(
                                serviceItem: serviceItem,
                              ));
                            },
                            child: const Text('Mängelbericht erstellen'),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

class InventorySection extends StatelessWidget {
  const InventorySection({
    Key? key,
    required this.serviceItem,
  }) : super(key: key);
  final ServiceItem serviceItem;

  @override
  Widget build(BuildContext context) => DataTable(
        columns: const <DataColumn>[
          DataColumn(label: Text('ID')),
          DataColumn(label: Text('Material')),
          DataColumn(label: Text('Menge')),
        ],
        rows: [
          for (final inventory in serviceItem.inventories.values)
            DataRow(
              cells: <DataCell>[
                DataCell(
                  Text(inventory.id.toString()),
                ),
                DataCell(
                  Text(inventory.material.toString()),
                ),
                DataCell(
                  Text('${inventory.quantity.toString()}  ${inventory.quantityUnit.toPrettyString()}'),
                ),
              ],
            ),
        ],
      );
}
