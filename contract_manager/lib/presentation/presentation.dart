/// The presentation layer is the only layer in our application
/// which knows about flutter. It contains all the widgets, is
/// responsible for routing and listens to user interactions.
library presentation_layer;

export 'app.dart';
export 'contracts_screen/contract_detail_screen.dart';
export 'contracts_screen/contracts_screen.dart';
export 'core/error_screen.dart';
export 'core/loading_screen.dart';
export 'login/login.dart';
export 'not_found_screen/not_found_screen.dart';
export 'register_screen/register.dart';
export 'report/report_picture.dart';
export 'report/report_screen.dart';
export 'widgets/c_m_app_bar.dart';
