import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';

class ReportImages extends StatelessWidget {
  const ReportImages({Key? key, required this.list}) : super(key: key);
  final BuiltList<String> list;
  @override
  Widget build(BuildContext context) => SizedBox(
        height: ((MediaQuery.of(context).size.height) * 2) / 3,
        width: (MediaQuery.of(context).size.width) - 10,
        child: ListView.builder(
          itemBuilder: (BuildContext ctx, int index) => Image.network(list[index], fit: BoxFit.cover),
          itemCount: list.length,
        ),
      );
}
