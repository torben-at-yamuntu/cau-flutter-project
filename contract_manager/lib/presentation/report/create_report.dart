import 'dart:io';
import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/application/create_report/create_report.dart';
import 'package:contract_manager/application/create_report/create_report_provider.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

class CreateReportScreen extends StatefulWidget {
  const CreateReportScreen({
    required this.serviceItem,
    Key? key,
  }) : super(key: key);
  final String title = 'createReport';
  final int current = 0;
  final ServiceItem serviceItem;

  @override
  _CreateReportScreenState createState() => _CreateReportScreenState();
}

class _CreateReportScreenState extends State<CreateReportScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _errorReport = false;
  final picker = ImagePicker();
  final List<File> _images = [];
  late final TextEditingController _messageController;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _images.add(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _messageController = TextEditingController();
  }

  @override
  void dispose() {
    _messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: CMAppBar(context: context),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 400),
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SwitchListTile(
                    title: const Text(
                      'Dies ist ein Mängelbericht',
                      style: TextStyle(fontSize: 15),
                    ),
                    value: _errorReport,
                    onChanged: (bool value) => setState(() => _errorReport = value),
                  ),
                  const Divider(height: 10),
                  const Text('Nachricht'),
                  const SizedBox(height: 10),
                  TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.zero),
                      ),
                    ),
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 10,
                    maxLength: null,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Bitte geben Sie eine Nachricht ein';
                      }
                      return null;
                    },
                  ),
                  const Divider(height: 10),
                  if (_images.isEmpty)
                    const Text(
                      'Noch keine Bilder hinzugefügt',
                    )
                  else
                    SizedBox(
                        height: ((MediaQuery.of(context).size.height) * 2) / 3,
                        width: (MediaQuery.of(context).size.width) - 10,
                        child: ListView.builder(
                          itemBuilder: (BuildContext ctx, int index) => Column(
                            children: [
                              Image.file(_images[index]),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Expanded(child: Container()),
                                  Text('Bild ${index + 1}'),
                                  Expanded(child: Container()),
                                  MaterialButton(
                                    onPressed: () => setState(() => _images.removeAt(index)),
                                    color: Colors.blue,
                                    textColor: Colors.white,
                                    padding: const EdgeInsets.all(10),
                                    shape: const CircleBorder(),
                                    child: const Icon(
                                      Icons.delete,
                                      size: 16,
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                          itemCount: _images.length,
                        )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                        onPressed: getImage,
                        child: const Text('Bild hochladen'),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                    ],
                  ),
                  const Divider(height: 10),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      onPressed: () {
                        // Validate returns true if the form is valid, or false
                        // otherwise.
                        if (_formKey.currentState!.validate()) {
                          // If the form is valid, display a Snackbar.
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Processing Data')));
                          try {
                            final UseCase<void, CreateReportParams> createReport = context.read(createReportProvider);

                            final List<Uint8List> imageBytes =
                                _images.map<Uint8List>((file) => file.readAsBytesSync()).toList();
                            createReport(CreateReportParams(
                              imageBytes: imageBytes,
                              isError: _errorReport,
                              message: _messageController.text,
                              serviceItem: widget.serviceItem,
                            ));
                          } catch (e) {
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Fehler!')));
                          }
                          context.router.pop();
                        }
                      },
                      child:
                          _errorReport ? const Text('Mängelbericht erstellen') : const Text('Dokumentation erstellen'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ));
}
