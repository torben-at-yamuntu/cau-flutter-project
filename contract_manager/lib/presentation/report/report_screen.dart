import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/presentation/report/report_picture.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';

class ReportScreen extends StatefulWidget {
  const ReportScreen({Key? key, required this.report}) : super(key: key);

  final Report report;

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  double width = 400;

  double height = 800;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: (MediaQuery.of(context).size.width) - 50,
            ),
            child: Form(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Card(
                  child: ConstrainedBox(
                      constraints: BoxConstraints(
                        maxWidth: (MediaQuery.of(context).size.width) - 50,
                      ),
                      child: Text((() {
                        if (widget.report.isError) {
                          return 'Fehler Bericht';
                        }
                        return 'Bericht';
                      })())),
                ),
                Card(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 400),
                    child: Text('Ersteller: ${widget.report.authorId}'),
                  ),
                ),
                Card(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 400),
                    child: Text('Beschreibung: ${widget.report.message}'),
                  ),
                ),
                Card(
                  child: SizedBox(
                    height: ((MediaQuery.of(context).size.height) * 2) / 3,
                    width: (MediaQuery.of(context).size.width) - 10,
                    child: ReportImages(
                      list: widget.report.imagePaths,
                    ),
                  ),
                ),
              ],
            )),
          ),
        ),
      );
}
