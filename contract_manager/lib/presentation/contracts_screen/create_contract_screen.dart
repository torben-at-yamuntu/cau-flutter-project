import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/application/application.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CreateContractScreen extends StatefulWidget {
  const CreateContractScreen({
    @PathParam('projectId') required this.projectId,
    Key? key,
  }) : super(key: key);

  final String projectId;

  @override
  CreateContractScreenState createState() => CreateContractScreenState();
}

class CreateContractScreenState extends State<CreateContractScreen> {
  List<ServiceItemParams> serviceItems = [];
  late final TextEditingController contractorIdController;
  late final TextEditingController specificationController;
  late final TextEditingController costController;
  late final TextEditingController nameController;
  late final GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();
    contractorIdController = TextEditingController();
    specificationController = TextEditingController();
    costController = TextEditingController();
    nameController = TextEditingController();
    _formKey = GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: CMAppBar(context: context),
      body: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 1080),
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(36),
                    child: Column(
                      children: <Widget>[
                        const Text(
                          'Einen Vertrag erstellen',
                          textAlign: TextAlign.center,
                        ),
                        TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              labelText: 'Vertragsname',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte geben Sie einen Vertragsnamen an';
                              }
                              return null;
                            }),
                        const SizedBox(height: 15),
                        TextFormField(
                          controller: contractorIdController,
                          decoration: const InputDecoration(
                            labelText: 'Vertragspartner ID',
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Bitte geben Sie die ID des Vertragspartners an';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 15),
                        TextFormField(
                          controller: costController,
                          decoration: const InputDecoration(
                            labelText: 'Kosten',
                          ),
                          validator: (value) {
                            if (value == null) {
                              return 'Bitte geben Sie eine valide Zahl ein!';
                            }
                            try {
                              double.parse(value);
                            } catch (e) {
                              return 'Bitte geben Sie eine valide Zahl ein!';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 15),
                        TextFormField(
                            controller: specificationController,
                            minLines: 5,
                            maxLines: 30,
                            decoration: const InputDecoration(
                              labelText: 'Vertragsinhalt',
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Bitte geben Sie den Vertragsinhalt an';
                              }
                              return null;
                            }),
                      ],
                    ),
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (_, index) => (index % 2 == 1)
                      ? CreateContractListTile(serviceItem: serviceItems[index ~/ 2], index: index ~/ 2 + 1)
                      : const Padding(
                          padding: EdgeInsets.only(left: 36, right: 36),
                          child: Divider(
                            height: 1,
                          ),
                        ),
                  childCount: serviceItems.length * 2 + 1,
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    CreateServiceItemButton(
                      onAddServiceItem: (params) {
                        setState(() {
                          serviceItems.add(params);
                        });
                      },
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          final createContract = context.read(createContractProvider);
                          final employeeId = context.read(authRepositoryProvider).getLoggedInUserId()!;

                          final costs = [
                            PaymentParams(
                              amount: double.parse(costController.text),
                              serviceItemIndexes: List<int>.generate(serviceItems.length, (i) => i + 1),
                            ),
                          ];

                          final createContractParams = CreateContractParams(
                            employeeId: employeeId,
                            projectId: '81JUgLHC4sW66ZPAaajR',
                            specification: specificationController.text,
                            costs: costs,
                            serviceItems: serviceItems,
                            contractorId: contractorIdController.text,
                            clientId: contractorIdController.text,
                            name: nameController.text,
                          );
                          createContract(createContractParams);
                        }
                      },
                      child: const Text('Vertrag erstellen'),
                    ),
                  ],
                ),
              ),
              const SliverToBoxAdapter(
                child: SizedBox(height: 15),
              ),
            ],
          ),
        ),
      ));
}

class CreateServiceItemButton extends StatefulWidget {
  const CreateServiceItemButton({required this.onAddServiceItem, Key? key}) : super(key: key);

  final void Function(ServiceItemParams params) onAddServiceItem;

  @override
  CreateServiceItemButtonState createState() => CreateServiceItemButtonState();
}

class CreateServiceItemButtonState extends State<CreateServiceItemButton> {
  List<ServiceItemInventoryParams> serviceItemInventories = [];
  late final TextEditingController startAtController;
  late final TextEditingController endPlannedAtController;
  late final TextEditingController specificationController;
  late final GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey();
    startAtController = TextEditingController();
    endPlannedAtController = TextEditingController();
    specificationController = TextEditingController();
  }

  @override
  void dispose() {
    startAtController.dispose();
    endPlannedAtController.dispose();
    specificationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(36),
        child: ExpansionTile(
          title: const Text('Füge eine Leistungsposition hinzu'),
          collapsedBackgroundColor: const Color(0x77101010),
          backgroundColor: const Color(0x77101010),
          trailing: const Icon(Icons.add),
          children: [
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(36),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Startdatum (Format yyyy-mm-dd)',
                        ),
                        controller: startAtController,
                        validator: (value) {
                          final bool dateValid =
                              RegExp(r'^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$').hasMatch(value.toString());
                          if (!dateValid) {
                            return 'Bitte geben Sie ein Datum an (Format yyyy-mm-dd)';
                          }
                          return null;
                        }),
                    const SizedBox(height: 15),
                    TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Enddatum (Format yyyy-mm-dd)',
                        ),
                        controller: endPlannedAtController,
                        validator: (value) {
                          final bool dateValid =
                              RegExp(r'^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$').hasMatch(value.toString());
                          if (!dateValid) {
                            return 'Bitte geben Sie ein Datum an (Format yyyy-mm-dd)';
                          }
                          return null;
                        }),
                    const SizedBox(height: 15),
                    TextFormField(
                        minLines: 5,
                        maxLines: 30,
                        decoration: const InputDecoration(
                          labelText: 'Inhalt der Leistungsposition',
                        ),
                        controller: specificationController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Bitte geben Sie den Inhalt der Leistungsposition an';
                          }
                          return null;
                        }),
                    const SizedBox(height: 15),
                    SizedBox(
                      height: serviceItemInventories.length * 30 + 10,
                      child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: serviceItemInventories.length,
                        itemBuilder: (BuildContext context, int index) => ServiceItemInventoryList(
                          serviceItemInventory: serviceItemInventories[index],
                        ),
                      ),
                    ),
                    const SizedBox(height: 15),
                    CreateAmountButton(
                      onAddServiceItemInventory: (params) {
                        setState(() {
                          serviceItemInventories.add(params);
                        });
                      },
                    ),
                    const SizedBox(height: 15),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();

                          final serviceItemParams = ServiceItemParams(
                            endPlannedAt: DateTime.parse(endPlannedAtController.text),
                            startAt: DateTime.parse(endPlannedAtController.text),
                            inventories: serviceItemInventories,
                            specification: specificationController.text,
                          );
                          startAtController.clear();
                          endPlannedAtController.clear();
                          specificationController.clear();
                          widget.onAddServiceItem(serviceItemParams);
                        }
                      },
                      child: const Text('Leistungsposition erstellen'),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}

class CreateAmountButton extends StatefulWidget {
  const CreateAmountButton({required this.onAddServiceItemInventory, Key? key}) : super(key: key);

  final void Function(ServiceItemInventoryParams params) onAddServiceItemInventory;

  @override
  CreateAmountButtonState createState() => CreateAmountButtonState();
}

class CreateAmountButtonState extends State<CreateAmountButton> {
  QuantityUnit _selectedUnit = QuantityUnit.kg;

  late final TextEditingController materialController;
  late final TextEditingController quantityController;
  late final TextEditingController priceController;
  late final GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    materialController = TextEditingController();
    quantityController = TextEditingController();
    priceController = TextEditingController();
  }

  @override
  void dispose() {
    materialController.dispose();
    quantityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(36),
        child: Form(
          key: _formKey,
          child: ExpansionTile(
            title: const Text('Füge eine Materialmenge hinzu'),
            collapsedBackgroundColor: const Color(0x77000000),
            backgroundColor: const Color(0x77000000),
            trailing: const Icon(Icons.add),
            children: [
              Padding(
                padding: const EdgeInsets.all(36),
                child: Wrap(runSpacing: 20, alignment: WrapAlignment.center, children: [
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 200),
                    child: TextFormField(
                        controller: materialController,
                        decoration: const InputDecoration(
                          labelText: 'Material',
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Bitte geben Sie ein Material an';
                          }
                          return null;
                        }),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 199),
                    child: TextFormField(
                        controller: quantityController,
                        decoration: const InputDecoration(
                          labelText: 'Menge',
                        ),
                        validator: (value) {
                          if (value == null) return 'Bitte geben Sie ein Material an (Format 123.45)';
                          try {
                            double.parse(value);
                          } catch (e) {
                            return 'Bitte geben Sie ein Material an (Format 123.45)';
                          }
                          return null;
                        }),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  DropdownButton(
                    hint: const Text('kg'),
                    value: _selectedUnit,
                    items: QuantityUnit.values
                        .map((unit) => DropdownMenuItem(
                              value: unit,
                              child: Text(unit.toPrettyString()),
                            ))
                        .toList(),
                    onChanged: (QuantityUnit? newValue) {
                      if (newValue != null) {
                        setState(() {
                          _selectedUnit = newValue;
                        });
                      }
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState!.save();
                            final ServiceItemInventoryParams params = ServiceItemInventoryParams(
                              material: materialController.text,
                              quantity: double.parse(quantityController.text),
                              unit: _selectedUnit,
                              price: double.parse(priceController.text),
                            );
                            widget.onAddServiceItemInventory(params);
                            materialController.clear();
                            quantityController.clear();
                            _selectedUnit = QuantityUnit.kg;
                          }
                        },
                        child: const Text('Menge erstellen'),
                      ),
                    ],
                  ),
                ]),
              ),
            ],
          ),
        ),
      );
}

class CreateContractListTile extends StatelessWidget {
  const CreateContractListTile({
    Key? key,
    required this.serviceItem,
    required this.index,
  }) : super(key: key);

  final ServiceItemParams serviceItem;
  final int index;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(left: 36, right: 36),
        child: ExpansionTile(
          title: Text('Leistungsposition $index'),
          collapsedBackgroundColor: const Color(0x77202020),
          backgroundColor: const Color(0x77202020),
          children: [
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('Startdatum: ${formatDate(serviceItem.startAt)}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('Enddatum: ${formatDate(serviceItem.endPlannedAt)}', textAlign: TextAlign.center),
                ),
              ],
            ),
            const Text(''),
            Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Container(),
                ),
                Flexible(
                  flex: 6,
                  child: Center(
                    child: Text('Beschreibung: ${serviceItem.specification}\n', textAlign: TextAlign.center),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(),
                ),
              ],
            ),
            ListView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              itemCount: serviceItem.inventories.length,
              itemBuilder: (BuildContext context, int index) => ServiceItemInventoryList(
                serviceItemInventory: serviceItem.inventories[index],
              ),
            ),
          ],
        ),
      );

  String formatDate(DateTime date) => '${date.day}.${date.month}.${date.year}';
}

class ServiceItemInventoryList extends StatelessWidget {
  const ServiceItemInventoryList({
    Key? key,
    required this.serviceItemInventory,
  }) : super(key: key);

  final ServiceItemInventoryParams serviceItemInventory;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          height: 30,
          color: const Color(0x77353535),
          child: Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text('${serviceItemInventory.material}   '),
                ),
              ),
              const Text('-'),
              Expanded(
                child: Text('   ${serviceItemInventory.quantity} ${serviceItemInventory.unit.toPrettyString()}'),
              )
            ],
          ),
        ),
      );
}
