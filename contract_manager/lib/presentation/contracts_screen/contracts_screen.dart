import 'package:auto_route/auto_route.dart';
import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/application/contract_stream.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:contract_manager/presentation/core/error_screen.dart';
import 'package:contract_manager/presentation/core/loading_screen.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:contract_manager/presentation/routing/app_router.gr.dart';
import 'package:contract_manager/presentation/widgets/c_m_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContractsScreen extends ConsumerWidget {
  const ContractsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, Reader watch) {
    final AsyncValue<BuiltList<ContractsSectionViewModel>> contractsState = watch(contractsStreamProvider);
    return contractsState.when<Widget>(
      loading: () => const LoadingScreen(),
      error: (_, __) => const ErrorScreen(),
      data: (contracts) => ContractsLoadedScreen(contractsSections: contracts),
    );
  }
}

class ContractsLoadedScreen extends StatelessWidget {
  const ContractsLoadedScreen({
    required this.contractsSections,
    Key? key,
  }) : super(key: key);

  final BuiltList<ContractsSectionViewModel> contractsSections;
  Future<String> getCompanyID(BuildContext context) async {
    final String? employeeID = context.read(authRepositoryProvider).getLoggedInUserId();
    final Employee employee =
        await context.read(employeeRepositoryProvider).watchEmployee(employeeId: employeeID!).first;
    return employee.companyId;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 1080),
            child: CustomScrollView(
              slivers: [
                for (final section in contractsSections) ...[
                  ProjectTile(
                    project: section.project,
                    contractor: section.client,
                  ),
                  ContractsSection(contractsSection: section),
                  const SliverToBoxAdapter(child: SizedBox(height: 30)),
                ],
                SliverToBoxAdapter(
                    child: ListTile(
                  title: const Text('Projekt erstellen'),
                  tileColor: const Color(0x77000000),
                  onTap: () async {
                    final String companyID = await getCompanyID(context);
                    return context.router.push(
                      CreateProjectScreenRoute(companyId: companyID),
                    );
                  },
                  trailing: const Icon(Icons.add),
                )),
              ],
            ),
          ),
        ),
      );
}

class ProjectTile extends StatelessWidget {
  const ProjectTile({
    required this.project,
    required this.contractor,
    Key? key,
  }) : super(key: key);

  final Project project;
  final String contractor;

  @override
  Widget build(BuildContext context) => SliverToBoxAdapter(
        child: ExpansionTile(
          title: Text(project.name),
          collapsedBackgroundColor: const Color(0x77000000),
          backgroundColor: const Color(0x77000000),
          children: [
            Padding(
              padding: const EdgeInsets.all(0),
              child: Column(
                children: [
                  Wrap(
                    runSpacing: 20,
                    children: [
                      SizedBox(
                        width: 400,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SelectableText('Kontodaten'),
                            const SizedBox(height: 5),
                            Row(
                              children: [
                                const SizedBox(
                                  width: 100,
                                  child: SelectableText('Name:'),
                                ),
                                SelectableText(contractor),
                              ],
                            ),
                            Row(
                              children: [
                                const SizedBox(
                                  width: 100,
                                  child: SelectableText('IBAN:'),
                                ),
                                SelectableText(contractor),
                              ],
                            ),
                            Row(
                              children: [
                                const SizedBox(
                                  width: 100,
                                  child: SelectableText('BIC:'),
                                ),
                                SelectableText(contractor),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 400,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Addresse'),
                            const SizedBox(height: 5),
                            Row(
                              children: [
                                SelectableText(contractor),
                                const Text(' '),
                                SelectableText(contractor),
                              ],
                            ),
                            Row(
                              children: [
                                SelectableText(contractor),
                                const Text(' '),
                                SelectableText(contractor),
                                const Text(', '),
                                SelectableText(contractor),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 30),
                  const SelectableText('Projekt ID:'),
                  SelectableText(
                    project.id,
                    style: const TextStyle(fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

class ContractsSection extends StatelessWidget {
  const ContractsSection({
    required this.contractsSection,
    Key? key,
  }) : super(key: key);

  final ContractsSectionViewModel contractsSection;

  @override
  Widget build(BuildContext context) => SliverList(
        delegate: SliverChildBuilderDelegate(
          (_, index) => (index == contractsSection.contracts.length * 2 + 1)
              ? Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: const Color(0x77303030),
                      ),
                      onPressed: () {
                        context.router.pushPath('project/$contractsSection.project.id/createContract');
                      },
                      child: const Text('Vertrag erstellen'),
                    ),
                  ],
                )
              : (index % 2 == 1)
                  ? ContractListTile(
                      contract: contractsSection.contracts[index ~/ 2], project: contractsSection.project)
                  : const Divider(
                      height: 1,
                    ),
          childCount: contractsSection.contracts.length * 2 + 2,
        ),
      );
}

class ContractListTile extends StatelessWidget {
  const ContractListTile({
    Key? key,
    required this.contract,
    required this.project,
  }) : super(key: key);

  final Contract contract;
  final Project project;
  @override
  Widget build(BuildContext context) => ListTile(
        title: Text(contract.specification),
        onTap: () => context.router.push(ContractDetailScreenRoute(
          contractId: contract.id,
          projectId: project.id,
        )),
      );
}
