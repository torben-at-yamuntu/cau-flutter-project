import 'package:auto_route/auto_route.dart';
import 'package:contract_manager/application/contract_stream.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/presentation/company_screen/company_management_screen.dart';
import 'package:contract_manager/presentation/presentation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContractDetailScreen extends ConsumerWidget {
  const ContractDetailScreen({
    @PathParam('contractId') required this.contractId,
    @PathParam('projectId') required this.projectId,
    Key? key,
  }) : super(key: key);

  //make UseCase because has to load contract than all ServiceItems from the ServiceItem Repository by "watchServiceItems"

  final String contractId;
  final String projectId;

  @override
  Widget build(BuildContext context, Reader watch) {
    final AsyncValue<Contract> contractState = watch(
      contractStreamProvider(
        ContractParams(
          contractId: contractId,
          projectId: projectId,
        ),
      ),
    );
    return contractState.when<Widget>(
      loading: () => const LoadingScreen(),
      error: (error, stackTrace) {
        print(error);
        print(stackTrace);
        return const LoginScreen();
      },
      data: (contract) => ContractDetailLoadedScreen(contract: contract),
    );
  }
}

class ContractDetailLoadedScreen extends StatelessWidget {
  const ContractDetailLoadedScreen({
    required this.contract,
    Key? key,
  }) : super(key: key);

  final Contract contract;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CMAppBar(context: context),
        body: Center(
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text('ID:'),
                      Text(' ${contract.id}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('Name: '),
                      Text('${contract.name}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('ProjektID: '),
                      Text(contract.projectId),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('Erstellungsdatum: '),
                      Text('${contract.createdAt.toString()}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('VertagsgeberID: '),
                      Text('${contract.contractorId.toString()}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('VertragnehmerID: '),
                      Text('${contract.clientId}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('Vertrag angenommen:'),
                      Text('${contract.acceptedBy.toString()}'),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('Details:'),
                      Text('${contract.specification}'),
                    ],
                  ),
                  BankInfoScreen(
                    bankinfo: contract.bankInfo,
                  ),
                  //costs print!!
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxHeight: 400),
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: contract.costs.length,
                      itemBuilder: (_, index) => ListTile(
                        title: Text(contract.costs[index].status.toString()),
                        subtitle: PaymentWidget(payment: contract.costs[index]),
                      ),
                    ),
                  ),
                  /* 
                  Text('costs: ${contract.costs}'), */
                  Text('gehört zu Vertrag: ${contract.parentServiceItemId}'),
                ],
              ),
            ),
          ),
        ),
      );
}

class PaymentWidget extends StatelessWidget {
  const PaymentWidget({
    Key? key,
    required this.payment,
  }) : super(key: key);

  final Payment payment;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Text('ID: ${payment.id}'),
          Text('ID: ${payment.amount}'),
          Text('ID: ${payment.status}'),
          Text('ID: ${payment.serviceItemIds}'),
          Text('ID: ${payment.sentPaymentAt}'),
          Text('ID: ${payment.receivedPaymentAt}'),
          Text('ID: ${payment.paymentActiveAt}'),
        ],
      );
}
