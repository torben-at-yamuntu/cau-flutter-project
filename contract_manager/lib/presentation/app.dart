import 'package:contract_manager/presentation/routing/app_router.gr.dart';
import 'package:contract_manager/presentation/routing/auth_guard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContractManager extends StatelessWidget {
  ContractManager({Key? key}) : super(key: key);

  final AppRouter _appRouter = AppRouter(authGuard: AuthGuard());
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();

  @override
  Widget build(BuildContext context) => ProviderScope(
        child: MaterialApp.router(
          key: _navigatorKey,
          title: 'Contract Manager',
          theme: ThemeData(),
          darkTheme: ThemeData.dark(),
          routerDelegate: _appRouter.delegate(),
          routeInformationParser: _appRouter.defaultRouteParser(),
        ),
      );
}
