import 'package:contract_manager/application/service_Item/complete_service_item.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final completeServiceItemProvider = Provider<UseCase<void, CompleteServiceItemParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IServiceItemRepository reportRepository = ref.watch(serviceItemRepositoryProvider);
  final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
  return CompleteServiceItem(
    reportRepository,
    batchRepository,
    authRepository,
  );
});
