import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class CreateServiceItem extends UseCase<ServiceItem, CreateServiceItemParams> {
  CreateServiceItem(this.repository, this.batchRepository);

  final IServiceItemRepository repository;
  final IBatchRepository batchRepository;

  @override
  Future<ServiceItem> call(CreateServiceItemParams params) async {
    final batch = batchRepository.getBatch();

    final ServiceItem serviceItem = await repository.create(
      projectId: params.projectId,
      contractId: params.contractId,
      specification: params.specification,
      startAt: params.startAt,
      endPlannedAt: params.endPlannedAt,
      inventories: params.inventories,
      batch: batch,
    );

    await repository.updateLastSeenVersion(
      serviceItem: serviceItem,
      employeeId: params.employeeId,
      batch: batch,
    );

    await batch.commit();

    return serviceItem;
  }
}

class CreateServiceItemParams extends Equatable {
  const CreateServiceItemParams({
    required this.projectId,
    required this.contractId,
    required this.specification,
    required this.startAt,
    required this.endPlannedAt,
    required this.inventories,
    required this.employeeId,
  });

  final String projectId;
  final String contractId;
  final String specification;
  final DateTime startAt;
  final DateTime endPlannedAt;
  final List<ServiceItemInventoryParams> inventories;
  final String employeeId;

  @override
  List<Object?> get props => [projectId, contractId, specification, startAt, endPlannedAt, employeeId];
}
