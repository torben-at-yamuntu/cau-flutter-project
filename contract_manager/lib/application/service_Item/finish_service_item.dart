import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class FinishServiceItem extends UseCase<void, FinishServiceItemParams> {
  FinishServiceItem(this.sRepository, this.batchRepository);

  final IServiceItemRepository sRepository;
  final IBatchRepository batchRepository;

  @override
  Future<void> call(FinishServiceItemParams params) async {
    final batch = batchRepository.getBatch();

    params.inventoryActualAmounts.forEach((inventoryId, actualAmount) async {
      await sRepository.updateServiceItemQuantity(
        serviceItem: params.serviceItem,
        serviceItemInventoryId: inventoryId,
        quantity: actualAmount,
        batch: batch,
      );
    });

    await sRepository.updateWorkDoneAtToNow(serviceItem: params.serviceItem);

    await sRepository.incrementVersion(serviceItem: params.serviceItem);

    await sRepository.updateLastSeenVersion(
      serviceItem: params.serviceItem,
      employeeId: params.employeeId,
      batch: batch,
    );

    await batch.commit();
  }
}

class FinishServiceItemParams extends Equatable {
  const FinishServiceItemParams({
    required this.serviceItem,
    required this.employeeId,
    required this.inventoryActualAmounts,
  });

  final ServiceItem serviceItem;
  final String employeeId;
  final Map<String, double> inventoryActualAmounts;

  @override
  List<Object?> get props => [serviceItem, employeeId, inventoryActualAmounts];
}
