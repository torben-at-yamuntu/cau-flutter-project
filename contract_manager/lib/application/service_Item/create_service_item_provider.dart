import 'package:contract_manager/application/service_item/create_service_item.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createServiceItemProvider = Provider<UseCase<void, CreateServiceItemParams>>((ref) {
  final IServiceItemRepository sRepository = ref.watch(serviceItemRepositoryProvider);
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  return CreateServiceItem(sRepository, batchRepository);
});
