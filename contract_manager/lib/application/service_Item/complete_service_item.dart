import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/service_item/service_item_failure.dart';
import 'package:equatable/equatable.dart';

class CompleteServiceItem extends UseCase<void, CompleteServiceItemParams> {
  CompleteServiceItem(this.repository, this.batchRepository, this.authRepository);

  final IServiceItemRepository repository;
  final IBatchRepository batchRepository;
  final IAuthRepository authRepository;

  @override
  Future<void> call(CompleteServiceItemParams params) async {
    final batch = batchRepository.getBatch();

    final String? employeeId = authRepository.getLoggedInUserId();
    if (employeeId == null) {
      throw const ServiceItemFailure.unexpected();
    }

    await repository.updateApprovedAtToNow(
      batch: batch,
      serviceItem: params.serviceItem,
    );

    await repository.updateLastSeenVersion(
      serviceItem: params.serviceItem,
      employeeId: employeeId,
      batch: batch,
    );

    await batch.commit();
  }
}

class CompleteServiceItemParams extends Equatable {
  const CompleteServiceItemParams({
    required this.serviceItem,
  });

  final ServiceItem serviceItem;

  @override
  List<Object?> get props => [
        serviceItem,
      ];
}
