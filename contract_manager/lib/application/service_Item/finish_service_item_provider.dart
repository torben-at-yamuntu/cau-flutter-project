import 'package:contract_manager/application/service_item/finish_service_item.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final finishServiceItemProvider = Provider<UseCase<void, FinishServiceItemParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IServiceItemRepository sRepository = ref.watch(serviceItemRepositoryProvider);
  return FinishServiceItem(sRepository, batchRepository);
});
