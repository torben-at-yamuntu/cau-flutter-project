import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/transaction/i_transaction_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class RetractSubContract extends UseCase<void, RetractSubContractParams> {
  RetractSubContract(
    this.cRepository,
    this.sRepository,
    this.transactionRepository,
  );

  final IContractRepository cRepository;
  final IServiceItemRepository sRepository;
  final ITransactionRepository transactionRepository;

  @override
  Future<void> call(RetractSubContractParams params) async {
    await transactionRepository.runTransaction((transaction) async {
      final Contract canceledSubContract = await cRepository.getContractById(
          projectId: params.canceledSubServiceItem.projectId,
          contractId: params.canceledSubServiceItem.contractId,
          transaction: transaction);
      final ServiceItem outsourcedServiceItem = await sRepository.getServiceItemById(
        projectId: params.canceledSubServiceItem.projectId,
        contractId: params.canceledSubServiceItem.outsourcedContractId!,
        serviceItemId: canceledSubContract.parentServiceItemId!,
      );

      //Delete Sub Contract
      await cRepository.deleteContract(
        contract: canceledSubContract,
        transaction: transaction,
      );
      //Change ServiceItem, which was used to create the subContract
      await sRepository.setSubContractId(serviceItem: outsourcedServiceItem, subContractIdOption: optionOf(null));

      await sRepository.updateLastSeenVersion(serviceItem: outsourcedServiceItem, employeeId: params.employeeId);
    });
  }
}

class RetractSubContractParams extends Equatable {
  const RetractSubContractParams({
    required this.canceledSubServiceItem,
    required this.employeeId,
  });
  final ServiceItem canceledSubServiceItem;
  final String employeeId;

  @override
  List<Object?> get props => [
        canceledSubServiceItem,
        employeeId,
      ];
}
