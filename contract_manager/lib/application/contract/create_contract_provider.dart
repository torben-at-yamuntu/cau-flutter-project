import 'package:contract_manager/application/contract/create_contract.dart';
import 'package:contract_manager/application/service_item/create_service_item.dart';
import 'package:contract_manager/application/service_item_stream.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createContractProvider = Provider<UseCase<void, CreateContractParams>>((ref) {
  final IContractRepository contractRepository = ref.watch(contractRepositoryProvider);
  final ITransactionRepository transactionRepository = ref.watch(transactionRepositoryProvider);
  final ICompanyRepository companyRepository = ref.watch(companyRepositoryProvider);
  final IProjectRepository projectRepository = ref.watch(projectRepositoryProvider);

  final UseCase<ServiceItem, CreateServiceItemParams> createServiceItem = ref.watch(createServiceItemProvider);

  return CreateContract(
    contractRepository,
    companyRepository,
    projectRepository,
    transactionRepository,
    createServiceItem,
  );
});
