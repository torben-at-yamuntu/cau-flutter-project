import 'package:contract_manager/application/register/register_company_uc.dart';
import 'package:contract_manager/domain/auth/i_auth_repository.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/company/i_company_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/employee/i_employee_repository.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createCompanyProvider = Provider<UseCase<void, RegisterCompanyParams>>((ref) {
  final IAuthRepository authRepo = ref.watch(authRepositoryProvider);
  final IBatchRepository batchRepo = ref.watch(batchRepositoryProvider);
  final ICompanyRepository companyRepo = ref.watch(companyRepositoryProvider);
  final IEmployeeRepository employeeRepo = ref.watch(employeeRepositoryProvider);
  return RegisterCompanyUseCase(authRepo, batchRepo, companyRepo, employeeRepo);
});
