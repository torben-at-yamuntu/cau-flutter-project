import 'package:contract_manager/application/service_item/create_service_item.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class CreateContract extends UseCase<void, CreateContractParams> {
  CreateContract(
    this.contractRepository,
    this.companyRepository,
    this.projectRepository,
    this.transactionRepository,
    this.createServiceItem,
  );

  final IContractRepository contractRepository;
  final ICompanyRepository companyRepository;
  final IProjectRepository projectRepository;
  final ITransactionRepository transactionRepository;

  final UseCase<ServiceItem, CreateServiceItemParams> createServiceItem;

  @override
  Future<void> call(CreateContractParams params) async {
    await transactionRepository.runTransaction((transaction) async {
      final Project project = await projectRepository.getProject(projectId: params.projectId);
      final Company client = await companyRepository.getCompany(companyId: params.clientId);

      final Contract contract = await contractRepository.create(
          bankInfo: client.bankInfo,
          projectId: params.projectId,
          contractorId: params.contractorId,
          clientId: client.id,
          specification: params.specification,
          transaction: transaction,
          name: params.name);

      final List<ServiceItem> serviceItems = [];
      for (final serviceItemParams in params.serviceItems) {
        final createParams = CreateServiceItemParams(
          projectId: params.projectId,
          contractId: contract.id,
          specification: serviceItemParams.specification,
          startAt: serviceItemParams.startAt,
          endPlannedAt: serviceItemParams.endPlannedAt,
          inventories: serviceItemParams.inventories,
          employeeId: params.employeeId,
        );
        final serviceItem = await createServiceItem(createParams);
        serviceItems.add(serviceItem);
      }

      for (final paymentParam in params.costs) {
        final List<String> serviceItemIds =
            paymentParam.serviceItemIndexes.map((index) => serviceItems[index].id).toList();
        await contractRepository.addPayment(
          contract: contract,
          amount: paymentParam.amount,
          serviceItemIds: serviceItemIds,
          transaction: transaction,
        );
      }

      await contractRepository.updateLastSeenVersion(
        contract: contract,
        employeeId: params.employeeId,
        transaction: transaction,
      );
    });
  }
}

class CreateContractParams extends Equatable {
  const CreateContractParams({
    required this.name,
    required this.employeeId,
    required this.projectId,
    required this.contractorId,
    required this.specification,
    required this.costs,
    required this.serviceItems,
    required this.clientId,
  });
  final String clientId;
  final String name;
  final String employeeId;
  final String projectId;
  final String contractorId;
  final String specification;
  final List<PaymentParams> costs;
  final List<ServiceItemParams> serviceItems;

  @override
  List<Object?> get props => [
        name,
        employeeId,
        projectId,
        contractorId,
        specification,
        costs,
        serviceItems,
        clientId,
      ];
}

class PaymentParams extends Equatable {
  const PaymentParams({
    required this.amount,
    required this.serviceItemIndexes,
  });

  final double amount;
  final List<int> serviceItemIndexes;

  @override
  List<Object?> get props => [amount, serviceItemIndexes];
}

class ServiceItemParams extends Equatable {
  const ServiceItemParams({
    required this.specification,
    required this.startAt,
    required this.endPlannedAt,
    required this.inventories,
  });

  final String specification;
  final DateTime startAt;
  final DateTime endPlannedAt;
  final List<ServiceItemInventoryParams> inventories;

  @override
  List<Object?> get props => [
        specification,
        startAt,
        endPlannedAt,
        inventories,
      ];
}
