import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class CreateSubContract extends UseCase<void, CreateSubContractParams> {
  CreateSubContract(
      this.cRepository, this.sRepository, this.transactionRepository, this.companyRepository, this.projectRepository);

  final IContractRepository cRepository;
  final IServiceItemRepository sRepository;
  final ICompanyRepository companyRepository;
  final ITransactionRepository transactionRepository;
  final IProjectRepository projectRepository;

  final String _specification = 'Ausgelagerter Vertrag';

  @override
  Future<Contract> call(CreateSubContractParams params) async =>
      await transactionRepository.runTransaction((transaction) async {
        //fetch new Contractor
        final Company newContractor = await companyRepository.getCompany(
          companyId: params.newContractorId,
          transaction: transaction,
        );
// TODO(jannes): find Company by name.

        //fetch current Contract
        final Contract currentContract = await cRepository.getContractById(
          projectId: params.outsourcedServiceItem.projectId,
          contractId: params.outsourcedServiceItem.contractId,
          transaction: transaction,
        );

        //Create Subcontract.
        final Contract subContract = await cRepository.create(
            name: 'Ausgelagert von: {currentContract.name}',
            projectId: params.outsourcedServiceItem.projectId,
            contractorId: params.newContractorId,
            clientId: currentContract.contractorId,
            specification: _specification,
            bankInfo: newContractor.bankInfo,
            transaction: transaction);

        //Create new ServiceItem in SubContract.
        final ServiceItem subServiceItem = await sRepository.create(
            projectId: currentContract.projectId,
            contractId: subContract.id,
            specification: params.outsourcedServiceItem.specification,
            startAt: DateTime.now(),
            endPlannedAt: params.outsourcedServiceItem.endPlannedAt,
            inventories: params.outsourcedServiceItem.inventories.values
                .map<ServiceItemInventoryParams>((serviceItemInventory) => serviceItemInventory.toParams())
                .toList(),
            transaction: transaction);
        //Update info of new Contract
        await cRepository.setParentServiceItemId(
          contract: subContract,
          parentServiceItemId: params.outsourcedServiceItem.id,
          transaction: transaction,
        );

        await cRepository.addPayment(
          contract: subContract,
          amount: subServiceItem.inventories.values
              .map<double>((serviceItemInventory) => serviceItemInventory.price * serviceItemInventory.quantity)
              .fold(0, (previousValue, element) => previousValue + element),
          serviceItemIds: [subServiceItem.id],
          transaction: transaction,
        );
// TODO(Jannes): fix costs

        //Update info of new ServiceItem
        await sRepository.setOldContractId(
          serviceItem: subServiceItem,
          outsourcedContractId: currentContract.id,
          transaction: transaction,
        );
        //Update info of old ServiceItem
        await sRepository.setSubContractId(
          serviceItem: params.outsourcedServiceItem,
          subContractIdOption: optionOf(subContract.id),
          transaction: transaction,
        );

        await sRepository.updateLastSeenVersion(
            serviceItem: params.outsourcedServiceItem, employeeId: params.employeeId);
        final Contract finalSubContract =
            await cRepository.getContractById(projectId: subContract.projectId, contractId: subContract.id);
        return finalSubContract;
      }) as Contract;
}

class CreateSubContractParams extends Equatable {
  const CreateSubContractParams({
    required this.outsourcedServiceItem,
    required this.employeeId,
    required this.newContractorId,
  });
  final ServiceItem outsourcedServiceItem;
  final String employeeId;
  final String newContractorId;

  @override
  List<Object?> get props => [
        outsourcedServiceItem,
        employeeId,
        newContractorId,
      ];
}
