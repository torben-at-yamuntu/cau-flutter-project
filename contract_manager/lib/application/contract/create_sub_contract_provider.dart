// import 'package:contract_manager/application/contract/create_sub_contract.dart';
// import 'package:contract_manager/domain/contract/i_contract_repository.dart';
// import 'package:contract_manager/domain/core/use_case.dart';
// import 'package:contract_manager/domain/domain.dart';
// import 'package:contract_manager/infrastructure/repositories.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';

// final createSubContractProvider = Provider<UseCase<void, CreateSubContractParams>>((ref) {
//   final IContractRepository cRepository = ref.watch(contractRepositoryProvider);
//   final IServiceItemRepository sRepository = ref.watch(serviceItemRepositoryProvider);
//   final ICompanyRepository companyRepository = ref.watch(companyRepositoryProvider);
//   final ITransactionRepository transactionRepository = ref.watch(transactionRepositoryProvider);
//   final IProjectRepository projectRepository = ref.watch(projectRepositoryProvider);
//   return CreateSubContract(
//     cRepository,
//     sRepository,
//     transactionRepository,
//     companyRepository,
//     projectRepository,
//   );
// });
