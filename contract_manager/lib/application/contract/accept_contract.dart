import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

/// Accept a Contract by EmployeeId and update lastSeenVersions
class AcceptContract extends UseCase<void, AcceptContractParams> {
  AcceptContract({required this.contractRepository, required this.transactionRepository});

  final IContractRepository contractRepository;
  final ITransactionRepository transactionRepository;

  @override
  Future<void> call(AcceptContractParams params) => transactionRepository.runTransaction(
        (transaction) async {
          await contractRepository.acceptContract(
            contract: params.contract,
            employeeId: params.employeeId,
            transaction: transaction,
          );
          await contractRepository.updateLastSeenVersion(
            contract: params.contract,
            employeeId: params.employeeId,
            transaction: transaction,
          );
        },
      );
}

class AcceptContractParams extends Equatable {
  const AcceptContractParams({
    required this.contract,
    required this.employeeId,
  });

  final Contract contract;
  final String employeeId;

  @override
  List<Object?> get props => [contract, employeeId];
}
