import 'package:contract_manager/application/contract/retract_sub_contract.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final retractSubContractProvider = Provider<UseCase<void, RetractSubContractParams>>((ref) {
  final IContractRepository cRepository = ref.watch(contractRepositoryProvider);
  final IServiceItemRepository sRepository = ref.watch(serviceItemRepositoryProvider);
  final ITransactionRepository transactionRepository = ref.watch(transactionRepositoryProvider);
  return RetractSubContract(
    cRepository,
    sRepository,
    transactionRepository,
  );
});
