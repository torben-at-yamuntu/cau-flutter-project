/// The application layer contains all the business logic of the app.
///
/// It is completely independent of the used ui framework and backend
/// and contains just pure dart code. This enables us if we want to change
/// away from flutter to take the application layer and use it in the
/// new project.
/// The application layer is responsible for the state management of the
/// app in a general way. That means, small ui changes, which are not
/// effecting the database like a toggle button to accept TOS for example
/// will not be handled in the application layer. But bigger changes which
/// have side effects on multiple pages will be handled by the application
/// layer.
/// Usually the application layer contains state machines to manage state
/// transitions and provide all use cases of the app.
library application_layer;

export 'company_stream.dart';
export 'contract/create_contract.dart';
export 'contract/create_contract_provider.dart';
export 'contract_stream.dart';
export 'employee_stream.dart';
export 'project_stream.dart';
