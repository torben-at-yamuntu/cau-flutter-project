import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final projectFutureProvider = FutureProvider.family<Project, String>((ref, projectId) {
  final IProjectRepository projectRepository = ref.watch(projectRepositoryProvider);
  return projectRepository.getProject(projectId: projectId);
});

class ProjectViewModel extends Equatable {
  const ProjectViewModel({
    required this.project,
  });

  final Project project;

  @override
  List<Object?> get props => [project];
}
