import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/application/service_item/create_service_item.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const ServiceItemInventory inventory1 = ServiceItemInventoryModel(
  id: 'Inventar',
  material: 'Rohre',
  quantity: 20,
  quantityUnit: QuantityUnit.kg,
  price: 20,
);

const ServiceItemInventory inventory2 = ServiceItemInventoryModel(
    id: 'Inventar2', material: 'Rohre', quantity: 230, quantityUnit: QuantityUnit.kg, price: 30);

const ServiceItemInventory inventory3 = ServiceItemInventoryModel(
    id: 'Inventar3', material: 'Rohre', quantity: 220, quantityUnit: QuantityUnit.m3, price: 40);

final ServiceItem item1 = ServiceItemModel(
  contractId: '1',
  id: '2',
  projectId: '4',
  specification: 'Rohr verlegen',
  startsAt: DateTime.now(),
  endPlannedAt: DateTime.now(),
  inventories: BuiltMap<String, ServiceItemInventory>(<ServiceItemInventory>[inventory1, inventory2, inventory3]),
  versionNumber: 1,
  lastSeenVersions: BuiltMap<String, int>(const <String, int>{}),
);
final ServiceItem item2 = ServiceItemModel(
  contractId: '7',
  id: '4',
  projectId: '5',
  specification: 'Kabel verlegen',
  startsAt: DateTime.now(),
  endPlannedAt: DateTime.now(),
  inventories: BuiltMap<String, ServiceItemInventory>(<String, ServiceItemInventory>{
    'id1': inventory1,
    'id2': inventory2,
    'id3': inventory3,
  }),
  versionNumber: 2,
  lastSeenVersions: BuiltMap<String, int>(const <String, int>{}),
);
final serviceItemStreamProvider = StreamProvider.family<ServiceItem, ServiceItemParams>(
  (ref, params) async* {
    if (params.serviceItemId == '1') yield item1;
    yield item2;
  },
);

final createServiceItemProvider = Provider<UseCase<ServiceItem, CreateServiceItemParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IServiceItemRepository serviceItemRepository = ref.watch(serviceItemRepositoryProvider);

  return CreateServiceItem(serviceItemRepository, batchRepository);
});

class ServiceItemParams extends Equatable {
  const ServiceItemParams({
    required this.projectId,
    required this.serviceItemId,
    required this.contractId,
  });

  final String projectId;
  final String serviceItemId;
  final String contractId;

  @override
  List<Object?> get props => [
        projectId,
        serviceItemId,
        contractId,
      ];
}
