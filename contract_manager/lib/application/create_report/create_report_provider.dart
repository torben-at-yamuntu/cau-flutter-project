import 'package:contract_manager/application/create_report/create_report.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createReportProvider = Provider<UseCase<void, CreateReportParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IServiceItemRepository reportRepository = ref.watch(serviceItemRepositoryProvider);
  final IStorageRepository storageRepository = ref.watch(storageRepositoryProvider);
  final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
  return CreateReport(reportRepository, batchRepository, storageRepository, authRepository);
});
