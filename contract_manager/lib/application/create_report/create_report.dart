import 'dart:typed_data';

import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/service_item/service_item_failure.dart';
import 'package:equatable/equatable.dart';

class CreateReport extends UseCase<void, CreateReportParams> {
  CreateReport(
    this.repository,
    this.batchRepository,
    this.storageRepository,
    this.authRepository,
  );

  final IServiceItemRepository repository;
  final IBatchRepository batchRepository;
  final IStorageRepository storageRepository;
  final IAuthRepository authRepository;

  @override
  Future<void> call(CreateReportParams params) async {
    final batch = batchRepository.getBatch();

    final String? employeeId = authRepository.getLoggedInUserId();
    if (employeeId == null) {
      throw const ServiceItemFailure.unexpected();
    }

    final List<String> imagePaths = [];

    for (final bytes in params.imageBytes) {
      final path = '${params.serviceItem.projectId}/${params.serviceItem.contractId}-${imagePaths.length}';
      await storageRepository.storeImage(bytes, path);
      imagePaths.add(path);
    }

    await repository.setReport(
      serviceItem: params.serviceItem,
      employeeId: employeeId,
      message: params.message,
      isError: params.isError,
      imagePaths: BuiltList<String>(imagePaths),
    );

    await repository.updateLastSeenVersion(
      serviceItem: params.serviceItem,
      employeeId: employeeId,
      batch: batch,
    );

    await batch.commit();
  }
}

class CreateReportParams extends Equatable {
  const CreateReportParams({
    required this.imageBytes,
    required this.serviceItem,
    required this.message,
    required this.isError,
  });

  final String message;
  final bool isError;
  final ServiceItem serviceItem;
  final List<Uint8List> imageBytes;

  @override
  List<Object?> get props => [
        message,
        isError,
        serviceItem,
        imageBytes,
      ];
}
