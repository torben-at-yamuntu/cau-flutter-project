import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/company/i_company_repository.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/company/company_model.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const company0 = CompanyModel(
  id: 'asdf',
  address: AddressModel(
    city: 'Kiel',
    country: 'Germany',
    houseNumber: 32,
    street: 'Westring',
    zip: 24542,
  ),
  bankInfo: BankInfoModel(
    owner: 'Company0',
    iban: 'de32 3456 5434 5435 24',
    bic: 'gfdsdfgfd',
  ),
  name: 'Company0',
);
const company1 = CompanyModel(
  id: 'ewre',
  address: AddressModel(
    city: 'Hamburg',
    country: 'Germany',
    houseNumber: 33,
    street: 'Mönckebergstraße',
    zip: 23653,
  ),
  bankInfo: BankInfoModel(
    owner: 'Company1',
    iban: 'de87 5457 8765 4545 65',
    bic: 'gfdsdfgfd',
  ),
  name: 'Company1',
);

final companyFutureProvider = FutureProvider.family<Company, String>((ref, companyId) {
  final ICompanyRepository companyRepository = ref.watch(companyRepositoryProvider);
  return companyRepository.getCompany(companyId: companyId);
});
