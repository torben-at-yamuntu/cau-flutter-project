import 'package:contract_manager/application/project/create_project.dart';
import 'package:contract_manager/domain/auth/i_auth_repository.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/project/i_project_repository.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createProjectProvider = Provider<UseCase<void, CreateProjectParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IProjectRepository projectRepository = ref.watch(projectRepositoryProvider);
  final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
  return CreateProject(
    projectRepository,
    batchRepository,
    authRepository,
  );
});
