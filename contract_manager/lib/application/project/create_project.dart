import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class CreateProject extends UseCase<void, CreateProjectParams> {
  CreateProject(this.repository, this.batchRepository, this.authRepository);

  final IProjectRepository repository;
  final IBatchRepository batchRepository;
  final IAuthRepository authRepository;

  @override
  Future<void> call(CreateProjectParams params) async {
    final batch = batchRepository.getBatch();

    final String? employeeId = authRepository.getLoggedInUserId();
    if (employeeId == null) {
      throw const ProjectFailure.unexpected();
    }

    await repository.create(
      batch: batch,
      companyId: params.companyId,
      name: params.name,
    );

    await batch.commit();
  }
}

class CreateProjectParams extends Equatable {
  const CreateProjectParams({
    required this.name,
    required this.companyId,
  });

  final String name;
  final String companyId;

  @override
  List<Object?> get props => [
        name,
        companyId,
      ];
}
