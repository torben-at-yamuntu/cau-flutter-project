import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final companyStreamProvider = StreamProvider.family<Company, String>((ref, companyId) {
  final ICompanyRepository repository = ref.watch(companyRepositoryProvider);
  return repository.watchCompany(companyId: companyId);
});

final employeesStreamProvider = StreamProvider.family<BuiltList<Employee>, String>((ref, companyId) {
  final IEmployeeRepository repository = ref.watch(employeeRepositoryProvider);
  return repository.watchEmployeesOfCompany(companyId: companyId);
});

final employeeStreamProvider = StreamProvider.family<Employee, String>((ref, employeeId) {
  final IEmployeeRepository employeeRepository = ref.watch(employeeRepositoryProvider);
  return employeeRepository.watchEmployee(employeeId: employeeId);
});

/// Returns a stream of CompanyViewModel's for a specific Company with `companyId`.
final companyViewModelStreamProvider = StreamProvider.family<CompanyViewModel, String>(
  (ref, companyId) async* {
    final Company company = await ref.watch(companyStreamProvider(companyId).last);
    final BuiltList<Employee> employees = await ref.watch(employeesStreamProvider(companyId).last);
    yield CompanyViewModel(company: company, employees: employees);
  },
);

/// wraps a company and the list of all its employees to one object
class CompanyViewModel extends Equatable {
  const CompanyViewModel({
    required this.company,
    required this.employees,
  });

  final Company company;
  final BuiltList<Employee> employees;

  @override
  List<Object?> get props => [company, employees];
}
