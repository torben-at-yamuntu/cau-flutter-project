import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class RegisterEmployeeUseCase extends UseCase<void, RegisterParams> {
  RegisterEmployeeUseCase(this.authRepo, this.batchRepo, this.employeeRepo);

  final IAuthRepository authRepo;
  final IBatchRepository batchRepo;
  final IEmployeeRepository employeeRepo;

  @override
  Future<void> call(RegisterParams params) async {
    final batch = batchRepo.getBatch();

    // Register new User at Firebase
    final String userID = await authRepo.register(
      params.email,
      params.password,
    );
    // Safe Employee in the Employee Repo.
    await employeeRepo.create(
      companyId: params.companyID,
      employeeId: userID,
      name: params.name,
      role: params.role,
      batch: batch,
    );

    await batch.commit();
  }
}

class RegisterParams extends Equatable {
  const RegisterParams(
      {required this.email, required this.password, required this.name, required this.role, required this.companyID});
  final String email;
  final String password;
  final String name;
  final EmployeeRole role;
  final String companyID;

  @override
  List<Object?> get props => [email, password, name, role, companyID];
}
