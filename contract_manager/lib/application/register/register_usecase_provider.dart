import 'package:contract_manager/application/register/register_company_uc.dart';
import 'package:contract_manager/application/register/register_employee_usecase.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/core/use_case.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final registerEmployeeUseCaseProvider = Provider<UseCase<void, RegisterParams>>((ref) {
  final IBatchRepository? batchRepository = ref.watch(batchRepositoryProvider);
  final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
  final IEmployeeRepository? employeeRepository = ref.watch(employeeRepositoryProvider);
  return RegisterEmployeeUseCase(authRepository, batchRepository!, employeeRepository!);
});

final registerCompanyUseCaseProvider = Provider<UseCase<void, RegisterCompanyParams>>((ref) {
  final IBatchRepository batchRepository = ref.watch(batchRepositoryProvider);
  final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
  final ICompanyRepository companyRepository = ref.watch(companyRepositoryProvider);
  final IEmployeeRepository employeeRepository = ref.watch(employeeRepositoryProvider);
  return RegisterCompanyUseCase(authRepository, batchRepository, companyRepository, employeeRepository);
});
