import 'package:contract_manager/domain/domain.dart';
import 'package:equatable/equatable.dart';

class RegisterCompanyUseCase extends UseCase<void, RegisterCompanyParams> {
  RegisterCompanyUseCase(
    this.authRepo,
    this.batchRepo,
    this.companyRepo,
    this.employeeRepo,
  );

  final IAuthRepository authRepo;
  final IBatchRepository batchRepo;
  final ICompanyRepository companyRepo;
  final IEmployeeRepository employeeRepo;

  @override
  Future<void> call(RegisterCompanyParams params) async {
    final batch = batchRepo.getBatch();

    final Company newCompany = await companyRepo.create(
      name: params.name,
      country: params.country,
      city: params.city,
      zip: params.zip,
      street: params.street,
      houseNumber: params.houseNumber,
      iban: params.iban,
      bic: params.bic,
      accountOwner: params.accountOwner,
    );

    final String newUserId = await authRepo.register(params.adminEmail, params.adminPassword);

    await employeeRepo.create(
      employeeId: newUserId,
      name: params.name,
      companyId: newCompany.id,
      role: EmployeeRole.admin,
    );

    await batch.commit();
  }
}

class RegisterCompanyParams extends Equatable {
  const RegisterCompanyParams({
    required this.name,
    required this.country,
    required this.city,
    required this.zip,
    required this.street,
    required this.houseNumber,
    required this.iban,
    required this.bic,
    required this.accountOwner,
    required this.adminEmail,
    required this.adminPassword,
  });

  final String name;
  final String country;
  final String city;
  final int zip;
  final String street;
  final int houseNumber;
  final String iban;
  final String bic;
  final String accountOwner;
  final String adminEmail;
  final String adminPassword;

  @override
  List<Object?> get props => [
        name,
        country,
        city,
        zip,
        street,
        houseNumber,
        iban,
        bic,
        accountOwner,
        adminEmail,
        adminPassword,
      ];
}
