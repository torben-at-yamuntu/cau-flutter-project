import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/application/application.dart';
import 'package:contract_manager/domain/auth/i_auth_repository.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/contract/contract_failure.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/infrastructure/auth/auth_provider.dart';
import 'package:contract_manager/infrastructure/repositories.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final contractStreamProvider = StreamProvider.family<Contract, ContractParams>((ref, params) {
  final IContractRepository contractRepository = ref.watch(contractRepositoryProvider);
  return contractRepository.watchContract(
    contractId: params.contractId,
    projectId: params.projectId,
  );
});

class ContractParams extends Equatable {
  const ContractParams({
    required this.projectId,
    required this.contractId,
  });

  final String projectId;
  final String contractId;

  @override
  List<Object?> get props => [contractId, projectId];
}

final participationContractsStream = StreamProvider.family<BuiltList<Contract>, String>((ref, companyId) {
  final IContractRepository contractRepository = ref.watch(contractRepositoryProvider);

  return contractRepository.watchAllContractsWithCompanyParticipation(companyId: companyId);
});

final contractsStreamProvider = StreamProvider<BuiltList<ContractsSectionViewModel>>(
  (ref) async* {
    final IAuthRepository authRepository = ref.watch(authRepositoryProvider);
    final IContractRepository contractRepository = ref.watch(contractRepositoryProvider);

    final String? employeeId = authRepository.getLoggedInUserId();
    if (employeeId == null) {
      throw const ContractFailure.unexpected();
    }

    final Employee employee = await ref.watch(employeeStreamProvider(employeeId).last);
    final contracts = await ref.watch(participationContractsStream(employee.companyId).last);

    final Map<String, List<Contract>> filteredContracts = <String, List<Contract>>{};
    for (final contract in contracts) {
      if (filteredContracts.containsKey(contract.projectId)) {
        filteredContracts[contract.projectId]!.add(contract);
      } else {
        filteredContracts[contract.projectId] = [contract];
      }
    }

    final Map<String, Project> filteredProjects = <String, Project>{};
    final Map<String, Company> filteredCompanies = <String, Company>{};

    for (final entry in filteredContracts.entries) {
      filteredProjects[entry.key] = await ref.watch(projectFutureProvider(entry.key).future);
      //   filteredCompanies[entry.key] =
      //      await ref.watch(companyFutureProvider(filteredProjects[entry.key]!.companyId).future);
    }

    final List<ContractsSectionViewModel> viewModels = [];
    for (final entry in filteredContracts.entries) {
      viewModels.add(
        ContractsSectionViewModel(
          project: filteredProjects[entry.key]!,
          contracts: BuiltList(filteredContracts[entry.key]!),
          client: '', //filteredCompanies[entry.key]!,
        ),
      );
    }
    yield BuiltList(viewModels);
  },
);

class ContractsSectionViewModel extends Equatable {
  const ContractsSectionViewModel({
    required this.project,
    required this.contracts,
    required this.client,
  });

  final Project project;
  final String client;
  final BuiltList<Contract> contracts;

  @override
  List<Object?> get props => [project, contracts, client];
}
