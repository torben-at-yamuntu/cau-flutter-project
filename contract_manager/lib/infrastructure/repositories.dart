import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/domain/company/i_company_repository.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/counter/i_counter_repository.dart';
import 'package:contract_manager/domain/employee/i_employee_repository.dart';
import 'package:contract_manager/domain/project/i_project_repository.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/domain/storage/i_storage_repository.dart';
import 'package:contract_manager/domain/transaction/i_transaction_repository.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch_repository.dart';
import 'package:contract_manager/infrastructure/company/company_repository.dart';
import 'package:contract_manager/infrastructure/contract/contract_repository.dart';
import 'package:contract_manager/infrastructure/counter/counter_repository.dart';
import 'package:contract_manager/infrastructure/employee/employee_repository.dart';
import 'package:contract_manager/infrastructure/infrastructure.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_repository.dart';
import 'package:contract_manager/infrastructure/storage/storage_app_repository.dart'
    if (dart.library.html) 'package:contract_manager/infrastructure/storage/storage_web_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final Provider<ICounterRepository> counterRepositoryProvider = Provider((_) => CounterRepository());
final Provider<ICompanyRepository> companyRepositoryProvider = Provider((_) => CompanyRepository());
final Provider<IContractRepository> contractRepositoryProvider = Provider((_) => ContractRepository());
final Provider<IEmployeeRepository> employeeRepositoryProvider = Provider((_) => EmployeeRepository());
final Provider<IProjectRepository> projectRepositoryProvider = Provider((_) => ProjectRepository());
final Provider<IServiceItemRepository> serviceItemRepositoryProvider = Provider((_) => ServiceItemRepository());
final Provider<ITransactionRepository> transactionRepositoryProvider =
    Provider((_) => TransactionRepository<dynamic>());

final Provider<IBatchRepository> batchRepositoryProvider = Provider((_) => FirestoreBatchRepository());
final Provider<IStorageRepository> storageRepositoryProvider = Provider((_) => StorageRepository());
