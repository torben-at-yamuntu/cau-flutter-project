import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/infrastructure/auth/auth_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final authRepositoryProvider = Provider<IAuthRepository>((_) => AuthRepository());
