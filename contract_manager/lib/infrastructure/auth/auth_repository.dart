import 'dart:async';

import 'package:contract_manager/domain/auth/auth_failure.dart';
import 'package:contract_manager/domain/auth/i_auth_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository implements IAuthRepository {
  AuthRepository([FirebaseAuth? firebaseAuth])
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  final FirebaseAuth _firebaseAuth;

  @override
  Future<void> logIn(String email, String password) async {
    print('gfdsdf');
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
      throw const AuthFailure.unexpected();
    }
  }

  @override
  Future<void> logout() async {
    try {
      await _firebaseAuth.signOut();
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
      throw const AuthFailure.unexpected();
    }
  }

  @override
  String? getLoggedInUserId() => _firebaseAuth.currentUser?.uid;

  @override
  Future<String> register(String email, String password) async {
    try {
      final UserCredential userCredential =
          await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);

      final String? uid = userCredential.user?.uid;

      if (uid == null) throw const AuthFailure.unexpected();
      return uid;
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
      throw const AuthFailure.unexpected();
    }
  }
}
