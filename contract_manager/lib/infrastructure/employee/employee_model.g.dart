// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployeeModel _$EmployeeModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['id', 'companyId', 'name', 'role'],
      disallowNullValues: const ['id', 'companyId', 'name', 'role']);
  return EmployeeModel(
    id: json['id'] as String,
    companyId: json['companyId'] as String,
    name: json['name'] as String,
    role: _$enumDecode(_$EmployeeRoleEnumMap, json['role']),
  );
}

Map<String, dynamic> _$EmployeeModelToJson(EmployeeModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'companyId': instance.companyId,
      'name': instance.name,
      'role': _$EmployeeRoleEnumMap[instance.role],
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$EmployeeRoleEnumMap = {
  EmployeeRole.read: 'read',
  EmployeeRole.readWrite: 'readWrite',
  EmployeeRole.admin: 'admin',
};
