import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/employee/employee.dart';
import 'package:contract_manager/domain/employee/employee_failure.dart';
import 'package:contract_manager/domain/employee/i_employee_repository.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/employee/employee_model.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../firestore_repository.dart';

class EmployeeRepository extends FirestoreRepository implements IEmployeeRepository {
  EmployeeRepository([FirebaseFirestore? firestore]) : super(firestore ?? FirebaseFirestore.instance);

  String get employeeCollectionName => 'employees';

  final StreamTransformer<Map<String, dynamic>?, Employee> employeeTransformer = StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      try {
        sink.add(EmployeeModel.fromJson(data!));
      } on BadKeyException catch (e, stackTrace) {
        print(e.message);
        print(stackTrace);
        sink.addError(const EmployeeFailure.unexpected());
      }
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const EmployeeFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  final StreamTransformer<List<Map<String, dynamic>?>, BuiltList<Employee>> employeeListTransformer =
      StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      final Iterable<Employee> employees = data
          .map<Employee?>((docData) {
            try {
              if (docData == null) {
                return null;
              }
              return EmployeeModel.fromJson(docData);
            } on BadKeyException catch (e, stackTrace) {
              print(e.message);
              print(stackTrace);
              return null;
            }
          })
          .where((employee) => employee != null)
          .cast<Employee>();
      sink.add(BuiltList<Employee>.from(employees));
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const EmployeeFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  @override
  Future<void> changeRole({
    required EmployeeRole role,
    required Employee employee,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference employeeRef = super.firestore.collection('$employeeCollectionName/${employee.id}').doc();
      final Employee updatedEmployee = employee.copyWith(role: role);

      await super.update(
        ref: employeeRef,
        data: employee.jsonFlatDiff(updatedEmployee),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const EmployeeFailure.unexpected();
    }
  }

  @override
  Future<Employee> create({
    required String employeeId,
    required String name,
    required String companyId,
    required EmployeeRole role,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference employeeRef = super.firestore.doc('$employeeCollectionName/$employeeId');
      final Employee employee = EmployeeModel(
        id: employeeRef.id,
        companyId: companyId,
        name: name,
        role: role,
      );

      await super.set(
        ref: employeeRef,
        data: employee.toJson(),
        batch: batch,
        transaction: transaction,
      );

      return employee;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const EmployeeFailure.unexpected();
    }
  }

  @override
  Stream<Employee> watchEmployee({
    required String employeeId,
  }) =>
      super
          .firestore
          .doc('$employeeCollectionName/$employeeId')
          .snapshots()
          .map((documentSnapshot) => documentSnapshot.data())
          .transform(employeeTransformer);

  @override
  Stream<BuiltList<Employee>> watchEmployeesOfCompany({required String companyId}) => super
      .firestore
      .collection(employeeCollectionName)
      .where('companyId', isEqualTo: companyId)
      .snapshots()
      .map<List<Map<String, dynamic>?>>(
        (querySnapshot) => querySnapshot.docs.map((documentSnapshot) => documentSnapshot.data()).toList(),
      )
      .transform(employeeListTransformer);

  /// Deletes the [Employee] where [Employee.id] equals [employee.id].
  @override
  Future<void> deleteEmployee({
    required Employee employee,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference employeeRef = super.firestore.collection('$employeeCollectionName/${employee.id}').doc();

      await super.delete(
        ref: employeeRef,
        transaction: transaction,
        batch: batch,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const EmployeeFailure.unexpected();
    }
  }
}
