import 'package:contract_manager/domain/employee/employee.dart';
import 'package:json_annotation/json_annotation.dart';

part 'employee_model.g.dart';

@JsonSerializable()
class EmployeeModel extends Employee {
  const EmployeeModel({
    required String id,
    required String companyId,
    required String name,
    required EmployeeRole role,
  }) : super(
          id,
          companyId,
          name,
          role,
        );

  factory EmployeeModel.fromJson(Map<String, dynamic> json) => _$EmployeeModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$EmployeeModelToJson(this);

  @override
  EmployeeModel copyWith({
    String? name,
    EmployeeRole? role,
  }) =>
      EmployeeModel(
        id: id,
        companyId: companyId,
        name: name ?? this.name,
        role: role ?? this.role,
      );
}
