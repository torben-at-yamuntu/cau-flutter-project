import 'dart:typed_data';

import 'package:contract_manager/domain/storage/i_storage_repository.dart';
import 'package:contract_manager/domain/storage/storage_failure.dart';
import 'package:contract_manager/infrastructure/firebase_storage_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image/image.dart' as img;

class StorageRepository extends FirebaseStorageRepository implements IStorageRepository {
  StorageRepository([FirebaseStorage? firebasestorage]) : super(firebasestorage ?? FirebaseStorage.instance);

  final String _imageCollectionPath = 'images';

  @override
  Future<void> storeImage(Uint8List bytes, String path) async {
    try {
      final storageReference = firestorage.ref('$_imageCollectionPath/$path');

      final uploadTask = storageReference.putData(bytes, SettableMetadata(contentType: 'image/jpeg'));

      await uploadTask;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const StorageFailure.unexpected();
    }
  }

  @override
  Future<img.Image> getImage(String path) async {
    try {
      final Reference storageReference = firestorage.ref(_imageCollectionPath).child(path);

      final Uint8List? bytes = await storageReference.getData();

      final img.Image? image = (bytes == null) ? null : img.decodeImage(bytes);

      return (image != null) ? image : throw const StorageFailure.unexpected();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const StorageFailure.unexpected();
    }
  }
}
