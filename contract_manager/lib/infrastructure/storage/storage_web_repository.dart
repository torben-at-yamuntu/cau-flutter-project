import 'dart:html' as html;
import 'dart:typed_data';

import 'package:contract_manager/domain/storage/i_storage_repository.dart';
import 'package:contract_manager/domain/storage/storage_failure.dart';
import 'package:contract_manager/infrastructure/firebase_storage_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as img;

class StorageRepository extends FirebaseStorageRepository implements IStorageRepository {
  StorageRepository([FirebaseStorage? firebasestorage]) : super(firebasestorage ?? FirebaseStorage.instance);

  final String _imageCollectionPath = 'images';

  @override
  Future<void> storeImage(Uint8List bytes, String path) async {
    try {
      final blob = html.Blob(bytes);

      final storageReference = firestorage.ref('$_imageCollectionPath/$path');

      final uploadTask = storageReference.putBlob(blob, SettableMetadata(contentType: 'image/jpeg'));

      await uploadTask;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const StorageFailure.unexpected();
    }
  }

  @override
  Future<img.Image> getImage(String path) async {
    try {
      final Reference storageReference = firestorage.ref(_imageCollectionPath).child(path);

      final String downloadUrl = await storageReference.getDownloadURL();

      final url = Uri.parse(downloadUrl);
      final response = await http.get(url);
      if (response.statusCode != html.HttpStatus.ok) {
        throw const StorageFailure.unexpected();
      }
      final img.Image? image = img.decodeImage(response.bodyBytes.toList());
      if (image != null) {
        return image;
      }
      throw const StorageFailure.unexpected();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const StorageFailure.unexpected();
    }
  }
}
