import 'package:firebase_storage/firebase_storage.dart';

/// Provides [FirebaseFirestorage] for repositories.
abstract class FirebaseStorageRepository {
  FirebaseStorageRepository(this.firestorage);

  final FirebaseStorage firestorage;
}
