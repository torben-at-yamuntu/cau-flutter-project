// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceItemModel _$ServiceItemModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'versionNumber',
    'lastSeenVersions',
    'id',
    'projectId',
    'contractId',
    'specification',
    'startsAt',
    'endPlannedAt',
    'workDoneAt',
    'approvedAt',
    'inventories',
    'report'
  ], disallowNullValues: const [
    'versionNumber',
    'lastSeenVersions',
    'id',
    'projectId',
    'contractId',
    'specification',
    'startsAt',
    'endPlannedAt',
    'inventories'
  ]);
  return ServiceItemModel(
    id: json['id'] as String,
    projectId: json['projectId'] as String,
    contractId: json['contractId'] as String,
    specification: json['specification'] as String,
    startsAt:
        const TimestampConverter().fromJson(json['startsAt'] as Timestamp),
    endPlannedAt:
        const TimestampConverter().fromJson(json['endPlannedAt'] as Timestamp),
    inventories: const ServiceItemConverter()
        .fromJson(json['inventories'] as Map<String, dynamic>),
    versionNumber: json['versionNumber'] as int,
    lastSeenVersions: const LastSeenVersionsConverter()
        .fromJson(json['lastSeenVersions'] as Map<String, dynamic>),
    report: const ReportConverter()
        .fromJson(json['report'] as Map<String, dynamic>),
    outsourcedContractId: json['outsourcedContractId'] as String?,
    subContractId: json['subContractId'] as String?,
    workDoneAt: const NullableTimestampConverter()
        .fromJson(json['workDoneAt'] as Timestamp?),
    approvedAt: const NullableTimestampConverter()
        .fromJson(json['approvedAt'] as Timestamp?),
  );
}

Map<String, dynamic> _$ServiceItemModelToJson(ServiceItemModel instance) {
  final val = <String, dynamic>{
    'versionNumber': instance.versionNumber,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('lastSeenVersions',
      const LastSeenVersionsConverter().toJson(instance.lastSeenVersions));
  val['id'] = instance.id;
  val['projectId'] = instance.projectId;
  val['contractId'] = instance.contractId;
  val['specification'] = instance.specification;
  writeNotNull(
      'startsAt', const TimestampConverter().toJson(instance.startsAt));
  writeNotNull(
      'endPlannedAt', const TimestampConverter().toJson(instance.endPlannedAt));
  val['workDoneAt'] =
      const NullableTimestampConverter().toJson(instance.workDoneAt);
  val['approvedAt'] =
      const NullableTimestampConverter().toJson(instance.approvedAt);
  val['outsourcedContractId'] = instance.outsourcedContractId;
  val['subContractId'] = instance.subContractId;
  writeNotNull(
      'inventories', const ServiceItemConverter().toJson(instance.inventories));
  val['report'] = const ReportConverter().toJson(instance.report);
  return val;
}
