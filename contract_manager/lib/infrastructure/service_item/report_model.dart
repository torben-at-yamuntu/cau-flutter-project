import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/infrastructure/converters/string_list_converter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'report_model.g.dart';

@StringListConverter()
@JsonSerializable()
class ReportModel extends Report {
  const ReportModel({
    required String authorId,
    required String message,
    required BuiltList<String> imagePaths,
    required bool isError,
  }) : super(authorId, message, imagePaths, isError);

  factory ReportModel.fromJson(Map<String, dynamic> json) => _$ReportModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$ReportModelToJson(this);

  @override
  ReportModel copyWith({
    String? authorId,
    String? message,
    BuiltList<String>? imagePaths,
    bool? isError,
  }) =>
      ReportModel(
        authorId: authorId ?? this.authorId,
        message: message ?? this.message,
        imagePaths: imagePaths ?? this.imagePaths,
        isError: isError ?? this.isError,
      );
}
