// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportModel _$ReportModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'authorId',
    'message',
    'imagePaths',
    'isError'
  ], disallowNullValues: const [
    'authorId',
    'message',
    'imagePaths',
    'isError'
  ]);
  return ReportModel(
    authorId: json['authorId'] as String,
    message: json['message'] as String,
    imagePaths:
        const StringListConverter().fromJson(json['imagePaths'] as List),
    isError: json['isError'] as bool,
  );
}

Map<String, dynamic> _$ReportModelToJson(ReportModel instance) {
  final val = <String, dynamic>{
    'authorId': instance.authorId,
    'message': instance.message,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull(
      'imagePaths', const StringListConverter().toJson(instance.imagePaths));
  val['isError'] = instance.isError;
  return val;
}
