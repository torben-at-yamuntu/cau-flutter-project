// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentModel _$PaymentModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'id',
    'amount',
    'serviceItemIds',
    'sentPaymentAt',
    'receivedPaymentAt',
    'paymentActiveAt'
  ], disallowNullValues: const [
    'id',
    'amount',
    'serviceItemIds'
  ]);
  return PaymentModel(
    id: json['id'] as String,
    amount: (json['amount'] as num).toDouble(),
    serviceItemIds:
        const StringListConverter().fromJson(json['serviceItemIds'] as List),
    sentPaymentAt: const NullableTimestampConverter()
        .fromJson(json['sentPaymentAt'] as Timestamp?),
    receivedPaymentAt: const NullableTimestampConverter()
        .fromJson(json['receivedPaymentAt'] as Timestamp?),
    paymentActiveAt: const NullableTimestampConverter()
        .fromJson(json['paymentActiveAt'] as Timestamp?),
  );
}

Map<String, dynamic> _$PaymentModelToJson(PaymentModel instance) {
  final val = <String, dynamic>{
    'id': instance.id,
    'amount': instance.amount,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('serviceItemIds',
      const StringListConverter().toJson(instance.serviceItemIds));
  val['sentPaymentAt'] =
      const NullableTimestampConverter().toJson(instance.sentPaymentAt);
  val['receivedPaymentAt'] =
      const NullableTimestampConverter().toJson(instance.receivedPaymentAt);
  val['paymentActiveAt'] =
      const NullableTimestampConverter().toJson(instance.paymentActiveAt);
  return val;
}
