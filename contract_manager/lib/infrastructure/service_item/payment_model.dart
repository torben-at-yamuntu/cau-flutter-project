import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/converters/nullable_timestamp_converter.dart';
import 'package:contract_manager/infrastructure/converters/string_list_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'payment_model.g.dart';

@JsonSerializable()
@NullableTimestampConverter()
@StringListConverter()
class PaymentModel extends Payment {
  const PaymentModel({
    required String id,
    required double amount,
    required BuiltList<String> serviceItemIds,
    DateTime? sentPaymentAt,
    DateTime? receivedPaymentAt,
    DateTime? paymentActiveAt,
  }) : super(
          id,
          amount,
          serviceItemIds,
          sentPaymentAt,
          receivedPaymentAt,
          paymentActiveAt,
        );

  factory PaymentModel.fromJson(Map<String, dynamic> json) => _$PaymentModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$PaymentModelToJson(this);

  @override
  PaymentModel copyWith({
    String? id,
    double? amount,
    PaymentStatus? status,
    BuiltList<String>? serviceItemIds,
    DateTime? sentPaymentAt,
    DateTime? receivedPaymentAt,
    DateTime? paymentActiveAt,
  }) =>
      PaymentModel(
        id: id ?? this.id,
        amount: amount ?? this.amount,
        serviceItemIds: serviceItemIds ?? this.serviceItemIds,
        sentPaymentAt: sentPaymentAt ?? this.sentPaymentAt,
        receivedPaymentAt: receivedPaymentAt ?? this.receivedPaymentAt,
        paymentActiveAt: paymentActiveAt ?? this.paymentActiveAt,
      );
}
