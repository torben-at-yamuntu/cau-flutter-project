import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/service_item/i_service_item_repository.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_failure.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/firestore_repository.dart';
import 'package:contract_manager/infrastructure/infrastructure.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_model.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class ServiceItemRepository extends FirestoreRepository implements IServiceItemRepository {
  ServiceItemRepository([FirebaseFirestore? firestore]) : super(firestore ?? FirebaseFirestore.instance);

  static const String _serviceItemCollectionName = 'serviceItems';

  final StreamTransformer<List<Map<String, dynamic>?>, BuiltList<ServiceItem>> serviceItemTransformer =
      StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      final Iterable<ServiceItem> serviceItems = data
          .map<ServiceItem?>((docData) {
            try {
              if (docData == null) {
                return null;
              }
              return ServiceItemModel.fromJson(docData);
            } on BadKeyException catch (e, stackTrace) {
              print(e.message);
              print(stackTrace);
              return null;
            }
          })
          .where((serviceItems) => serviceItems != null)
          .cast<ServiceItem>();
      sink.add(BuiltList<ServiceItem>.from(serviceItems));
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const ServiceItemFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  @override
  Future<void> addServiceItemInventory({
    required ServiceItem serviceItem,
    required ServiceItemInventory serviceItemInventory,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();

      await super.update(
        ref: serviceItemRef,
        data: <String, dynamic>{
          'inventories': FieldValue. //
              arrayUnion(<ServiceItemInventory>[serviceItemInventory])
        },
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<ServiceItem> create({
    required String projectId,
    required String contractId,
    required String specification,
    required DateTime startAt,
    required DateTime endPlannedAt,
    required List<ServiceItemInventoryParams> inventories,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef =
          super.firestore.collection('projects/$projectId/contracts/$contractId/$_serviceItemCollectionName').doc();

      final Map<String, ServiceItemInventory> inventoryModels = {};
      for (int i = 0; i < inventories.length; i++) {
        final serviceItemInventory = ServiceItemInventoryModel(
          id: i.toString(),
          material: inventories[i].material,
          quantity: inventories[i].quantity,
          quantityUnit: inventories[i].unit,
          price: inventories[i].price,
        );
        inventoryModels[serviceItemInventory.id] = serviceItemInventory;
      }

      final ServiceItem serviceItem = ServiceItemModel(
        id: serviceItemRef.id,
        projectId: projectId,
        contractId: contractId,
        specification: specification,
        startsAt: startAt,
        endPlannedAt: endPlannedAt,
        inventories: BuiltMap<String, ServiceItemInventory>(inventoryModels),
        versionNumber: 0,
        lastSeenVersions: BuiltMap(<String, dynamic>{}),
      );

      await super.set(
        ref: serviceItemRef,
        data: serviceItem.toJson(),
        batch: batch,
        transaction: transaction,
      );

      return serviceItem;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> deleteServiceItemInventory({
    required ServiceItem serviceItem,
    required ServiceItemInventory serviceItemInventory,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();
      await super.update(
        ref: serviceItemRef,
        data: <String, dynamic>{
          'inventories': {
            FieldValue.arrayRemove(<ServiceItemInventory>[serviceItemInventory])
          }
        },
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> incrementVersion({
    required ServiceItem serviceItem,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();

      await super.update(
        ref: serviceItemRef,
        data: <String, dynamic>{'versionNumber': FieldValue.increment(1)},
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> updateApprovedAtToNow({
    required ServiceItem serviceItem,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();
      final ServiceItem updatedServiceItem = serviceItem.copyWith(approvedAt: DateTime.now());

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> updateLastSeenVersion({
    required ServiceItem serviceItem,
    required String employeeId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super.firestore.doc(
          'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}');

      final ServiceItem updatedServiceItem = serviceItem.copyWith(
        lastSeenVersions: serviceItem.lastSeenVersions.rebuild(
          (builder) => builder.updateValue(
            employeeId,
            (_) => serviceItem.versionNumber,
            ifAbsent: () => serviceItem.versionNumber,
          ),
        ),
      );

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> updateServiceItemQuantity({
    required ServiceItem serviceItem,
    required String serviceItemInventoryId,
    required double quantity,
    QuantityUnit? unit,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();

      final ServiceItemInventory? serviceItemInventory = serviceItem.inventories.asMap().remove(serviceItemInventoryId);
      if (serviceItemInventory != null) {
        final ServiceItemInventory updatedServiceItemInventory = serviceItemInventory.copyWith(quantity: quantity);

        final ServiceItem updatedServiceItem = serviceItem.copyWith(
          inventories: serviceItem.inventories.rebuild(
            (builder) => builder.updateValue(serviceItemInventoryId, (_) => updatedServiceItemInventory),
          ),
        );

        await super.update(
          ref: serviceItemRef,
          data: serviceItem.jsonFlatDiff(updatedServiceItem),
          batch: batch,
          transaction: transaction,
        );
      }
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> updateWorkDoneAtToNow({
    required ServiceItem serviceItem,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super
          .firestore
          .collection(
              'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}')
          .doc();
      final ServiceItem updatedServiceItem = serviceItem.copyWith(workDoneAt: DateTime.now());

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Stream<BuiltList<ServiceItem>> watchServiceItems({required Contract contract}) => super
      .firestore
      .collection('projects/${contract.projectId}/contracts/${contract.id}/$_serviceItemCollectionName')
      .snapshots()
      .map<List<Map<String, dynamic>?>>(
          (querySnapshot) => querySnapshot.docs.map((documentSnapshot) => documentSnapshot.data()).toList())
      .transform(serviceItemTransformer);

  @override
  Future<void> setReport({
    required ServiceItem serviceItem,
    required String employeeId,
    required String message,
    required bool isError,
    required BuiltList<String> imagePaths,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef =
          super.firestore.collection('$_serviceItemCollectionName/${serviceItem.id}').doc();

      final ReportModel report = ReportModel(
        authorId: employeeId,
        message: message,
        imagePaths: imagePaths,
        isError: isError,
      );

      final ServiceItem updatedServiceItem = serviceItem.copyWith(report: report);

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> setOldContractId({
    required ServiceItem serviceItem,
    required String outsourcedContractId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = firestore.doc(
          'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}');
      final ServiceItem updatedServiceItem = serviceItem.copyWith(outsourcedContractId: outsourcedContractId);

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<void> setSubContractId({
    required ServiceItem serviceItem,
    required Option<String> subContractIdOption,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef = super.firestore.doc(
          'projects/${serviceItem.projectId}/contracts/${serviceItem.contractId}/$_serviceItemCollectionName/${serviceItem.id}');
      final ServiceItem updatedServiceItem = serviceItem.copyWith(subContractId: subContractIdOption);

      await super.update(
        ref: serviceItemRef,
        data: serviceItem.jsonFlatDiff(updatedServiceItem),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }

  @override
  Future<ServiceItem> getServiceItemById({
    required String projectId,
    required String contractId,
    required String serviceItemId,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference serviceItemRef =
          firestore.doc('projects/$projectId/contracts/$contractId/serviceItems/$serviceItemId');

      await serviceItemRef.snapshots().map((documentSnapshot) => documentSnapshot.data()).first;
      final DocumentSnapshot serviceItemSnapshot = await super.getDocument(
        ref: serviceItemRef,
        transaction: transaction,
      );
      final Map<String, dynamic>? json = serviceItemSnapshot.data();

      return (json != null) ? ServiceItemModel.fromJson(json) : throw const ServiceItemFailure.unexpected();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ServiceItemFailure.unexpected();
    }
  }
}
