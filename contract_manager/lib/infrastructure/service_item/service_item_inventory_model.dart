import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:json_annotation/json_annotation.dart';

part 'service_item_inventory_model.g.dart';

@JsonSerializable()
class ServiceItemInventoryModel extends ServiceItemInventory {
  const ServiceItemInventoryModel({
    required String id,
    required String material,
    required double quantity,
    required QuantityUnit quantityUnit,
    required double price,
  }) : super(id, material, quantity, quantityUnit, price);

  factory ServiceItemInventoryModel.fromJson(Map<String, dynamic> json) => _$ServiceItemInventoryModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$ServiceItemInventoryModelToJson(this);

  @override
  ServiceItemInventoryModel copyWith({
    String? id,
    String? material,
    double? quantity,
    QuantityUnit? quantityUnit,
    double? price,
  }) =>
      ServiceItemInventoryModel(
        id: id ?? this.id,
        material: material ?? this.material,
        quantity: quantity ?? this.quantity,
        quantityUnit: quantityUnit ?? this.quantityUnit,
        price: price ?? this.price,
      );
}
