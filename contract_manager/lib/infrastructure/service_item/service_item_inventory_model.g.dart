// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_item_inventory_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceItemInventoryModel _$ServiceItemInventoryModelFromJson(
    Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'id',
    'material',
    'quantity',
    'quantityUnit',
    'price'
  ], disallowNullValues: const [
    'id',
    'material',
    'quantity',
    'quantityUnit',
    'price'
  ]);
  return ServiceItemInventoryModel(
    id: json['id'] as String,
    material: json['material'] as String,
    quantity: (json['quantity'] as num).toDouble(),
    quantityUnit: _$enumDecode(_$QuantityUnitEnumMap, json['quantityUnit']),
    price: (json['price'] as num).toDouble(),
  );
}

Map<String, dynamic> _$ServiceItemInventoryModelToJson(
        ServiceItemInventoryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'material': instance.material,
      'quantity': instance.quantity,
      'quantityUnit': _$QuantityUnitEnumMap[instance.quantityUnit],
      'price': instance.price,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$QuantityUnitEnumMap = {
  QuantityUnit.h: 'h',
  QuantityUnit.m2: 'm2',
  QuantityUnit.kg: 'kg',
  QuantityUnit.t: 't',
  QuantityUnit.m: 'm',
  QuantityUnit.m3: 'm3',
  QuantityUnit.stk: 'stk',
};
