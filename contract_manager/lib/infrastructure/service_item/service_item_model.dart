import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/domain/service_item/service_item.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/converters/last_seen_versions_converter.dart';
import 'package:contract_manager/infrastructure/converters/nullable_timestamp_converter.dart';
import 'package:contract_manager/infrastructure/converters/report_converter.dart';
import 'package:contract_manager/infrastructure/converters/service_item_converter.dart';
import 'package:contract_manager/infrastructure/converters/string_list_converter.dart';
import 'package:contract_manager/infrastructure/converters/timestamp_converter.dart';
import 'package:dartz/dartz.dart';
import 'package:json_annotation/json_annotation.dart';

part 'service_item_model.g.dart';

@JsonSerializable()
@StringListConverter()
@ServiceItemConverter()
@ReportConverter()
@TimestampConverter()
@NullableTimestampConverter()
@LastSeenVersionsConverter()
class ServiceItemModel extends ServiceItem {
  const ServiceItemModel({
    required String id,
    required String projectId,
    required String contractId,
    required String specification,
    required DateTime startsAt,
    required DateTime endPlannedAt,
    required BuiltMap<String, ServiceItemInventory> inventories,
    required int versionNumber,
    required BuiltMap<String, int> lastSeenVersions,
    Report? report,
    String? outsourcedContractId,
    String? subContractId,
    DateTime? workDoneAt,
    DateTime? approvedAt,
  }) : super(
          id,
          projectId,
          contractId,
          specification,
          startsAt,
          endPlannedAt,
          workDoneAt,
          approvedAt,
          inventories,
          report,
          outsourcedContractId,
          subContractId,
          versionNumber,
          lastSeenVersions,
        );

  factory ServiceItemModel.fromJson(Map<String, dynamic> json) => _$ServiceItemModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$ServiceItemModelToJson(this);

  @override
  ServiceItem copyWith({
    DateTime? workDoneAt,
    DateTime? approvedAt,
    BuiltMap<String, ServiceItemInventory>? inventories,
    BuiltMap<String, int>? lastSeenVersions,
    Report? report,
    String? contractId,
    String? outsourcedContractId,
    Option<String>? subContractId,
  }) =>
      ServiceItemModel(
        id: this.id,
        projectId: projectId,
        contractId: contractId ?? this.contractId,
        specification: specification,
        startsAt: startsAt,
        endPlannedAt: endPlannedAt,
        workDoneAt: workDoneAt ?? this.workDoneAt,
        approvedAt: approvedAt ?? this.approvedAt,
        inventories: inventories ?? this.inventories,
        report: report ?? this.report,
        outsourcedContractId: outsourcedContractId ?? this.outsourcedContractId,
        subContractId: subContractId == null ? this.subContractId : subContractId.fold(() => null, (a) => a),
        versionNumber: versionNumber,
        lastSeenVersions: lastSeenVersions ?? this.lastSeenVersions,
      );
}
