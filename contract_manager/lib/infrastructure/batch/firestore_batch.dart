import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/batch/batch_failure.dart';
import 'package:contract_manager/domain/batch/i_batch.dart';

class FirestoreBatch implements IBatch {
  FirestoreBatch(this.batch);

  final WriteBatch batch;

  @override
  Future<void> commit() async {
    try {
      await batch.commit();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e.toString());
      print(stackTrace);
      throw const BatchFailure.unexpected();
    }
  }
}
