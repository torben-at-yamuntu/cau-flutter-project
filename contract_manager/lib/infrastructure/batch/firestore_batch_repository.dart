import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/batch/i_batch.dart';
import 'package:contract_manager/domain/batch/i_batch_repository.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/firestore_repository.dart';

class FirestoreBatchRepository extends FirestoreRepository implements IBatchRepository {
  FirestoreBatchRepository([FirebaseFirestore? firestore])
      : super(firestore ?? FirebaseFirestore.instance);

  @override
  IBatch getBatch() => FirestoreBatch(firestore.batch());
}
