/// The infrastructure layer is the only layer who knows what is
/// "outside". It knows about the device functionalities, the database
/// and all other outside communication related tasks.
///
/// It implements the entities which are defined in the domain layer
/// as models and adds serialization.
/// It also implements the repositories, which are defined in the domain
/// layer. Repositories are facades to the outside. It enables other layers
/// for example to create, update or delete entity data in the database
/// without knowing anything about the implementation. But repositories
/// can also do more. For example the StorageRepository provides
/// functionality to upload files into the cloud.
library infrastructure_layer;

export 'batch/firestore_batch.dart';
export 'company/address_model.dart';
export 'company/bank_info_model.dart';
export 'company/company_model.dart';
export 'contract/contract_model.dart';
export 'contract/contract_repository.dart';
export 'converters/address_converter.dart';
export 'converters/bank_info_converter.dart';
export 'converters/nullable_timestamp_converter.dart';
export 'converters/payment_list_converter.dart';
export 'converters/report_converter.dart';
export 'converters/service_item_converter.dart';
export 'converters/service_item_converter.dart';
export 'converters/string_list_converter.dart';
export 'converters/timestamp_converter.dart';
export 'employee/employee_model.dart';
export 'project/project_model.dart';
export 'project/project_repository.dart';
export 'service_item/payment_model.dart';
export 'service_item/report_model.dart';
export 'service_item/service_item_inventory_model.dart';
export 'service_item/service_item_model.dart';
export 'transaction/firestore_transaction.dart';
export 'transaction/transaction_repository.dart';
