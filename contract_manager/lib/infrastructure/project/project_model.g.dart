// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectModel _$ProjectModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['id', 'createdAt', 'companyId', 'name'],
      disallowNullValues: const ['id', 'createdAt', 'companyId', 'name']);
  return ProjectModel(
    id: json['id'] as String,
    createdAt:
        const TimestampConverter().fromJson(json['createdAt'] as Timestamp),
    companyId: json['companyId'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$ProjectModelToJson(ProjectModel instance) {
  final val = <String, dynamic>{
    'id': instance.id,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull(
      'createdAt', const TimestampConverter().toJson(instance.createdAt));
  val['companyId'] = instance.companyId;
  val['name'] = instance.name;
  return val;
}
