import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/infrastructure/converters/timestamp_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project_model.g.dart';

@JsonSerializable()
@TimestampConverter()
class ProjectModel extends Project {
  const ProjectModel({
    required String id,
    required DateTime createdAt,
    required String companyId,
    required String name,
  }) : super(
          id,
          createdAt,
          companyId,
          name,
        );

  factory ProjectModel.fromJson(Map<String, dynamic> json) => _$ProjectModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$ProjectModelToJson(this);

  @override
  ProjectModel copyWith({
    String? name,
  }) =>
      ProjectModel(
        id: id,
        createdAt: createdAt,
        companyId: companyId,
        name: name ?? this.name,
      );
}
