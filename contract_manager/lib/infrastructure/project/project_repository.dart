import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/project/i_project_repository.dart';
import 'package:contract_manager/domain/project/project.dart';
import 'package:contract_manager/domain/project/project_failure.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/firestore_repository.dart';
import 'package:contract_manager/infrastructure/project/project_model.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class ProjectRepository extends FirestoreRepository implements IProjectRepository {
  ProjectRepository([FirebaseFirestore? firestore]) : super(firestore ?? FirebaseFirestore.instance);

  static const String _projectCollectionName = 'projects';

  final StreamTransformer<Map<String, dynamic>?, Project> projectTransformer = StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      try {
        sink.add(ProjectModel.fromJson(data!));
      } on BadKeyException catch (e, stackTrace) {
        print(e.message);
        print(stackTrace);
        sink.addError(const ProjectFailure.unexpected());
      }
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const ProjectFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  @override
  Future<void> updateName({
    required Project project,
    required String name,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference projectRef = super.firestore.collection('$_projectCollectionName/${project.id}').doc();
      final Project updatedProject = project.copyWith(name: name);

      await super.update(
        ref: projectRef,
        data: project.jsonFlatDiff(updatedProject),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ProjectFailure.unexpected();
    }
  }

  @override
  Future<Project> create({
    required String companyId,
    required String name,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference projectRef = firestore.collection(_projectCollectionName).doc();

      final ProjectModel project = ProjectModel(
        id: projectRef.id,
        createdAt: DateTime.now(),
        companyId: companyId,
        name: name,
      );

      await super.set(ref: projectRef, data: project.toJson());

      return project;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ProjectFailure.unexpected();
    }
  }

  @override
  Future<Project> getProject({
    required String projectId,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference projectRef = firestore.collection(_projectCollectionName).doc(projectId);

      final Map<String, dynamic>? json =
          await projectRef.snapshots().map((documentSnapshot) => documentSnapshot.data()).first;
      return (json != null) ? ProjectModel.fromJson(json) : throw const ProjectFailure.unexpected();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ProjectFailure.unexpected();
    }
  }
}
