// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractModel _$ContractModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'versionNumber',
    'lastSeenVersions',
    'id',
    'projectId',
    'createdAt',
    'contractorId',
    'clientId',
    'acceptedBy',
    'specification',
    'bankInfo',
    'costs',
    'name'
  ], disallowNullValues: const [
    'versionNumber',
    'lastSeenVersions',
    'id',
    'projectId',
    'createdAt',
    'contractorId',
    'clientId',
    'bankInfo',
    'costs',
    'name'
  ]);
  return ContractModel(
    id: json['id'] as String,
    projectId: json['projectId'] as String,
    createdAt:
        const TimestampConverter().fromJson(json['createdAt'] as Timestamp),
    contractorId: json['contractorId'] as String,
    clientId: json['clientId'] as String,
    specification: json['specification'] as String,
    bankInfo: const BankInfoConverter()
        .fromJson(json['bankInfo'] as Map<String, dynamic>),
    costs: const PaymentListConverter().fromJson(json['costs'] as List),
    versionNumber: json['versionNumber'] as int,
    lastSeenVersions: const LastSeenVersionsConverter()
        .fromJson(json['lastSeenVersions'] as Map<String, dynamic>),
    name: json['name'] as String,
    parentServiceItemId: json['parentServiceItemId'] as String?,
    acceptedBy: json['acceptedBy'] as String?,
  );
}

Map<String, dynamic> _$ContractModelToJson(ContractModel instance) {
  final val = <String, dynamic>{
    'versionNumber': instance.versionNumber,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('lastSeenVersions',
      const LastSeenVersionsConverter().toJson(instance.lastSeenVersions));
  val['id'] = instance.id;
  val['projectId'] = instance.projectId;
  writeNotNull(
      'createdAt', const TimestampConverter().toJson(instance.createdAt));
  val['contractorId'] = instance.contractorId;
  val['clientId'] = instance.clientId;
  val['acceptedBy'] = instance.acceptedBy;
  val['specification'] = instance.specification;
  writeNotNull('bankInfo', const BankInfoConverter().toJson(instance.bankInfo));
  writeNotNull('costs', const PaymentListConverter().toJson(instance.costs));
  val['parentServiceItemId'] = instance.parentServiceItemId;
  val['name'] = instance.name;
  return val;
}
