import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/contract/contract_failure.dart';
import 'package:contract_manager/domain/contract/i_contract_repository.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/contract/contract_model.dart';
import 'package:contract_manager/infrastructure/firestore_repository.dart';
import 'package:contract_manager/infrastructure/infrastructure.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:stream_transform/stream_transform.dart';

class ContractRepository extends FirestoreRepository implements IContractRepository {
  ContractRepository([FirebaseFirestore? firestore]) : super(firestore ?? FirebaseFirestore.instance);

  static const String _contractCollectionName = 'contracts';

  final StreamTransformer<List<Map<String, dynamic>?>, BuiltList<Contract>> contracstTransformer =
      StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      final Iterable<Contract> contracts = data
          .map<Contract?>((docData) {
            try {
              if (docData == null) {
                return null;
              }
              return ContractModel.fromJson(docData);
            } on BadKeyException catch (e, stackTrace) {
              print(e.message);
              print(stackTrace);
              return null;
            }
          })
          .where((project) => project != null)
          .cast<Contract>();
      sink.add(BuiltList<Contract>.from(contracts));
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const ContractFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );
  final StreamTransformer<Map<String, dynamic>?, Contract> contractTransformer = StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      try {
        if (data != null) {
          final contract = ContractModel.fromJson(data);
          sink.add(contract);
        }
        sink.addError(const ContractFailure.unexpected());
      } on BadKeyException catch (e, stackTrace) {
        print(e.message);
        print(stackTrace);
        sink.addError(const ContractFailure.unexpected());
      }
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const ContractFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  @override
  Future<void> acceptContract({
    required Contract contract,
    required String employeeId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef =
          super.firestore.doc('projects/${contract.projectId}/$_contractCollectionName/${contract.id}');
      await super.update(
        ref: contractRef,
        data: <String, dynamic>{'acceptedBy': employeeId},
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> addPayment({
    required Contract contract,
    required double amount,
    required List<String> serviceItemIds,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef =
          super.firestore.doc('projects/${contract.projectId}/$_contractCollectionName/${contract.id}');

      final Payment payment = PaymentModel(
        id: (contract.costs.length + 1).toString(),
        amount: amount,
        serviceItemIds: BuiltList(serviceItemIds),
        sentPaymentAt: null,
        receivedPaymentAt: null,
        paymentActiveAt: null,
      );

      await super.update(
        ref: contractRef,
        data: <String, dynamic>{
          'costs': FieldValue. //
              arrayUnion(<Map<String, dynamic>>[payment.toJson()])
        },
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<Contract> create({
    required String projectId,
    required String contractorId,
    required String clientId,
    required String specification,
    required BankInfo bankInfo,
    required String name,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef =
          super.firestore.collection('projects/$projectId/$_contractCollectionName').doc();

      final Contract serviceItem = ContractModel(
        id: contractRef.id,
        projectId: projectId,
        createdAt: DateTime.now(),
        contractorId: contractorId,
        clientId: clientId,
        specification: specification,
        bankInfo: bankInfo,
        name: name,
        costs: BuiltList<Payment>(<Payment>[]),
        versionNumber: 0,
        lastSeenVersions: BuiltMap(<String, int>{}),
      );

      await super.set(
        ref: contractRef,
        data: serviceItem.toJson(),
        batch: batch,
        transaction: transaction,
      );

      return serviceItem;
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> incrementVersion({
    required Contract contract,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractItemRef =
          super.firestore.doc('projects/${contract.projectId}/$_contractCollectionName/${contract.id}');

      await super.update(
        ref: contractItemRef,
        data: <String, dynamic>{'versionNumber': FieldValue.increment(1)},
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> updateLastSeenVersion({
    required Contract contract,
    required String employeeId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference docRef =
          firestore.doc('projects/${contract.projectId}/$_contractCollectionName/${contract.id}');

      final Contract updatedContract = contract.copyWith(
        lastSeenVersions: contract.lastSeenVersions.rebuild(
          (builder) => builder.updateValue(
            employeeId,
            (_) => contract.versionNumber,
            ifAbsent: () => contract.versionNumber,
          ),
        ),
      );

      final updateData = contract.jsonFlatDiff(updatedContract);

      await super.update(ref: docRef, data: updateData, batch: batch, transaction: transaction);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e.toString());
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  Stream<BuiltList<Contract>> watchAllContractsWithCompanyasContractor(
    String companyId,
  ) =>
      super
          .firestore
          .collectionGroup(_contractCollectionName)
          .where('contractorId', isEqualTo: companyId)
          .snapshots()
          .map<List<Map<String, dynamic>?>>(
            (querySnapshot) => querySnapshot.docs.map((documentSnapshot) => documentSnapshot.data()).toList(),
          )
          .transform(contracstTransformer);

  Stream<BuiltList<Contract>> watchAllContractsWithCompaniesClient(
    String companyId,
  ) =>
      super
          .firestore
          .collectionGroup(_contractCollectionName)
          .where('clientId', isEqualTo: companyId)
          .snapshots()
          .map<List<Map<String, dynamic>?>>(
            (querySnapshot) => querySnapshot.docs.map((documentSnapshot) => documentSnapshot.data()).toList(),
          )
          .transform(contracstTransformer);

  @override
  Stream<BuiltList<Contract>> watchAllContractsWithCompanyParticipation({required String companyId}) =>
      watchAllContractsWithCompanyasContractor(companyId).merge(watchAllContractsWithCompaniesClient(companyId));

  @override
  Future<void> setParentServiceItemId({
    required Contract contract,
    required String parentServiceItemId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference docRef =
          firestore.doc('projects/${contract.projectId}/$_contractCollectionName/${contract.id}');

      final Contract updatedContract = contract.copyWith(parentServiceItemId: parentServiceItemId);

      final updateData = contract.jsonFlatDiff(updatedContract);

      await super.update(ref: docRef, data: updateData, batch: batch, transaction: transaction);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e.toString());
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<Contract> getContractById({
    required String projectId,
    required String contractId,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef =
          firestore.collection('projects').doc(projectId).collection('contract').doc(contractId);

      final Map<String, dynamic>? json =
          await contractRef.snapshots().map((documentSnapshot) => documentSnapshot.data()).first;
      return (json != null) ? ContractModel.fromJson(json) : throw const ContractFailure.unexpected();
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> deleteContract({
    required Contract contract,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef = firestore.doc('projects/${contract.projectId}/contracts/${contract.id}');

      await super.delete(
        ref: contractRef,
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> updatePaymentPaymentActiveAt({
    required Contract contract,
    required String paymentId,
    required DateTime paymentActiveAt,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef = firestore.doc('projects/${contract.projectId}/contracts/${contract.id}');

      final BuiltList<Payment> currentCosts = contract.costs;
      final Payment currentPayment = currentCosts.firstWhere((payment) => payment.id == paymentId);
      final Payment updatedPayment = currentPayment.copyWith(paymentActiveAt: paymentActiveAt);
      final BuiltList<Payment> updatedCosts =
          currentCosts.toList().map((e) => e.id == paymentId ? updatedPayment : e).toBuiltList();

      final Contract updatedContract = contract.copyWith(costs: updatedCosts);

      final updateData = contract.jsonFlatDiff(updatedContract);

      await super.update(ref: contractRef, data: updateData, batch: batch, transaction: transaction);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> updatePaymentReceivedPaymentAt({
    required Contract contract,
    required String paymentId,
    required DateTime receivedPaymentAt,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef = firestore.doc('projects/${contract.projectId}/contracts/${contract.id}');

      final BuiltList<Payment> currentCosts = contract.costs;
      final Payment currentPayment = currentCosts.firstWhere((payment) => payment.id == paymentId);
      final Payment updatedPayment = currentPayment.copyWith(receivedPaymentAt: receivedPaymentAt);
      final BuiltList<Payment> updatedCosts =
          currentCosts.toList().map((e) => e.id == paymentId ? updatedPayment : e).toBuiltList();

      final Contract updatedContract = contract.copyWith(costs: updatedCosts);

      final updateData = contract.jsonFlatDiff(updatedContract);

      await super.update(ref: contractRef, data: updateData, batch: batch, transaction: transaction);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Future<void> updatePaymentSentPaymentAt({
    required Contract contract,
    required String paymentId,
    required DateTime sentPaymentAt,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference contractRef = firestore.doc('projects/${contract.projectId}/contracts/${contract.id}');

      final BuiltList<Payment> currentCosts = contract.costs;
      final Payment currentPayment = currentCosts.firstWhere((payment) => payment.id == paymentId);
      final Payment updatedPayment = currentPayment.copyWith(sentPaymentAt: sentPaymentAt);
      final BuiltList<Payment> updatedCosts =
          currentCosts.toList().map((e) => e.id == paymentId ? updatedPayment : e).toBuiltList();

      final Contract updatedContract = contract.copyWith(costs: updatedCosts);

      final updateData = contract.jsonFlatDiff(updatedContract);

      await super.update(ref: contractRef, data: updateData, batch: batch, transaction: transaction);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const ContractFailure.unexpected();
    }
  }

  @override
  Stream<Contract> watchContract({
    required String contractId,
    required String projectId,
  }) =>
      firestore
          .collection('projects')
          .doc(projectId)
          .collection('contracts')
          .doc(contractId)
          .snapshots()
          .map<Map<String, dynamic>?>((document) => document.data())
          .transform(contractTransformer);
}
