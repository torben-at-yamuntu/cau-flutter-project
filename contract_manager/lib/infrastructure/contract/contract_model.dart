import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/contract/contract.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/converters/bank_info_converter.dart';
import 'package:contract_manager/infrastructure/converters/last_seen_versions_converter.dart';
import 'package:contract_manager/infrastructure/converters/payment_list_converter.dart';
import 'package:contract_manager/infrastructure/converters/timestamp_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contract_model.g.dart';

@JsonSerializable()
@TimestampConverter()
@PaymentListConverter()
@BankInfoConverter()
@LastSeenVersionsConverter()
class ContractModel extends Contract {
  const ContractModel({
    required String id,
    required String projectId,
    required DateTime createdAt,
    required String contractorId,
    required String clientId,
    required String specification,
    required BankInfo bankInfo,
    required BuiltList<Payment> costs,
    required int versionNumber,
    required BuiltMap<String, int> lastSeenVersions,
    required String name,
    String? parentServiceItemId,
    String? acceptedBy,
  }) : super(
          id,
          projectId,
          createdAt,
          contractorId,
          clientId,
          acceptedBy,
          specification,
          bankInfo,
          costs,
          parentServiceItemId,
          name,
          versionNumber,
          lastSeenVersions,
        );

  factory ContractModel.fromJson(Map<String, dynamic> json) => _$ContractModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$ContractModelToJson(this);

  @override
  ContractModel copyWith({
    String? acceptedBy,
    BankInfo? bankInfo,
    BuiltList<Payment>? costs,
    BuiltMap<String, int>? lastSeenVersions,
    String? parentServiceItemId,
  }) =>
      ContractModel(
        id: id,
        projectId: projectId,
        createdAt: createdAt,
        contractorId: contractorId,
        clientId: clientId,
        acceptedBy: acceptedBy ?? this.acceptedBy,
        specification: specification,
        bankInfo: bankInfo ?? this.bankInfo,
        costs: costs ?? this.costs,
        parentServiceItemId: parentServiceItemId ?? this.parentServiceItemId,
        name: name,
        versionNumber: versionNumber,
        lastSeenVersions: lastSeenVersions ?? this.lastSeenVersions,
      );
}
