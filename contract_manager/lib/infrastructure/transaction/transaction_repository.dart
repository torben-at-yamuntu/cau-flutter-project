import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/transaction/i_transaction_repository.dart';
import 'package:contract_manager/domain/transaction/transaction_failure.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';

class TransactionRepository<T> implements ITransactionRepository<T> {
  TransactionRepository([FirebaseFirestore? firestore])
      : _firestore = firestore ?? FirebaseFirestore.instance;

  final FirebaseFirestore _firestore;

  @override
  Future<T> runTransaction(TransactionCallback<T> callback) async {
    try {
      return _firestore.runTransaction<T>(
        (transaction) => callback.call(
          FirestoreTransaction(transaction),
        ),
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e.toString());
      print(stackTrace);

      throw const TransactionFailure.unexpected();
    }
  }
}
