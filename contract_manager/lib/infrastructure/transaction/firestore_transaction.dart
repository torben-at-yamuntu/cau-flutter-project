import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';

class FirestoreTransaction implements ITransaction {
  FirestoreTransaction(this.transaction);

  final Transaction transaction;
}
