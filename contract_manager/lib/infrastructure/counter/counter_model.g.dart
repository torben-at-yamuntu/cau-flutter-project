// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'counter_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CounterModel _$CounterModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['createdAt', 'id', 'value'],
      disallowNullValues: const ['createdAt', 'id', 'value']);
  return CounterModel(
    createdAt:
        const TimestampConverter().fromJson(json['createdAt'] as Timestamp),
    id: json['id'] as String,
    value: json['value'] as int,
  );
}

Map<String, dynamic> _$CounterModelToJson(CounterModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull(
      'createdAt', const TimestampConverter().toJson(instance.createdAt));
  val['id'] = instance.id;
  val['value'] = instance.value;
  return val;
}
