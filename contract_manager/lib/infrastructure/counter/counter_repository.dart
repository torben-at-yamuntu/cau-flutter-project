import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/counter/counter.dart';
import 'package:contract_manager/domain/counter/counter_failure.dart';
import 'package:contract_manager/domain/counter/i_counter_repository.dart';
import 'package:contract_manager/infrastructure/counter/counter_model.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class CounterRepository implements ICounterRepository {
  CounterRepository([FirebaseFirestore? firestore]) : _firestore = firestore ?? FirebaseFirestore.instance;

  static const String _counterCollectionName = 'counters';

  final FirebaseFirestore _firestore;

  final StreamTransformer<List<Map<String, dynamic>?>, BuiltList<Counter>> counterTransformer =
      StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      final Iterable<Counter> counters = data
          .map<Counter?>((docData) {
            try {
              if (docData == null) {
                return null;
              }
              return CounterModel.fromJson(docData);
            } on BadKeyException catch (e, stackTrace) {
              print(e.message);
              print(stackTrace);
              return null;
            }
          })
          .where((counter) => counter != null)
          .cast<Counter>();
      sink.add(BuiltList<Counter>.from(counters));
    },
    handleError: (error, stackTrace, sink) {
      print(error);
      print(stackTrace);
      sink.addError(const CounterFailure.unexpected());
    },
    handleDone: (sink) {
      sink.close();
    },
  );

  @override
  Stream<BuiltList<Counter>> watchCounters() => _firestore
      .collection(_counterCollectionName)
      .snapshots()
      .map<List<Map<String, dynamic>?>>(
        (querySnapshot) => querySnapshot.docs.map((documentSnapshot) => documentSnapshot.data()).toList(),
      )
      .transform(counterTransformer);

  @override
  Future<Either<CounterFailure, Unit>> create() async {
    try {
      final DocumentReference counterRef = _firestore.collection(_counterCollectionName).doc();
      final CounterModel counter = CounterModel(createdAt: DateTime.now(), id: counterRef.id, value: 0);
      await counterRef.set(counter.toJson());

      return right(unit);
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      return left(const CounterFailure.unexpected());
    }
  }

  @override
  Future<Either<CounterFailure, Unit>> increment(String counterId) async {
    try {
      final DocumentReference counterRef = _firestore.collection(_counterCollectionName).doc(counterId);
      await counterRef.update(<String, dynamic>{
        'value': FieldValue.increment(1),
      });
      return right(unit);
    } catch (e) {
      return left(const CounterFailure.unexpected());
    }
  }
}
