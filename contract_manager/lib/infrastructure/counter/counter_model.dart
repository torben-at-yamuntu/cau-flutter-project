import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/counter/counter.dart';
import 'package:contract_manager/infrastructure/converters/timestamp_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'counter_model.g.dart';

@JsonSerializable()
@TimestampConverter()
class CounterModel extends Counter {
  const CounterModel({
    required DateTime createdAt,
    required String id,
    required int value,
  }) : super(createdAt, id, value);

  factory CounterModel.fromJson(Map<String, dynamic> json) => _$CounterModelFromJson(json);

  Map<String, Object?> toJson() => _$CounterModelToJson(this);

  @override
  CounterModel copyWith({DateTime? createdAt, String? id, int? value}) => CounterModel(
        createdAt: createdAt ?? this.createdAt,
        id: id ?? this.id,
        value: value ?? this.value,
      );
}
