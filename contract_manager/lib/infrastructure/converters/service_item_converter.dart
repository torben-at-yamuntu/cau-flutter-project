import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/service_item/service_item_inventory.dart';
import 'package:contract_manager/infrastructure/service_item/service_item_inventory_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class ServiceItemConverter implements JsonConverter<BuiltMap<String, ServiceItemInventory>, Map<String, dynamic>> {
  const ServiceItemConverter();

  @override
  BuiltMap<String, ServiceItemInventory> fromJson(Map<String, dynamic> json) => BuiltMap.from(
      json.map<String, ServiceItemInventory>((key, dynamic value) => MapEntry<String, ServiceItemInventory>(
          key, ServiceItemInventoryModel.fromJson(value as Map<String, dynamic>))));

  @override
  Map<String, dynamic> toJson(BuiltMap<String, ServiceItemInventory> object) =>
      object.toMap().map<String, dynamic>((key, value) => MapEntry<String, Map<String, dynamic>>(key, value.toJson()));
}
