import 'package:contract_manager/domain/service_item/report.dart';
import 'package:contract_manager/infrastructure/service_item/report_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class ReportConverter implements JsonConverter<Report?, Map<String, dynamic>?> {
  const ReportConverter();

  @override
  Report? fromJson(Map<String, dynamic>? json) {
    if (json == null) return null;
    return ReportModel.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(Report? report) {
    if (report == null) return null;
    return report.toJson();
  }
}
