import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class BankInfoConverter implements JsonConverter<BankInfo, Map<String, dynamic>> {
  const BankInfoConverter();

  @override
  BankInfo fromJson(Map<String, dynamic> json) => BankInfoModel.fromJson(json);

  @override
  Map<String, dynamic> toJson(BankInfo bankInfo) => bankInfo.toJson();
}
