import 'package:built_collection/built_collection.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class LastSeenVersionsConverter
    implements JsonConverter<BuiltMap<String, int>, Map<String, dynamic>> {
  const LastSeenVersionsConverter();

  @override
  BuiltMap<String, int> fromJson(Map<String, dynamic> json) => json
      .map<String, int>((key, dynamic value) => MapEntry<String, int>(key, value as int))
      .build();

  @override
  Map<String, dynamic> toJson(BuiltMap<String, int> object) => object.toMap();
}
