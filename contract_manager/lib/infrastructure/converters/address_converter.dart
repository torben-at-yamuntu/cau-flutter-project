import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class AddressConverter implements JsonConverter<Address, Map<String, dynamic>> {
  const AddressConverter();

  @override
  Address fromJson(Map<String, dynamic> json) => AddressModel.fromJson(json);

  @override
  Map<String, dynamic> toJson(Address address) => address.toJson();
}
