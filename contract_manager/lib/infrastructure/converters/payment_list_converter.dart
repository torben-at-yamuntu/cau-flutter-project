import 'package:built_collection/built_collection.dart';
import 'package:contract_manager/domain/service_item/payment.dart';
import 'package:contract_manager/infrastructure/service_item/payment_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class PaymentListConverter implements JsonConverter<BuiltList<Payment>, List> {
  const PaymentListConverter();

  @override
  BuiltList<Payment> fromJson(List json) => BuiltList.from(
      json.map<Payment>((dynamic paymentJson) => PaymentModel.fromJson(paymentJson as Map<String, dynamic>)));

  @override
  List<Map<String, dynamic>> toJson(BuiltList<Payment> object) =>
      object.map<Map<String, dynamic>>((payment) => payment.toJson()).toList();
}
