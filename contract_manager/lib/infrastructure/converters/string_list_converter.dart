import 'package:built_collection/built_collection.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class StringListConverter implements JsonConverter<BuiltList<String>, List<dynamic>> {
  const StringListConverter();
  @override
  BuiltList<String> fromJson(List<dynamic> json) => BuiltList.from(json);

  @override
  List<String> toJson(BuiltList<String> object) => object.toList();
}
