// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressModel _$AddressModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const [
    'country',
    'city',
    'zip',
    'street',
    'houseNumber'
  ], disallowNullValues: const [
    'country',
    'city',
    'zip',
    'street',
    'houseNumber'
  ]);
  return AddressModel(
    country: json['country'] as String,
    city: json['city'] as String,
    zip: json['zip'] as int,
    street: json['street'] as String,
    houseNumber: json['houseNumber'] as int,
  );
}

Map<String, dynamic> _$AddressModelToJson(AddressModel instance) =>
    <String, dynamic>{
      'country': instance.country,
      'city': instance.city,
      'zip': instance.zip,
      'street': instance.street,
      'houseNumber': instance.houseNumber,
    };
