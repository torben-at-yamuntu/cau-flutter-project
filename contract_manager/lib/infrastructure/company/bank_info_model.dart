import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:json_annotation/json_annotation.dart';

part 'bank_info_model.g.dart';

@JsonSerializable()
class BankInfoModel extends BankInfo {
  const BankInfoModel({
    required String iban,
    required String owner,
    required String bic,
  }) : super(iban, owner, bic);

  factory BankInfoModel.fromJson(Map<String, dynamic> json) => _$BankInfoModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$BankInfoModelToJson(this);

  @override
  BankInfoModel copyWith({String? iban, String? owner, String? bic}) => BankInfoModel(
        iban: iban ?? this.iban,
        owner: owner ?? this.owner,
        bic: bic ?? this.bic,
      );
}
