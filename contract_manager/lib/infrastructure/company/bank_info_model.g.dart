// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankInfoModel _$BankInfoModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['iban', 'bic', 'owner'],
      disallowNullValues: const ['iban', 'bic', 'owner']);
  return BankInfoModel(
    iban: json['iban'] as String,
    owner: json['owner'] as String,
    bic: json['bic'] as String,
  );
}

Map<String, dynamic> _$BankInfoModelToJson(BankInfoModel instance) =>
    <String, dynamic>{
      'iban': instance.iban,
      'bic': instance.bic,
      'owner': instance.owner,
    };
