import 'package:contract_manager/domain/company/address.dart';
import 'package:contract_manager/domain/company/bank_info.dart';
import 'package:contract_manager/domain/company/company.dart';
import 'package:contract_manager/infrastructure/converters/address_converter.dart';
import 'package:contract_manager/infrastructure/converters/bank_info_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'company_model.g.dart';

@JsonSerializable()
@BankInfoConverter()
@AddressConverter()
class CompanyModel extends Company {
  const CompanyModel({
    required String id,
    required String name,
    required BankInfo bankInfo,
    required Address address,
  }) : super(id, name, bankInfo, address);

  factory CompanyModel.fromJson(Map<String, dynamic> json) => _$CompanyModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$CompanyModelToJson(this);

  @override
  CompanyModel copyWith({
    String? id,
    String? name,
    BankInfo? bankInfo,
    Address? address,
  }) =>
      CompanyModel(
        id: id ?? this.id,
        name: name ?? this.name,
        bankInfo: bankInfo ?? this.bankInfo,
        address: address ?? this.address,
      );
}
