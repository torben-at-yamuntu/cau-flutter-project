import 'package:contract_manager/domain/company/address.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address_model.g.dart';

@JsonSerializable()
class AddressModel extends Address {
  const AddressModel({
    required String country,
    required String city,
    required int zip,
    required String street,
    required int houseNumber,
  }) : super(
          country,
          city,
          zip,
          street,
          houseNumber,
        );

  factory AddressModel.fromJson(Map<String, dynamic> json) => _$AddressModelFromJson(json);

  @override
  Map<String, Object?> toJson() => _$AddressModelToJson(this);

  @override
  AddressModel copyWith({
    String? country,
    String? city,
    int? zip,
    String? street,
    int? houseNumber,
  }) =>
      AddressModel(
        country: country ?? this.country,
        city: city ?? this.city,
        zip: zip ?? this.zip,
        street: street ?? this.street,
        houseNumber: houseNumber ?? this.houseNumber,
      );
}
