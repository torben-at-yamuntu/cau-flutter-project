// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompanyModel _$CompanyModelFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['id', 'name', 'address', 'bankInfo'],
      disallowNullValues: const ['id', 'name', 'address', 'bankInfo']);
  return CompanyModel(
    id: json['id'] as String,
    name: json['name'] as String,
    bankInfo: const BankInfoConverter()
        .fromJson(json['bankInfo'] as Map<String, dynamic>),
    address: const AddressConverter()
        .fromJson(json['address'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CompanyModelToJson(CompanyModel instance) {
  final val = <String, dynamic>{
    'id': instance.id,
    'name': instance.name,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('address', const AddressConverter().toJson(instance.address));
  writeNotNull('bankInfo', const BankInfoConverter().toJson(instance.bankInfo));
  return val;
}
