import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/domain/company/company_failure.dart';
import 'package:contract_manager/domain/domain.dart';
import 'package:contract_manager/domain/transaction/i_transaction.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/company/address_model.dart';
import 'package:contract_manager/infrastructure/company/bank_info_model.dart';
import 'package:contract_manager/infrastructure/company/company_model.dart';
import 'package:contract_manager/infrastructure/firestore_repository.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class CompanyRepository extends FirestoreRepository implements ICompanyRepository {
  CompanyRepository([FirebaseFirestore? firestore]) : super(firestore ?? FirebaseFirestore.instance);

  static const String companyCollection = 'companies';

  final StreamTransformer<Map<String, dynamic>?, Company> companyStreamTransformer =
      StreamTransformer.fromHandlers(handleData: (data, sink) {
    try {
      if (data != null) {
        final company = CompanyModel.fromJson(data);
        sink.add(company);
      } else {
        sink.addError(const CompanyFailure.unexpected());
      }
    } catch (e, stackTrace) {
      print('Here 1');
      print(e.runtimeType);
      print(e);
      print(stackTrace);

      sink.addError(const CompanyFailure.unexpected());
    }
  });

  @override
  Future<Company> create({
    required String name,
    required String country,
    required String city,
    required int zip,
    required String street,
    required int houseNumber,
    required String iban,
    required String bic,
    required String accountOwner,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference companyRef = firestore.collection(companyCollection).doc();

      final Address address = AddressModel(
        country: country,
        city: city,
        zip: zip,
        street: street,
        houseNumber: houseNumber,
      );

      final BankInfo bankInfo = BankInfoModel(
        iban: iban,
        owner: accountOwner,
        bic: bic,
      );

      final Company company = CompanyModel(
        id: companyRef.id,
        name: name,
        bankInfo: bankInfo,
        address: address,
      );

      await super.set(ref: companyRef, data: company.toJson());

      return company;
    } catch (e, stackTrace) {
      print('Here 6');
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const CompanyFailure.unexpected();
    }
  }

  @override
  Future<void> updateAddress({
    required Company company,
    required Address address,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference companyRef = super.firestore.collection(companyCollection).doc(company.id);

      final Company updatedCompany = company.copyWith(address: address);

      await super.update(
        ref: companyRef,
        data: company.jsonFlatDiff(updatedCompany),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print('Here 5');
      print(e.runtimeType);
      print(e);
      print(stackTrace);
      throw const CompanyFailure.unexpected();
    }
  }

  @override
  Future<void> updateBankInfo({
    required Company company,
    required BankInfo bankInfo,
    covariant FirestoreBatch? batch,
    covariant FirestoreTransaction? transaction,
  }) async {
    try {
      final DocumentReference companyRef = super.firestore.collection('$companyCollection/${company.id}').doc();

      final Company updatedCompany = company.copyWith(bankInfo: bankInfo);

      await super.update(
        ref: companyRef,
        data: company.jsonFlatDiff(updatedCompany),
        batch: batch,
        transaction: transaction,
      );
    } catch (e, stackTrace) {
      print(e.runtimeType);
      print(e);
      print('Here 2');
      print(stackTrace);
      throw const CompanyFailure.unexpected();
    }
  }

  @override
  Future<Company> getCompany({required String companyId, ITransaction? transaction}) async {
    final DocumentReference doc = firestore.collection(companyCollection).doc(companyId);

    try {
      final Map<String, dynamic>? json = await doc.snapshots().map((documentSnapshot) => documentSnapshot.data()).first;
      return (json != null) ? CompanyModel.fromJson(json) : throw const CompanyFailure.unexpected();
    } on BadKeyException catch (e, stackTrace) {
      print(e.message);
      print('Here 3');
      print(stackTrace);
      throw const CompanyFailure.unexpected();
    } catch (e, stackTrace) {
      print(e);
      print('Here 4');
      print(stackTrace);
      throw const CompanyFailure.unexpected();
    }
  }

  @override
  Stream<Company> watchCompany({
    required String companyId,
  }) =>
      firestore
          .collection(companyCollection)
          .doc(companyId)
          .snapshots()
          .map<Map<String, dynamic>?>((document) => document.data())
          .transform(companyStreamTransformer);
}
