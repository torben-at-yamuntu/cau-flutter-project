import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contract_manager/infrastructure/batch/firestore_batch.dart';
import 'package:contract_manager/infrastructure/transaction/firestore_transaction.dart';

/// Provides [FirebaseFirestore] for repositories.
abstract class FirestoreRepository {
  FirestoreRepository(this.firestore);

  final FirebaseFirestore firestore;

  Future<void> delete({
    required DocumentReference ref,
    FirestoreBatch? batch,
    FirestoreTransaction? transaction,
  }) async {
    if (batch != null) {
      batch.batch.delete(ref);
      return;
    }

    if (transaction != null) {
      transaction.transaction.delete(ref);
      return;
    }

    await ref.delete();
  }

  Future<void> set({
    required DocumentReference ref,
    required Map<String, dynamic> data,
    FirestoreBatch? batch,
    FirestoreTransaction? transaction,
  }) async {
    if (batch != null) {
      batch.batch.set(ref, data);
      return;
    }

    if (transaction != null) {
      transaction.transaction.set(ref, data);
      return;
    }

    await ref.set(data);
  }

  Future<void> update({
    required DocumentReference ref,
    required Map<String, dynamic> data,
    FirestoreBatch? batch,
    FirestoreTransaction? transaction,
  }) async {
    if (batch != null) {
      batch.batch.update(ref, data);
      return;
    }

    if (transaction != null) {
      transaction.transaction.update(ref, data);
      return;
    }

    await ref.update(data);
  }

  Future<DocumentSnapshot> getDocument({
    required DocumentReference ref,
    FirestoreTransaction? transaction,
  }) {
    if (transaction != null) {
      return transaction.transaction.get(ref);
    }

    return ref.get();
  }
}
